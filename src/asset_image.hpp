//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

#pragma once
#include "utils.hpp"

namespace oit {
	namespace detail {
		void AssetLoadBMPFile(const std::string &filename, unsigned int &width, unsigned int &height, std::vector<unsigned char> &data);
	}

	//--------------------------------------------------------------------------------------------
	void ImageFlipY(unsigned int width, unsigned int height, unsigned int num_channels, std::vector<unsigned char> &data);

	//--------------------------------------------------------------------------------------------
	void ImageFlipX(unsigned int width, unsigned int height, unsigned int num_channels, std::vector<unsigned char> &data);
}