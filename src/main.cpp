﻿//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

#include "../dependencies/lap_wic/lap_wic.hpp"
#include "renderer_opaque.hpp"
#include "renderer_blending.hpp"
#include "renderer_meshkin.hpp"
#include "renderer_linked_list.hpp"
#include "renderer_occupancy_map.hpp"
#include "asset_manager.hpp"
#include "gui.hpp"

class Application {

private:
	oit::Camera* camera;
	oit::Scene* scene;
	oit::Gui* gui;
	unsigned int gui_toggle;
	bool toggle_override_transparency = false;

	oit::RendererInterface* renderer;
	oit::AssetManager asset_manager;

public:

	//--------------------------------------------------------------------------------------------
	//creates an unmanaged GPUMesh, with unmanaged GPUBuffers, considering the entire obj file as a single mesh, discards materials
	void CreateBasicScene() {

		scene = new oit::Scene();
		for (int i = 1; i <= 4; i++) {
			std::vector<oit::Vertex> vertices;
			float d = 1;
			if (i % 2) d = -1;		/// in no z-sorted order!
			oit::Vertex v1;	v1.position = glm::vec3(-40 + i * 20, -40 + i * 20, d*i * 15);		v1.texcoord = glm::vec3(0, 0, 0);	vertices.push_back(v1);
			oit::Vertex v2;	v2.position = glm::vec3(40 + i * 20, -40 + i * 20, d*i * 15);		v2.texcoord = glm::vec3(1, 0, 0);	vertices.push_back(v2);
			oit::Vertex v3;	v3.position = glm::vec3(40 + i * 20, 40 + i * 20, d*i * 15);		v3.texcoord = glm::vec3(1, 1, 0);	vertices.push_back(v3);
			oit::Vertex v4;	v4.position = glm::vec3(-40 + i * 20, 40 + i * 20, d*i * 15);		v4.texcoord = glm::vec3(0, 1, 0);	vertices.push_back(v4);
			std::vector<unsigned int> indices;
			indices.push_back(0);	indices.push_back(1);	indices.push_back(2);
			indices.push_back(2);	indices.push_back(3);	indices.push_back(0);
			oit::AABB aabb;
			oit::GPUBuffer* buffer_vertex = new oit::GPUBuffer((float*)&vertices[0], (int)vertices.size() * sizeof(oit::Vertex), oit::detail::BUFFER_USAGE_GPUSTATIC);
			oit::GPUBuffer* buffer_index = new oit::GPUBuffer((unsigned int*)&indices[0], (int)indices.size() * sizeof(unsigned int), oit::detail::BUFFER_USAGE_GPUSTATIC);
			oit::GPUMesh* mesh = new oit::GPUMesh(buffer_vertex, buffer_index, 0, (int)indices.size(), aabb, oit::detail::TOPOLOGY_TRIANGLES);

			oit::GPUMaterial* mat = new oit::GPUMaterial();
			glm::vec4 diffuse_transparency = glm::vec4(0, 0, 0, 0);
			if (i == 1) diffuse_transparency = glm::vec4(1, 0, 0, 0.2f);
			if (i == 2) diffuse_transparency = glm::vec4(0, 1, 0, 0.35f);
			if (i == 3) diffuse_transparency = glm::vec4(0, 0, 1, 0.25f);
			if (i == 4) diffuse_transparency = glm::vec4(1, 1, 0, 0.35f);
			mat->Add(glm::vec3(diffuse_transparency.x, diffuse_transparency.y, diffuse_transparency.z), "mat_diffuse");
			mat->Add(diffuse_transparency.w, "mat_transparency");
			oit::GPUEntity* entity = new oit::GPUEntity(mesh, mat);
			oit::SceneNode* node = new oit::SceneNode();
			node->AddEntity(entity);
			scene->AddSceneNode(node);
		}
		asset_manager.LoadScene(scene);
	}

	//--------------------------------------------------------------------------------------------
	//loads a complex scene
	void CreateComplexScene() {

		scene = new oit::Scene();
		for (unsigned int i = 1; i <= 64; i++) {
			std::string obj_filename = "../assets/" + std::to_string(i) + ".obj";
			std::string mtl_filename = "../assets/" + std::to_string(i) + ".mtl";

			std::vector<oit::Vertex> vertices;
			std::vector<unsigned int> indices;
			std::vector<oit::detail::ObjMaterial> materials;
			std::vector<oit::detail::ObjMesh> meshes;
			oit::detail::AssetLoadOBJFile(obj_filename, vertices, indices, materials, meshes);
			unsigned int total_indices = 0;
			for (auto &v : vertices) v.position += glm::vec3(-234500, 0, 25000);
			//test::MeshComputeNormals(vertices, indices);
			oit::AABB aabb;
			for (auto& m : meshes) {
				total_indices += m.index_count;
				m.aabb.inf += glm::vec3(-234500, 0, 25000);
				m.aabb.sup += glm::vec3(-234500, 0, 25000);
				aabb.AddAABB(m.aabb);
			}
			oit::GPUBuffer* buffer_vertex = new oit::GPUBuffer((float*)&vertices[0], (int)vertices.size() * sizeof(oit::Vertex), oit::detail::BUFFER_USAGE_GPUSTATIC);
			oit::GPUBuffer* buffer_index = new oit::GPUBuffer((unsigned int*)&indices[0], (int)indices.size() * sizeof(unsigned int), oit::detail::BUFFER_USAGE_GPUSTATIC);
			oit::GPUMesh* mesh = new oit::GPUMesh(buffer_vertex, buffer_index, 0, total_indices, aabb, oit::detail::TOPOLOGY_TRIANGLES);

			oit::GPUMaterial* mat = new oit::GPUMaterial();
			mat->Add(glm::vec3(materials[0].diffuse_transparency.x, materials[0].diffuse_transparency.y, materials[0].diffuse_transparency.z), "mat_diffuse");
			mat->Add(materials[0].diffuse_transparency.w, "mat_transparency");
			//no specular
			oit::GPUEntity* entity = new oit::GPUEntity(mesh, mat);
			oit::SceneNode* node = new oit::SceneNode();
			node->AddEntity(entity);
			scene->AddSceneNode(node);

		}
		asset_manager.LoadScene(scene);
	}

	Application(unsigned int width, unsigned int height) {

		//CreateBasicScene();
		CreateComplexScene();

		camera = new oit::Camera();
		camera->SetView(glm::vec3(0, 0, 40), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
		scene->SetCamera(camera);

		//gui
		gui_toggle = 3;
		gui = new oit::Gui("../dependencies/font/font.bmp", "../shaders/gui.vert", "../shaders/gui.frag");
		gui->SetResolution(width, height);

		//start with renderer opaque
		std::cout << "*************************CHANGED RENDERER*******************************" << std::endl;
		renderer = new oit::RendererOpaque();
		renderer->SetScene(scene);
		renderer->SetCamera(camera);
		renderer->SetProjection(75.0f, 1.0f, 5000.0f);
		renderer->SetRasterizationMode(oit::detail::RASTERIZATION_FILL);
		renderer->SetResolution(width, height);
	}

	~Application() {
		delete camera;
		delete gui;
		delete renderer;
	}



	//--------------------------------------------------------------------------------------------
	void Render() {
		//ammortized frametime
		static oit::Timer timer;
		static float frametimes[10] = { 0,0,0,0,0,0,0,0,0,0 };
		static int counter = 0;
		frametimes[counter] = timer.ElapsedSinceLastCallMilliSeconds();
		counter = (counter + 1) % 10;
		float frametime_sum = 0;
		float frametime_samples = 0;
		for (int i = 0; i < 10; i++) if (frametimes[i] > 0) {
			frametime_sum += frametimes[i];
			frametime_samples += 1;
		}
		float frametime_ms = frametime_sum / frametime_samples;

		//simulate scene
		scene->Simulate(timer.ElapsedMilliSeconds());

		//draw scene
		renderer->SetScene(scene);
		renderer->Draw();

		//render gui
		if (gui_toggle >= 1) {
			gui->ClearText();

			if (dynamic_cast<oit::RendererOccupancyMap*>(renderer)) {
				glm::uvec4 occupancy = dynamic_cast<oit::RendererOccupancyMap*>(renderer)->GetOccupancyForPixel();		// x =farthest, w = closest
				std::string slabs[4];
				slabs[0] = std::bitset<32>(occupancy.x).to_string();
				slabs[1] = std::bitset<32>(occupancy.y).to_string();
				slabs[2] = std::bitset<32>(occupancy.z).to_string();
				slabs[3] = std::bitset<32>(occupancy.w).to_string();
				for (unsigned k = 0; k < 4; k++) {
					float offset_x = 20;	if (k % 2 == 1) offset_x = 20 + 32 * 8 + 10;
					float offset_y = 12;	if (k > 1) offset_y = 21;
					for (unsigned int i = 0; i < 32; i++) if (slabs[k].at(i) == '1') gui->AddText(offset_x + 8 * i, offset_y, 8, 8, "1", glm::vec3(1, 0, 0));
					else gui->AddText(offset_x + 8 * i, offset_y, 8, 8, "0", glm::vec3(1, 1, 1));
				}
			}
			
			if (dynamic_cast<oit::RendererLinkedList*>(renderer)) {
				std::string& sorting_algorithm_name = dynamic_cast<oit::RendererLinkedList*>(renderer)->GetSortingAlgorithmName();
				gui->AddText(20, 40, 13, 13, "Current renderer = " + renderer->GetName() + " with " + sorting_algorithm_name, glm::vec3(1, 1, 1));
			}
			else {
				gui->AddText(20, 40, 13, 13, "Current renderer = " + renderer->GetName(), glm::vec3(1, 1, 1));
			}

			gui->AddText(20, 53, 13, 13, std::string("Transparency forced at 75% ") + (toggle_override_transparency ? "yes" : "no"));
			gui->AddText(20, 66, 13, 13, std::string("frame time ") + std::to_string(frametime_ms) + " ms", glm::vec3(1, 0, 0));
			if (gui_toggle >= 2) {
				glm::vec3 camp = camera->GetPosition();
				glm::vec3 camf = camera->GetForward();
				glm::vec3 camu = camera->GetUp();
				gui->AddText(20, 79, 13, 13, std::string("Cam position =") + std::to_string(camp.x) + " " + std::to_string(camp.y) + " " + std::to_string(camp.z), glm::vec3(1, 1, 1));
				gui->AddText(20, 92, 13, 13, std::string("Cam forward  =") + std::to_string(camf.x) + " " + std::to_string(camf.y) + " " + std::to_string(camf.z), glm::vec3(1, 1, 1));
				gui->AddText(20, 105, 13, 13, std::string("Cam up       =") + std::to_string(camu.x) + " " + std::to_string(camu.y) + " " + std::to_string(camu.z), glm::vec3(1, 1, 1));

				//glm::vec3 camfff = camera->GetFocus();
				//gui->AddText(20, 105, 13, 13, std::string("Cam focus    =") + std::to_string(camfff.x) + " " + std::to_string(camfff.y) + " " + std::to_string(camfff.z), glm::vec3(1, 1, 1));
				//glm::vec3 camr = camera->GetRight();
				//gui->AddText(20, 118, 13, 13, std::string("Cam right    =") + std::to_string(camr.x) + " " + std::to_string(camr.y) + " " + std::to_string(camr.z), glm::vec3(1, 1, 1));
			}
			if (gui_toggle >= 3) {
				gui->AddText(20, 118, 13, 13, "Keys: ", glm::vec3(1, 1, 1));
				gui->AddText(20, 131, 13, 13, "  F1 - view origin", glm::vec3(1, 1, 1));
				gui->AddText(20, 144, 13, 13, "  F2 - view 1", glm::vec3(1, 1, 1));
				gui->AddText(20, 157, 13, 13, "  F3 - view 2", glm::vec3(1, 1, 1));
				gui->AddText(20, 170, 13, 13, "  F4 - view 3", glm::vec3(1, 1, 1));
				gui->AddText(20, 183, 13, 13, "  F5 - view 4", glm::vec3(1, 1, 1));
				gui->AddText(20, 196, 13, 13, "  F6 - toggle fullscreen", glm::vec3(1, 1, 1));
				gui->AddText(20, 209, 13, 13, "  F7 - toggle vsync", glm::vec3(1, 1, 1));
				gui->AddText(20, 222, 13, 13, "  F8 - toggle scene", glm::vec3(1, 1, 1));
				gui->AddText(20, 235, 13, 13, "  G  - toggle gui (none, renderer & fps, +camera, +keys)", glm::vec3(1, 1, 1));
				gui->AddText(20, 248, 13, 13, "  ESC    - exit", glm::vec3(1, 1, 1));
				gui->AddText(20, 261, 13, 13, "  SPACE  - reload all shaders", glm::vec3(1, 1, 1));
				gui->AddText(20, 274, 13, 13, "  `      - Opaque renderer", glm::vec3(1, 1, 1));
				gui->AddText(20, 287, 13, 13, "  1      - Blending renderer", glm::vec3(1, 1, 1));
				gui->AddText(20, 300, 13, 13, "  2      - Meshkin partial renderer", glm::vec3(1, 1, 1));
				gui->AddText(20, 313, 13, 13, "  3      - Meshkin full renderer", glm::vec3(1, 1, 1));
				gui->AddText(20, 326, 13, 13, "  4      - Occupancy Map renderer (not suitable for this scene)", glm::vec3(1, 1, 1));
				gui->AddText(20, 339, 13, 13, "  5      - Per Pixel Linked Lists renderer, with Bubble Sort", glm::vec3(1, 1, 1));
				gui->AddText(20, 352, 13, 13, "  6      - Per Pixel Linked Lists renderer, with Selection Sort", glm::vec3(1, 1, 1));
				gui->AddText(20, 365, 13, 13, "  7      - Per Pixel Linked Lists renderer, with Insertion Sort", glm::vec3(1, 1, 1));
				gui->AddText(20, 378, 13, 13, "  8      - Per Pixel Linked Lists renderer, with Shell Sort", glm::vec3(1, 1, 1));
				gui->AddText(20, 391, 13, 13, "  9      - Per Pixel Linked Lists renderer, with Quick Sort", glm::vec3(1, 1, 1));
				gui->AddText(20, 404, 13, 13, "  0      - Per Pixel Linked Lists renderer, with Merge Sort", glm::vec3(1, 1, 1));
				gui->AddText(20, 417, 13, 13, "  WASDRF - camera movement ", glm::vec3(1, 1, 1));
				gui->AddText(20, 430, 13, 13, "  -,+    - camera speed ", glm::vec3(1, 1, 1));
				gui->AddText(20, 443, 13, 13, "  T		- override object transparency", glm::vec3(1, 1, 1));
				gui->AddText(20, 456, 13, 13, "  O		- show occupancy (only for occupancy renderer)", glm::vec3(1, 1, 1));
			}
			gui->Draw();
		}
	}


	void NotifyResize(lap::wic::Window& wnd, unsigned int width, unsigned int height, uint64_t timestamp) {
		if(renderer) renderer->SetResolution(width, height);
		gui->SetResolution(width, height);
	}
	void NotifyKeyPress(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		static float distance = 1.0f;

		//handle key
		switch (key) {
		case lap::wic::Key::SPACE:
			std::cout << "***************************RELOAD********************************" << std::endl;
			renderer->ReloadGPUPrograms();
			gui->ReloadGPUProgram();
			break;
		case lap::wic::Key::ESCAPE: 
			wnd.close();			
			break;//but the window is not destroyed, the OpenGL context will remain valid until the window object is destroyed (in main function)
		case lap::wic::Key::GRAVE_ACCENT:
			if (renderer) delete renderer;
			renderer = new oit::RendererOpaque();
			renderer->SetScene(scene);
			renderer->SetCamera(camera);
			renderer->SetProjection(75.0f, 1.0f, 5000.0f);
			renderer->SetRasterizationMode(oit::detail::RASTERIZATION_FILL);
			renderer->SetResolution(wnd.getWindowProperties().width, wnd.getWindowProperties().height);
			break;
		case lap::wic::Key::NUM1:
			if (renderer) delete renderer;
			renderer = new oit::RendererBlendingDefault();
			renderer->SetScene(scene);
			renderer->SetCamera(camera);
			renderer->SetProjection(75.0f, 1.0f, 5000.0f);
			renderer->SetRasterizationMode(oit::detail::RASTERIZATION_FILL);
			renderer->SetOverrideObjectTransparency(toggle_override_transparency);
			renderer->SetResolution(wnd.getWindowProperties().width, wnd.getWindowProperties().height);
			break;
		case lap::wic::Key::NUM2:
			if (renderer) delete renderer;
			renderer = new oit::RendererMeshkin();
			renderer->SetScene(scene);
			renderer->SetCamera(camera);
			renderer->SetProjection(75.0f, 1.0f, 5000.0f);
			renderer->SetRasterizationMode(oit::detail::RASTERIZATION_FILL);
			renderer->SetOverrideObjectTransparency(toggle_override_transparency);
			renderer->SetResolution(wnd.getWindowProperties().width, wnd.getWindowProperties().height);
			dynamic_cast<oit::RendererMeshkin*>(renderer)->SetFull(false);
			break;
		case lap::wic::Key::NUM3:
			if (renderer) delete renderer;
			renderer = new oit::RendererMeshkin();
			renderer->SetScene(scene);
			renderer->SetCamera(camera);
			renderer->SetProjection(75.0f, 1.0f, 5000.0f);
			renderer->SetRasterizationMode(oit::detail::RASTERIZATION_FILL);
			renderer->SetOverrideObjectTransparency(toggle_override_transparency);
			renderer->SetResolution(wnd.getWindowProperties().width, wnd.getWindowProperties().height);
			dynamic_cast<oit::RendererMeshkin*>(renderer)->SetFull(true);
			break;
		case lap::wic::Key::NUM4:
			if (renderer) delete renderer;
			renderer = new oit::RendererOccupancyMap();
			renderer->SetScene(scene);
			renderer->SetCamera(camera);
			renderer->SetProjection(75.0f, 1.0f, 5000.0f);
			renderer->SetRasterizationMode(oit::detail::RASTERIZATION_FILL);
			renderer->SetOverrideObjectTransparency(toggle_override_transparency);
			renderer->SetResolution(wnd.getWindowProperties().width, wnd.getWindowProperties().height);
			break;
		case lap::wic::Key::NUM5:
			if (!dynamic_cast<oit::RendererLinkedList*>(renderer)) {
				if (renderer) delete renderer;
				renderer = new oit::RendererLinkedList();
				renderer->SetScene(scene);
				renderer->SetCamera(camera);
				renderer->SetProjection(75.0f, 1.0f, 5000.0f);
				renderer->SetRasterizationMode(oit::detail::RASTERIZATION_FILL);
				renderer->SetOverrideObjectTransparency(toggle_override_transparency);
				renderer->SetResolution(wnd.getWindowProperties().width, wnd.getWindowProperties().height);
			}
			dynamic_cast<oit::RendererLinkedList*>(renderer)->SetSortingAlgorithm(oit::RendererLinkedList::SortingAlgorithm::Bubble);
			break;
		case lap::wic::Key::NUM6:
			if (!dynamic_cast<oit::RendererLinkedList*>(renderer)) {
				if (renderer) delete renderer;
				renderer = new oit::RendererLinkedList();
				renderer->SetScene(scene);
				renderer->SetCamera(camera);
				renderer->SetProjection(75.0f, 1.0f, 5000.0f);
				renderer->SetRasterizationMode(oit::detail::RASTERIZATION_FILL);
				renderer->SetOverrideObjectTransparency(toggle_override_transparency);
				renderer->SetResolution(wnd.getWindowProperties().width, wnd.getWindowProperties().height);
			}
			dynamic_cast<oit::RendererLinkedList*>(renderer)->SetSortingAlgorithm(oit::RendererLinkedList::SortingAlgorithm::Selection);

			break;
		case lap::wic::Key::NUM7:
			if (!dynamic_cast<oit::RendererLinkedList*>(renderer)) {
				if (renderer) delete renderer;
				renderer = new oit::RendererLinkedList();
				renderer->SetScene(scene);
				renderer->SetCamera(camera);
				renderer->SetProjection(75.0f, 1.0f, 5000.0f);
				renderer->SetRasterizationMode(oit::detail::RASTERIZATION_FILL);
				renderer->SetOverrideObjectTransparency(toggle_override_transparency);
				renderer->SetResolution(wnd.getWindowProperties().width, wnd.getWindowProperties().height);
			}
			dynamic_cast<oit::RendererLinkedList*>(renderer)->SetSortingAlgorithm(oit::RendererLinkedList::SortingAlgorithm::Insertion);
			break;
		case lap::wic::Key::NUM8:
			if (!dynamic_cast<oit::RendererLinkedList*>(renderer)) {
				if (renderer) delete renderer;
				renderer = new oit::RendererLinkedList();
				renderer->SetScene(scene);
				renderer->SetCamera(camera);
				renderer->SetProjection(75.0f, 1.0f, 5000.0f);
				renderer->SetRasterizationMode(oit::detail::RASTERIZATION_FILL);
				renderer->SetOverrideObjectTransparency(toggle_override_transparency);
				renderer->SetResolution(wnd.getWindowProperties().width, wnd.getWindowProperties().height);
			}
			dynamic_cast<oit::RendererLinkedList*>(renderer)->SetSortingAlgorithm(oit::RendererLinkedList::SortingAlgorithm::Shell);
			break;
		case lap::wic::Key::NUM9:
			if (!dynamic_cast<oit::RendererLinkedList*>(renderer)) {
				if (renderer) delete renderer;
				renderer = new oit::RendererLinkedList();
				renderer->SetScene(scene);
				renderer->SetCamera(camera);
				renderer->SetProjection(75.0f, 1.0f, 5000.0f);
				renderer->SetRasterizationMode(oit::detail::RASTERIZATION_FILL);
				renderer->SetOverrideObjectTransparency(toggle_override_transparency);
				renderer->SetResolution(wnd.getWindowProperties().width, wnd.getWindowProperties().height);
			}
			dynamic_cast<oit::RendererLinkedList*>(renderer)->SetSortingAlgorithm(oit::RendererLinkedList::SortingAlgorithm::Quick);
			break;
		case lap::wic::Key::NUM0:
			if (!dynamic_cast<oit::RendererLinkedList*>(renderer)) {
				if (renderer) delete renderer;
				renderer = new oit::RendererLinkedList();
				renderer->SetScene(scene);
				renderer->SetCamera(camera);
				renderer->SetProjection(75.0f, 1.0f, 5000.0f);
				renderer->SetRasterizationMode(oit::detail::RASTERIZATION_FILL);
				renderer->SetOverrideObjectTransparency(toggle_override_transparency);
				renderer->SetResolution(wnd.getWindowProperties().width, wnd.getWindowProperties().height);
			}
			dynamic_cast<oit::RendererLinkedList*>(renderer)->SetSortingAlgorithm(oit::RendererLinkedList::SortingAlgorithm::Merge);
			break;
		case lap::wic::Key::G:
			gui_toggle = (gui_toggle + 1) % 4;
			break;
		case lap::wic::Key::T:
			toggle_override_transparency = !toggle_override_transparency;
			renderer->SetOverrideObjectTransparency(toggle_override_transparency);
			break;
		case lap::wic::Key::O:
			{
				static bool toggle_occupancy = false;
				if (dynamic_cast<oit::RendererOccupancyMap*>(renderer)) {
					toggle_occupancy = !toggle_occupancy;
					dynamic_cast<oit::RendererOccupancyMap*>(renderer)->ShowOccupancy(toggle_occupancy);
				}
			}
			break;
		case lap::wic::Key::MINUS:
			if (distance == 100.0f) distance = 10.0f; 
			else distance = 1.0f;
			break;
		case lap::wic::Key::EQUAL:
			if (distance == 10.0f) distance = 100.0f; 
			else distance = 10.0f;
			break;
		case lap::wic::Key::LEFT:
		case lap::wic::Key::A:
			camera->TranslateRight(-distance);
			break;
		case lap::wic::Key::RIGHT:
		case lap::wic::Key::D:
			camera->TranslateRight(distance);
			break;
		case lap::wic::Key::UP:
		case lap::wic::Key::W:
			camera->TranslateForward(distance);
			break;
		case lap::wic::Key::DOWN:
		case lap::wic::Key::S:
			camera->TranslateForward(-distance);
			break;
		case lap::wic::Key::R:
			camera->TranslateUpword(distance);
			break;
		case lap::wic::Key::F:
			camera->TranslateUpword(-distance);
			break;
		case lap::wic::Key::F1:
			camera->SetView(glm::vec3(0, 0, 40), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
			break;
		case lap::wic::Key::F2:
			camera->SetView(glm::vec3(1272.17, 343.0f, -2639.22f), glm::vec3(1271.27, 343.0f, -2639.57f), glm::vec3(0, 1, 0));
			break;
		case lap::wic::Key::F3:
			camera->SetView(glm::vec3(1222.93f, 693.0f, -716.75f), glm::vec3(1222.39f, 693.0f, -717.55f), glm::vec3(0, 1, 0));
			break;
		case lap::wic::Key::F4:
			camera->SetView(glm::vec3(911.08f, 200.0f, -639.67f), glm::vec3(910.6f, 200.0f, -640.53f), glm::vec3(0, 1, 0));
			break;
		case lap::wic::Key::F5:
			camera->SetView(glm::vec3(-275.79, 700.0f, -454.67f), glm::vec3(-275.16, 700.0f, -455.29), glm::vec3(0, 1, 0));
			break;
		case lap::wic::Key::F6:
			{
				static bool toggle_fullscreen = false;
				toggle_fullscreen = !toggle_fullscreen;
				static unsigned int oldw, oldh, oldpx, oldpy;
				if (toggle_fullscreen) {
					oldw = wnd.getWindowProperties().width;
					oldh = wnd.getWindowProperties().height;
					oldpx = wnd.getWindowProperties().position_x;
					oldpy = wnd.getWindowProperties().position_y;
					wnd.setFullscreen();
				}
				else {
					wnd.setWindowed(oldw, oldh, oldpx, oldpy);
				}
				break;
			}
		case lap::wic::Key::F7:
			{
				static bool value = true;
				value = !value;
				if(!value) wnd.setSwapInterval(0);	//none
				else wnd.setSwapInterval(-1);		//adaptive
				break;
			}
		case lap::wic::Key::F8:
		{
			static bool value = true;
			value = !value;
			asset_manager.Clear();
			if (!value) CreateBasicScene();
			else CreateComplexScene();

			scene->SetCamera(camera);
			camera->SetView(glm::vec3(0, 0, 40), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

			break;
		}
		default:
			break;
		}
	}
	void NotifyKeyRelease(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
	}
	void NotifyKeyRepeat(lap::wic::Window& wnd, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
		NotifyKeyPress(wnd, key, alt, control, shift, system, state, timestamp);
	}
	void NotifyMousePress(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
	}
	void NotifyMouseRelease(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
	}
	void NotifyMouseDrag(lap::wic::Window& wnd, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
	}
	void NotifyMouseMove(lap::wic::Window& wnd, unsigned int posx, unsigned int posy, const lap::wic::InputState& state, uint64_t timestamp) {
		float dx = wnd.getWindowProperties().width / 2.0f - (float)posx;
		float dy = wnd.getWindowProperties().height / 2.0f - (float)posy;
		if (abs(dx) < 2 && abs(dy) < 2) return;
		camera->RotateY(dx / 1000.0f);
		camera->RotateX(dy / 1000.0f);
	}
	void NotifyMouseScroll(lap::wic::Window& wnd, float scrollx, float scrolly, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
	}
};

int main(){
	//window-input-context system + a window.
	lap::wic::WICSystem wicsystem(&std::cout, false);
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.title = "Rasterized Order Independent Transparency";
	wp.width = 1280;		wp.height = 720;
	wp.position_x = 300;	wp.position_y = 100;
	fp.samples_per_pixel = 1;	//a single sample per pixel
	cp.swap_interval = -1;
	ip.cursor_enabled = false;	//cursor is not visible and always reverts to middle of the screen after movement
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);

	//app object
	Application app(window.getWindowProperties().width, window.getWindowProperties().height);
	using namespace std::placeholders;
	window.setCallbackFramebufferResize(std::bind(&Application::NotifyResize, &app, _1, _2, _3, _4));
	window.setCallbackKeyPress(std::bind(&Application::NotifyKeyPress, &app, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRelease(std::bind(&Application::NotifyKeyRelease, &app, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackKeyRepeat(std::bind(&Application::NotifyKeyRepeat, &app, _1, _2, _3, _4, _5, _6, _7, _8));
	window.setCallbackMousePress(std::bind(&Application::NotifyMousePress, &app, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseRelease(std::bind(&Application::NotifyMouseRelease, &app, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseDrag(std::bind(&Application::NotifyMouseDrag, &app, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10));
	window.setCallbackMouseMove(std::bind(&Application::NotifyMouseMove, &app, _1, _2, _3, _4, _5));
	window.setCallbackMouseScroll(std::bind(&Application::NotifyMouseScroll, &app, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11));

	//main loop
	while (window.isOpened()) {
		app.Render();
		window.swapBuffers();
		window.processEvents();
		wicsystem.processProgramEvents();
	};


	return 0;
}