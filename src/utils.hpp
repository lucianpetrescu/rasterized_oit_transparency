//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

#pragma once
//------------------------------------------------------------------------------------------------
#if defined(_WIN64) || defined(_WIN32) || defined(WIN32) || defined(__WIN32__)
#define OIT_WINDOWS
#ifndef WIN32_LEAN_AND_MEAN
	#define WIN32_LEAN_AND_MEAN
#endif
#ifdef _MSC_VER
	#define VC_EXTRALEAN
#endif
#ifdef APIENTRY
#undef APIENTRY  
#endif
#include <windows.h>	
#elif defined(__APPLE__)
#define OIT_APPLE
#else
#define OIT_UNIX
#endif
#include "../dependencies/lap_wic/lap_wic.hpp"
#include "../dependencies/glm/glm.hpp"
#include "../dependencies/glm/gtc/type_ptr.hpp"
#include "../dependencies/glm/gtc/matrix_transform.hpp"
#include "../dependencies/glm/gtx/quaternion.hpp"
#include <string>
#include <vector>
#include <sstream>
#include <chrono>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <map>

namespace oit {

	//------------------------------------------------------------------------------------------------
	static float ToFloat(const std::string &source) {
		return std::stof(source);
	}
	static unsigned int ToUint(const std::string &source) {
		return std::stoul(source);
	}
	static int ToInt(const std::string &source) {
		return std::stoi(source);
	}
	static void Tokenize(const std::string &source, std::vector<std::string>& result) {
		result.clear();
		std::string aux = source;
		for (unsigned int i = 0; i<aux.size(); i++) if (aux[i] == '\t' || aux[i] == '\n') aux[i] = ' ';
		std::stringstream ss(aux, std::ios::in);
		while (ss.good()) {
			std::string s;
			ss >> s;
			if (s.size()>0) result.push_back(s);
		}
	}

	//------------------------------------------------------------------------------------------------
	//------------------------------------------------------------------------------------------------
	//timer
	class Timer {
	public:
		Timer()
		{
#if defined(OIT_WINDOWS)
			LARGE_INTEGER li;
			BOOL result = QueryPerformanceFrequency(&li);
			assert(result && "query performance frequency failed");
			mPCfreq = double(li.QuadPart) / 1000000.0;
#endif
			Reset();
		}
		void Reset()
		{
#if defined(OIT_WINDOWS)
			LARGE_INTEGER li;
			QueryPerformanceCounter(&li);
			mStart = li.QuadPart;
#else
			mStart = std::chrono::high_resolution_clock::now();
#endif
		}
		int64_t Elapsed()
		{
#if defined(OIT_WINDOWS)
			LARGE_INTEGER li;
			QueryPerformanceCounter(&li);
			mCurrent = li.QuadPart;
			return (int64_t)((mCurrent - mStart) / mPCfreq);
#else
			mCurrent = std::chrono::high_resolution_clock::now();
			return std::chrono::duration_cast<std::chrono::microseconds>(mCurrent - mStart).count();
#endif
		}
		int64_t ElapsedSinceLastCall()
		{
#if defined(OIT_WINDOWS)
			LARGE_INTEGER li;
			QueryPerformanceCounter(&li);
			int64_t rightNow = (int64_t)li.QuadPart;
			int64_t result = (int64_t)((rightNow - mCurrent) / mPCfreq);
			mCurrent = rightNow;
			return result;
#else
			auto rightNow = std::chrono::high_resolution_clock::now();
			auto result = rightNow - mCurrent;
			mCurrent = rightNow;
			return std::chrono::duration_cast<std::chrono::microseconds>(result).count();
#endif
		}
		float ElapsedSeconds() { return Elapsed() / 1000000.0f; }
		float ElapsedMilliSeconds() { return Elapsed() / 1000.0f; }
		float ElapseMicroSeconds() { return Elapsed() / 1.0f; }
		float ElapsedSinceLastCallSeconds() { return ElapsedSinceLastCall() / 1000000.0f; }
		float ElapsedSinceLastCallMilliSeconds() { return ElapsedSinceLastCall() / 1000.0f; }
		float ElapsedSinceLastCallMicroSeconds() { return ElapsedSinceLastCall() / 1.0f; }
	private:
		double mPCfreq;
#if defined(OIT_WINDOWS)
		int64_t mStart;
		int64_t mCurrent;
#else
		std::chrono::high_resolution_clock::time_point mCurrent;
		std::chrono::high_resolution_clock::time_point mStart;
#endif
	};


	//------------------------------------------------------------------------------------------------
	namespace detail {
		const unsigned int LOGGER_LEVEL_CRITICAL = 1000;
		const unsigned int LOGGER_LEVEL_NORMAL = 1001;
		const unsigned int LOGGER_LEVEL_VERBOSE = 1002;
		struct LoggerLevel {
			LoggerLevel() { level = LOGGER_LEVEL_NORMAL; }
			LoggerLevel(unsigned int value) { level = value; }
			LoggerLevel& operator=(const LoggerLevel& rhs) { level = rhs.level; return (*this); }
			bool operator==(const LoggerLevel& rhs) { return (level == rhs.level); }
			bool operator==(const LoggerLevel& rhs) const { return (level == rhs.level); }
			unsigned int level;
		};
	}

	///use this for critical log events like not finding a file
	const static detail::LoggerLevel LOGGER_CRITICAL(detail::LOGGER_LEVEL_CRITICAL);
	///use this for major operations (asset loading and asset processing)
	const static detail::LoggerLevel LOGGER_NORMAL(detail::LOGGER_LEVEL_NORMAL);
	///use this for everything else
	const static detail::LoggerLevel LOGGER_VERBOSE(detail::LOGGER_LEVEL_VERBOSE);

	namespace detail {
		class LoggerImplementation {
		public:
			LoggerImplementation() {
				level = LOGGER_LEVEL_NORMAL;
				input_level = LOGGER_LEVEL_NORMAL;
				Set(&std::cout, true, "oit.log", true);
			}
			~LoggerImplementation() {
				Close();
			}
			void Set(std::ostream* console, bool logToConsole, const std::string &file, bool logToFile) {
				SetConsole(console);
				SetWriteToConsole(logToConsole);
				SetFile(file);
				SetWriteToFile(logToFile);
			}
			void SetFile(const std::string &filename) {
				if (file_stream) {
					file_stream->close();
					delete file_stream;
				}
				if (filename != "") file_stream = new std::ofstream(filename.c_str(), std::ios::out);
			}
			void SetConsole(std::ostream *console) { console_stream = console; }
			void Close() {
				if (file_stream) {
					file_stream->close();
					delete file_stream;
				}
				file_stream = nullptr;
				console_stream = nullptr;
			}
			std::ofstream* GetFile() { return file_stream; }
			std::ostream* GetConsole() { return console_stream; }
			void SetLevel(const LoggerLevel& level) {
				if (level == LOGGER_CRITICAL || level == LOGGER_NORMAL || level == LOGGER_VERBOSE) this->level = level;
				else assert(false && "[Logger] not a supported log level");
			}
			LoggerLevel GetLevel() { return level; }
			void SetWriteToConsole(bool value) { write_to_console = value; }
			bool GetWriteToConsole() { return write_to_console; }
			void SetWriteToFile(bool value) { write_to_file = value; }
			bool GetWriteToFile() { return write_to_file; }

			void _InternalSetInputLevel(const LoggerLevel& input_level) {
				this->input_level = input_level;
			}
			bool _InternalValidInputLevel() {
				if (input_level.level <= level.level) return true;
				return false;
			}

		private:
			LoggerLevel level;
			bool write_to_console;
			bool write_to_file;
			std::ofstream *file_stream;
			std::ostream *console_stream;
			LoggerLevel input_level;
		};
	}
	static detail::LoggerImplementation Logger;

	template<typename T>
	detail::LoggerImplementation& operator<<(detail::LoggerImplementation& logger, const T &value) {
		if (logger._InternalValidInputLevel()) {
			if (logger.GetConsole() && logger.GetWriteToConsole()) (*logger.GetConsole()) << value;
			if (logger.GetFile() && logger.GetWriteToFile()) (*logger.GetFile()) << value;
		}
		return logger;
	}
	static detail::LoggerImplementation& operator<<(detail::LoggerImplementation& logger, std::ostream& (*manipulator)(std::ostream&))
	{
		if (logger._InternalValidInputLevel()) {
			if (logger.GetConsole() && logger.GetWriteToConsole()) manipulator((*logger.GetConsole()));
			if (logger.GetFile() && logger.GetWriteToFile()) manipulator((*logger.GetFile()));
		}
		return logger;
	}
	static detail::LoggerImplementation& operator<<(detail::LoggerImplementation& logger, const detail::LoggerLevel& level)
	{
		logger._InternalSetInputLevel(level);
		return logger;
	}
}
