//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

#include "asset_manager.hpp"

namespace oit {

	//--------------------------------------------------------------------------------------------
	//creates an unmanaged GPUMesh, with unmanaged GPUBuffers, considering the entire obj file as a single mesh, discards materials
	GPUMesh* CreateMeshFromFile(const std::string& filename) {
		std::vector<Vertex> vertices;
		std::vector<unsigned int> indices;
		std::vector<detail::ObjMaterial> materials;
		std::vector<detail::ObjMesh> meshes;
		detail::AssetLoadOBJFile(filename, vertices, indices, materials, meshes);

		//link into a single object
		unsigned int total_indices = 0;
		AABB aabb;
		for (auto& m : meshes) {
			total_indices += m.index_count;
			aabb.AddAABB(m.aabb);
		}

		//create
		GPUBuffer* buffer_vertex = new GPUBuffer((float*)&vertices[0], (int)vertices.size() * sizeof(Vertex), detail::BUFFER_USAGE_GPUSTATIC);
		GPUBuffer* buffer_index = new GPUBuffer((unsigned int*)&indices[0], (int)indices.size() * sizeof(unsigned int), detail::BUFFER_USAGE_GPUSTATIC);
		GPUMesh* mesh = new GPUMesh(buffer_vertex, buffer_index, 0, total_indices, aabb, detail::TOPOLOGY_TRIANGLES);
		return mesh;
	}

	//--------------------------------------------------------------------------------------------
	//creates an unmanaged GPUTexture
	GPUTexture* CreateTextureFromFile(const std::string& filename) {

		std::vector<unsigned char> data;
		unsigned int width, height;
		detail::AssetLoadBMPFile(filename, width, height, data);

		GPUTexture* texture = new GPUTexture();
		texture->Create(&data[0], width, height, 3, true);
		return texture;
	}

	//--------------------------------------------------------------------------------------------
	//asset managers manage the assets in a scene, it can load OBJ+MTL scenes, in-code generated scenes
	//it can also save/load in a prorpietary binary format
	//it can also create managed objects which will be c
	AssetManager::AssetManager() {
		scene = nullptr;
	}
	AssetManager::~AssetManager() {
		Clear();
	}

	//--------------------------------------------------------------------------------------------
	//deletes all managed assets
	void AssetManager::Clear() {
		if (scene) {
			delete scene;
			scene = nullptr;
		}
		if (buffers.size() > 0) {
			for (auto& b : buffers)	if (b.first) delete b.first;
			buffers.clear();
		}
		if (meshes.size() > 0) {
			for (auto& m : meshes) if (m.first) delete m.first;
			meshes.clear();
		}
		if (textures.size() > 0) {
			for (auto& t : textures) if (t.first) delete t.first;
			textures.clear();
		}
		if (materials.size() > 0) {
			for (auto& m : materials) if (m.first) delete m.first;
			materials.clear();
		}
		if (entities.size() > 0) {
			for (auto& e : entities) if (e.first) delete e.first;
			entities.clear();
		}
		Logger << LOGGER_NORMAL << "[AssetManager] Cleared scene." << std::endl;
	}

	//--------------------------------------------------------------------------------------------
	//clears scene and loads from file
	Scene* AssetManager::LoadScenePropietary(const std::string& filename) {
		//clear existing data
		Clear();

		return nullptr;
	}

	//--------------------------------------------------------------------------------------------
	//saves current scene and all assets (even the ones that are unrefereced by the scene) to file
	void AssetManager::SaveSceneProprietary(const std::string& filename) {
		assert(true && "TODO");
	}

	//--------------------------------------------------------------------------------------------
	//clears all assets, destroys the previous scene if any existed and loads an obj(mtl optional) as scene
	Scene* AssetManager::LoadSceneObj(const std::string& obj_filename) {
		//clear existing data
		Clear();
		Timer timer;
		Logger << LOGGER_NORMAL << "------------------------------------------------------------------------" << std::endl;
		Logger << LOGGER_NORMAL << "[AssetManager] Starting to load scene from " << obj_filename << " file." << std::endl;

		//load objs
		std::vector<Vertex> vertices;
		std::vector<unsigned int> indices;
		std::vector<detail::ObjMesh> obj_meshes;
		std::vector<detail::ObjMaterial> obj_materials;
		detail::AssetLoadOBJFile(obj_filename, vertices, indices, obj_materials, obj_meshes);

		//load all referenced textures
		std::string texture_path;
		unsigned int found = (int)obj_filename.find_last_of("/\\");
		if (found == std::string::npos) texture_path = "";
		else texture_path = obj_filename.substr(0, found + 1);	// include the slash or backslash
		std::vector<std::string> texture_files;
		for (auto& m : obj_materials) {
			if (m.alphamap_file != "")			texture_files.push_back(texture_path + m.alphamap_file);
			if (m.colormap_file != "")			texture_files.push_back(texture_path + m.colormap_file);
			if (m.displacementmap_file != "")	texture_files.push_back(texture_path + m.displacementmap_file);
			if (m.normalmap_file != "")			texture_files.push_back(texture_path + m.normalmap_file);
			if (m.specularmap_file != "")		texture_files.push_back(texture_path + m.specularmap_file);
		}
		for (auto& tf : texture_files) {
			if (!GetTexture(tf)) AddTexture(CreateTextureFromFile(tf), tf);
		}

		//create the materials
		std::vector<GPUMaterial*> new_materials; new_materials.resize(obj_materials.size());
		for (unsigned int i = 0; i < new_materials.size(); i++) {
			new_materials[i] = new GPUMaterial();
			new_materials[i]->Add(obj_materials[i].ambiental, "mat_ambient");
			new_materials[i]->Add(glm::vec3(obj_materials[i].diffuse_transparency.x, obj_materials[i].diffuse_transparency.y, obj_materials[i].diffuse_transparency.z), "mat_diffuse");
			new_materials[i]->Add(obj_materials[i].diffuse_transparency.w, "mat_transparency");
			new_materials[i]->SetTransparency(obj_materials[i].diffuse_transparency.w);
			new_materials[i]->Add(obj_materials[i].specular_exponent, "mat_specular_exponent");
			//materials[i]->Add(obj_materials[i].transmission, "mat_transmission");
			if (obj_materials[i].alphamap_file != "") {
				new_materials[i]->Add(GetTexture(texture_path + obj_materials[i].alphamap_file), 0);
				new_materials[i]->SetAlphaCulled(true);
			}
			if (obj_materials[i].colormap_file != "")		new_materials[i]->Add(GetTexture(texture_path + obj_materials[i].colormap_file), 1);
			if (obj_materials[i].displacementmap_file != "")new_materials[i]->Add(GetTexture(texture_path + obj_materials[i].displacementmap_file), 2);
			if (obj_materials[i].normalmap_file != "")		new_materials[i]->Add(GetTexture(texture_path + obj_materials[i].normalmap_file), 3);
			if (obj_materials[i].specularmap_file != "")	new_materials[i]->Add(GetTexture(texture_path + obj_materials[i].specularmap_file), 4);
		}


		//create 2 hardware buffers that will be used by all the submeshes.
		unsigned int total_indices = 0;
		for (auto& m : obj_meshes) {
			total_indices += m.index_count;
		}
		GPUBuffer* buffer_vertex = new GPUBuffer((float*)&vertices[0], (int)vertices.size() * sizeof(Vertex), detail::BUFFER_USAGE_GPUSTATIC);
		GPUBuffer* buffer_index = new GPUBuffer((unsigned int*)&indices[0], (int)indices.size() * sizeof(unsigned int), detail::BUFFER_USAGE_GPUSTATIC);

		//create the scene and add many scene nodes, each with an entity that contains the mesh + material combination
		Scene* scene = new Scene;
		for (auto& m : obj_meshes) {
			GPUMesh* new_mesh = new GPUMesh(buffer_vertex, buffer_index, m.index_first, m.index_count, m.aabb, detail::TOPOLOGY_TRIANGLES);
			GPUEntity* new_entity = new GPUEntity(new_mesh, new_materials[m.index_material]);
			SceneNode* new_node = new SceneNode();
			new_node->AddEntity(new_entity);
			scene->AddSceneNode(new_node);
		}
		AddScene(scene);

		Logger << LOGGER_NORMAL << "[AssetManager] Finished loading scene from " << obj_filename << ", time = " << timer.ElapsedSeconds() << " seconds." << std::endl;
		return scene;
	}

	//--------------------------------------------------------------------------------------------
	//clears all assets, destroys the previous scene if any existed abd loads a manually created scene
	void AssetManager::LoadScene(Scene* scene) {
		Clear();
		Timer timer;
		Logger << LOGGER_NORMAL << "------------------------------------------------------------------------" << std::endl;
		Logger << LOGGER_NORMAL << "[AssetManager] Starting to load scene from memory." << std::endl;

		AddScene(scene);

		Logger << LOGGER_NORMAL << "[AssetManager] Finished loading scene from memory, time = " << timer.ElapsedSeconds() << " seconds." << std::endl;
	}

	//--------------------------------------------------------------------------------------------
	//private registration functions
	GPUBuffer* AssetManager::GetBuffer(const std::string& name) {
		for (auto& b : buffers) if (b.second == name) return b.first;
		return nullptr;
	}
	GPUTexture* AssetManager::GetTexture(const std::string& name) {
		for (auto& t : textures) if (t.second == name) return t.first;
		return nullptr;
	}
	GPUMaterial* AssetManager::GetMaterial(const std::string& name) {
		for (auto& m : materials) if (m.second == name) return m.first;
		return nullptr;
	}
	GPUMesh* AssetManager::GetMesh(const std::string& name) {
		for (auto& m : meshes) if (m.second == name) return m.first;
		return nullptr;
	}
	GPUEntity* AssetManager::GetEntity(const std::string& name) {
		for (auto& e : entities) if (e.second == name) return e.first;
		return nullptr;
	}
	void AssetManager::AddBuffer(GPUBuffer* buffer, const std::string& name) {
		assert(buffer && "[AssetManager] adding a null buffer.");
		for (auto& b : buffers) if (b.first == buffer) return;
		std::string namekey = name;
		if (name == "") namekey = std::to_string((size_t)buffer);
		buffers[buffer] = namekey;
	}
	void AssetManager::AddMesh(GPUMesh* mesh, const std::string& name) {
		assert(mesh && "[AssetManager] adding a null mesh.");
		for (auto& m : meshes) if (m.first == mesh) return;
		std::string namekey = name;
		if (name == "") namekey = std::to_string((size_t)mesh);
		meshes[mesh] = namekey;
		AddBuffer(mesh->GetBufferIndex());
		AddBuffer(mesh->GetBufferVertex());
	}
	void AssetManager::AddMaterial(GPUMaterial* material, const std::string& name ) {
		assert(material && "[AssetManager] adding a null material.");
		for (auto& m : materials) if (m.first == material) return;
		std::string namekey = name;
		if (name == "") namekey = std::to_string((size_t)material);
		materials[material] = namekey;
		for (auto& m : material->GetTextures()) AddTexture(m.first);
	}
	void AssetManager::AddTexture(GPUTexture* texture, const std::string& name) {
		assert(texture && "[AssetManager] adding a null texture.");
		for (auto& t : textures) if (t.first == texture) return;
		std::string namekey = name;
		if (name == "") namekey = std::to_string((size_t)texture);
		textures[texture] = namekey;
	}
	void AssetManager::AddEntity(GPUEntity* entity, const std::string& name) {
		assert(entity && "[AssetManager] adding a null entity.");
		for (auto& e : entities) if (e.first == entity) return;
		std::string namekey = name;
		if (name == "") namekey = std::to_string((size_t)entity);
		entities[entity] = namekey;
		AddMesh(entity->GetMesh());
		AddMaterial(entity->GetMaterial());
		if (entity->GetInstanceCount() > 1) AddBuffer(entity->GetInstanceBuffer());
	}
	void AssetManager::AddScene(Scene* scene) {
		this->scene = scene;
		std::list<SceneNode*> nodelist;
		for (auto& c : scene->GetChildren()) nodelist.push_back(c);
		while (nodelist.size() > 0) {
			for (auto& c : nodelist.front()->GetChildren()) nodelist.push_back(c);
			for (auto& e : nodelist.front()->GetEntities()) AddEntity(e);
			nodelist.pop_front();
		}
	}

}