//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

#include "asset_image.hpp"

namespace oit {
	namespace detail {
		void AssetLoadBMPFile(const std::string &filename, unsigned int &width, unsigned int &height, std::vector<unsigned char> &data) {
			Timer timer;
			struct header {
				unsigned char type[2];
				int f_lenght;
				short rezerved1;
				short rezerved2;
				int offBits;
			};
			struct header_info {
				int size;
				int width;
				int height;
				short planes;
				short bitCount;
				int compresion;
				int sizeImage;
				int xPelsPerMeter;
				int yPelsPerMeter;
				int clrUsed;
				int clrImportant;
			};

			std::ifstream file(filename.c_str(), std::ios::in | std::ios::binary);
			if (!file.good()) {
				Logger << LOGGER_CRITICAL << "[AssetImage]: Could not find BMP file " << filename << " or lacking reading rights." << std::endl;
				std::terminate();
				return;
			}

			header h; header_info h_info;
			file.read((char*)&h, sizeof(unsigned char) * 2 + sizeof(int) + sizeof(short) * 2 + sizeof(int));
			file.read((char*)&h_info, sizeof(header_info));

			data.resize(h_info.width*h_info.height * 3);

			long padd = 0;
			if ((h_info.width * 3) % 4 != 0) padd = 4 - (h_info.width * 3) % 4;

			width = h_info.width;
			height = h_info.height;

			long pointer;
			unsigned char* line = new unsigned char[3 * width];
			for (unsigned int i = 0; i < height; i++)
			{
				file.read((char*)line, 3 * width);
				for (unsigned int j = 0; j < width; j++)
				{
					pointer = (i*width + j) * 3;
					data[pointer] = line[j * 3 + 2];
					data[pointer + 1] = line[j * 3 + 1];
					data[pointer + 2] = line[j * 3 + 0];
				}
				file.seekg(padd, std::ios_base::cur);
			}
			delete[] line;
			file.close();
			Logger << LOGGER_NORMAL << "[AssetImage]: Loaded file " << filename << " in " << timer.ElapsedSeconds() << " seconds." << std::endl;
		}
	}

	//--------------------------------------------------------------------------------------------
	void ImageFlipY(unsigned int width, unsigned int height, unsigned int num_channels, std::vector<unsigned char> &data) {
		Timer timer;
		for (unsigned int i = 0; i<width; i++) {
			for (unsigned int j = 0; j<height / 2; j++) {
				for (unsigned int k = 0; k<num_channels; k++) std::swap(data[(j*width + i)*num_channels + k], data[((height - 1 - j)*width + i)*num_channels + k]);
			}
		}
		Logger << LOGGER_NORMAL << "[AssetImage]: flipped image in " << timer.ElapsedSeconds() << " seconds." << std::endl;
	}

	//--------------------------------------------------------------------------------------------
	void ImageFlipX(unsigned int width, unsigned int height, unsigned int num_channels, std::vector<unsigned char> &data) {
		assert(false && "TODO");
	}
}