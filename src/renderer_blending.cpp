//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
// standard pipeline interpolative blending

#include "renderer_blending.hpp"

namespace oit {
	RendererBlendingDefault::RendererBlendingDefault() {
		override_object_transparency = false;
		scene = nullptr;
		camera = nullptr;
		shader = new oit::GPUProgram("../shaders/blending_default.vert", "../shaders/blending_default.frag");
		SetTextureFiltering(detail::TEXTURE_FILTER_TRILINEAR_ANISOTROPIC);
		SetRasterizationMode(detail::RASTERIZATION_FILL);
		SetProjection(75.0f, 1.0f, 10000.0f);
		SetResolution(800, 600);
		background_color = glm::vec4(0.5, 0.5, 0.5, 0);
		//background_color = glm::vec4(0, 0, 0, 0);
		glClearColor(background_color.x, background_color.y, background_color.z, background_color.w);
	}
	RendererBlendingDefault::~RendererBlendingDefault() {
		delete shader;
	}
	void RendererBlendingDefault::SetProjection(float fovy, float clip_near, float clip_far)  {
		projection.fovy = fovy;
		projection.clip_near = clip_near;
		projection.clip_far = clip_far;
	}
	void RendererBlendingDefault::SetTextureFiltering(unsigned int mode)  {
		texture_filter.Set(mode);
		for (unsigned int i = 0; i < 10; i++) texture_filter.Bind(i);
		texture_filter_mode = mode;
	}
	void RendererBlendingDefault::SetRasterizationMode(unsigned int mode)  {
		if (mode == detail::RASTERIZATION_POINTS) glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
		else if (mode == detail::RASTERIZATION_WIREFRAME) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		else if (mode == detail::RASTERIZATION_FILL) glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		drawing_mode = mode;
	}
	GPUProgram* RendererBlendingDefault::GetGPUProgram() {
		return shader;
	}
	void RendererBlendingDefault::SetCamera(Camera* incamera)  {
		assert(incamera && "[RendererBlending] null camera.");
		this->camera = incamera;
	}
	void RendererBlendingDefault::SetScene(Scene* inscene)  {
		assert(inscene && "[RendererBlending] null scene.");
		this->scene = inscene;
	}
	Camera*RendererBlendingDefault::GetCamera() {
		return camera;
	}
	void RendererBlendingDefault::ReloadGPUPrograms()  {
		shader->Reload();
	}
	void RendererBlendingDefault::SetResolution(unsigned int width, unsigned int height)  {
		screen.width = width;
		screen.height = height;
		projection.matrix = glm::perspective(glm::radians(projection.fovy), (float)width / (float)height, projection.clip_near, projection.clip_far);
	}
	void RendererBlendingDefault::SetOverrideObjectTransparency(bool value) {
		override_object_transparency = value;
	}
	bool RendererBlendingDefault::GetOverrideObjectTransparency() {
		return override_object_transparency;
	}
	void RendererBlendingDefault::SetBackgroundColor(const glm::vec4& background_color) {
		this->background_color = background_color;
	}
	std::string RendererBlendingDefault::GetName()  {
		return "Blending";
	}

	//---------------------------------------------------------------------------------------
	void RendererBlendingDefault::Draw()  {
		SetTextureFiltering(detail::TEXTURE_FILTER_TRILINEAR_ANISOTROPIC);
		SetRasterizationMode(detail::RASTERIZATION_FILL);

		glClearColor(background_color.x, background_color.y, background_color.z, background_color.w);
		glViewport(0, 0, screen.width, screen.height);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glBlendEquationi(0, GL_FUNC_ADD);
		glBlendFunci(0, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		shader->Bind();
		shader->SetUniform("view_matrix", camera->GetViewMatrix(), false);
		shader->SetUniform("projection_matrix", projection.matrix, false);
		//collect scene nodes
		std::vector<SceneNode*> nodelist;
		scene->GetSceneNodes(nodelist);
		//for each node
		for (auto& node : nodelist) {
			shader->SetUniform("model_matrix", node->GetCurrentTransformation(), false);

			//draw all entities
			const std::vector<GPUEntity*> entities = node->GetEntities();
			for (auto& e : entities) {
				e->GetMaterial()->Bind(shader);
				if (override_object_transparency) shader->SetUniform("mat_transparency", 0.75f);
				e->GetMesh()->Draw();
			}
		}

		glDisable(GL_BLEND);
	}
}