//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


#include "gui.hpp"

namespace oit {
	namespace detail {
		///----------------------------------------------------------------------------------------------
		GuiVertexFormat::GuiVertexFormat() {
			position = texcoord = glm::vec2(0);
			color = glm::vec3(1, 1, 1);
		}
		GuiVertexFormat::GuiVertexFormat(const glm::vec2& position, const glm::vec2& texcoord, const glm::vec3& color) {
			this->position = position;
			this->texcoord = texcoord;
			this->color = color;
		}

		///----------------------------------------------------------------------------------------------
		GuiString::GuiString() {
			start_pos_x = start_pos_y = width = height = 0; text = "";
			glm::vec3 color = glm::vec3(1, 1, 1);
		}
		GuiString::GuiString(float start_pos_x, float start_pos_y, float width, float height, const std::string &text, const glm::vec3& color) {
			this->start_pos_x = start_pos_x;
			this->start_pos_y = start_pos_y;
			this->width = width;
			this->height = height;
			this->text = text;
			this->color = color;
		}
	}

	///----------------------------------------------------------------------------------------------
	Gui::Gui(const std::string& fontfile, const std::string& vertex_shader_file, const std::string& fragment_shader_file) {
		screen_width = screen_height = 0;
		this->fontfile = fontfile;

		//buffers
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glGenBuffers(1, &vbo);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		//texture
		std::vector<unsigned char> data;
		unsigned int width, height;
		detail::AssetLoadBMPFile(fontfile, width, height, data);
		texture.Create(&data[0], width, height, 3, false);

		//filter
		texture_filter.SetClamp();
		texture_filter.SetBilinear();

		//shader
		program.Set(vertex_shader_file, fragment_shader_file);
		vertices.reserve(1024);
	}
	Gui::~Gui() {
		glDeleteBuffers(1, &vbo);
		glDeleteVertexArrays(1, &vao);
	}
	void Gui::ReloadGPUProgram() {
		program.Reload();
	}
	void Gui::ReloadAssets() {
		std::vector<unsigned char> data;
		unsigned int width, height;
		detail::AssetLoadBMPFile(fontfile, width, height, data);
		texture.Create(&data[0], width, height, 3, false);
	}
	void Gui::SetResolution(unsigned int width, unsigned int height) {
		screen_width = (float)width;
		screen_height = (float)height;
	}
	void Gui::ClearText() {
		strings.clear();
		vertices.clear();
	}
	void Gui::AddText(float startposx, float startposy, float wperchar, float hperchar, std::string text, const glm::vec3& color) {
		detail::GuiString s;
		s.start_pos_x = startposx;
		s.start_pos_y = screen_height - startposy;
		s.width = wperchar;
		s.height = hperchar;
		s.text = text;
		s.color = color;
		strings.push_back(s);
	}
	void Gui::Draw() {
		program.Bind();
		texture.Bind(0);
		int previous_texture_filter;
		glGetIntegerv(GL_SAMPLER_BINDING, &previous_texture_filter);
		texture_filter.Bind(0);
		program.SetUniform("screen_resolution", screen_width, screen_height);

		//create content (it changes per frame anyways and this is a test so no point in losing time with optimization)
		vertices.clear();
		detail::GuiVertexFormat v1, v2, v3, v4;
		for (unsigned int i = 0; i<strings.size(); i++) {
			detail::GuiString s = strings[i];
			for (unsigned int k = 0; k<s.text.size(); k++) {
				//uv
				char ch = s.text[k];
				float uvx = (ch % 16) / 16.0f;
				float uvy = (ch / 16) / 16.0f - 2.0f / 16;

				v1.position = glm::vec2(s.start_pos_x + k*s.width, s.start_pos_y);					//down left
				v1.texcoord = glm::vec2(uvx, 1 - (uvy + 1.0f / 16.0f));
				v1.color = s.color;

				v2.position = glm::vec2(s.start_pos_x + (k + 1)*s.width, s.start_pos_y);			//down right
				v2.texcoord = glm::vec2(uvx + 1.0f / 16.0f, 1 - (uvy + 1.0f / 16.0f));
				v2.color = s.color;

				v3.position = glm::vec2(s.start_pos_x + (k + 1)*s.width, s.start_pos_y + s.height);	//up right
				v3.texcoord = glm::vec2(uvx + 1.0f / 16.0f, 1 - uvy);
				v3.color = s.color;

				v4.position = glm::vec2(s.start_pos_x + k*s.width, s.start_pos_y + s.height);		//up left
				v4.texcoord = glm::vec2(uvx, 1 - uvy);
				v4.color = s.color;

				vertices.push_back(v1); vertices.push_back(v2); vertices.push_back(v3);
				vertices.push_back(v3); vertices.push_back(v4); vertices.push_back(v1);
			}
		}

		//to gpu
		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(detail::GuiVertexFormat), &vertices[0], GL_DYNAMIC_DRAW);
		glVertexAttribPointer(0, 2, GL_FLOAT, 0, sizeof(detail::GuiVertexFormat), 0);
		glVertexAttribPointer(1, 2, GL_FLOAT, 0, sizeof(detail::GuiVertexFormat), (void*)(sizeof(glm::vec2)));
		glVertexAttribPointer(2, 3, GL_FLOAT, 0, sizeof(detail::GuiVertexFormat), (void*)(2 * sizeof(glm::vec2)));

		//draw
		glDisable(GL_DEPTH_TEST);
		glDrawArrays(GL_TRIANGLES, 0, (int)vertices.size());
		glEnable(GL_DEPTH_TEST);

		//bind the previous texture filter
		glBindSampler(0, previous_texture_filter);
	}
}