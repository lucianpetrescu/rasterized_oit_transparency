//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

#include "renderer_occupancy_map.hpp"

namespace oit {
		RendererOccupancyMap::RendererOccupancyMap() {
			override_object_transparency = false;
			scene = nullptr;
			camera = nullptr;
			shader_map = new oit::GPUProgram("../shaders/occupancy_map.vert", "../shaders/occupancy_map.frag");
			shader_render = new oit::GPUProgram("../shaders/occupancy_render.vert", "../shaders/occupancy_render.frag");
			shader_final = new oit::GPUProgram("../shaders/occupancy_final.vert", "../shaders/occupancy_final.frag");

			//fullscreen mesh
			std::vector<Vertex> vertices;
			Vertex v1;	v1.position = glm::vec3(-1, -1, 0);	v1.texcoord = glm::vec3(0, 0, 0);	vertices.push_back(v1);
			Vertex v2;	v2.position = glm::vec3(1, -1, 0);	v2.texcoord = glm::vec3(1, 0, 0);	vertices.push_back(v2);
			Vertex v3;	v3.position = glm::vec3(1, 1, 0);	v3.texcoord = glm::vec3(1, 1, 0);	vertices.push_back(v3);
			Vertex v4;	v4.position = glm::vec3(-1, 1, 0);	v4.texcoord = glm::vec3(0, 1, 0);	vertices.push_back(v4);
			std::vector<unsigned int> indices;
			indices.push_back(0);	indices.push_back(1);	indices.push_back(2);
			indices.push_back(2);	indices.push_back(3);	indices.push_back(0);
			AABB aabb;
			fullscreen.buffer_vertex = new GPUBuffer((float*)&vertices[0], (unsigned int)vertices.size() * sizeof(Vertex), detail::BUFFER_USAGE_GPUSTATIC);
			fullscreen.buffer_index = new GPUBuffer((unsigned int*)&indices[0], (unsigned int)indices.size() * sizeof(unsigned int), detail::BUFFER_USAGE_GPUSTATIC);
			fullscreen.mesh = new GPUMesh(fullscreen.buffer_vertex, fullscreen.buffer_index, 0, (unsigned int)indices.size(), aabb, detail::TOPOLOGY_TRIANGLES);

			//occupancy as images (no core bitwise OR op)
			texture_occupancy_map0 = new GPUTexture();
			texture_occupancy_map0->Create((unsigned int*)nullptr, 800, 600, 1, false);		//R32UI
			texture_occupancy_map1 = new GPUTexture();
			texture_occupancy_map1->Create((unsigned int*)nullptr, 800, 600, 1, false);		//R32UI
			texture_occupancy_map2 = new GPUTexture();
			texture_occupancy_map2->Create((unsigned int*)nullptr, 800, 600, 1, false);		//R32UI
			texture_occupancy_map3 = new GPUTexture();
			texture_occupancy_map3->Create((unsigned int*)nullptr, 800, 600, 1, false);		//R32UI

			//fullscreen framebuffer
			fullscreen.framebuffer1 = new GPUFramebuffer(800, 600);
			fullscreen.framebuffer2 = new GPUFramebuffer(800, 600);

			fullscreen.target_count = new GPUTexture();
			fullscreen.target_count->Create(nullptr, detail::DATA_TYPE_FLOAT, 800, 600, 4, false);	//RGBA16F ... no blending for integers!
			fullscreen.framebuffer1->AddBinding(fullscreen.target_count, 0);
			fullscreen.target_alpha = new GPUTexture();
			fullscreen.target_alpha->Create(nullptr, detail::DATA_TYPE_FLOAT, 800, 600, 1, false);	//R16F
			fullscreen.framebuffer1->AddBinding(fullscreen.target_alpha, 1);

			fullscreen.target_color = new GPUTexture();
			fullscreen.target_color->Create(nullptr, detail::DATA_TYPE_FLOAT, 800, 600, 4, false);	//RGBA32F
			fullscreen.framebuffer2->AddBinding(fullscreen.target_color, 0);


			//chosen pixel
			chosen_pixel.occupancy[0] = chosen_pixel.occupancy[1] = chosen_pixel.occupancy[2] = chosen_pixel.occupancy[3] = 0;
			chosen_pixel.posx = 400;  chosen_pixel.posy = 300;
			chosen_pixel.buffer = new GPUBuffer((float*)(&chosen_pixel.occupancy[0]), sizeof(unsigned int) * 4, detail::BUFFER_USAGE_CPU_GPU);


			SetTextureFiltering(detail::TEXTURE_FILTER_TRILINEAR_ANISOTROPIC);
			SetRasterizationMode(detail::RASTERIZATION_FILL);
			SetProjection(75.0f, 1.0f, 10000.0f);
			SetResolution(800, 600);
			background_color = glm::vec4(0.5, 0.5, 0.5, 0);
			//background_color = glm::vec4(0, 0, 0, 0);
			glClearColor(background_color.x, background_color.y, background_color.z, background_color.w);
			glClearDepth(1);
		}
		RendererOccupancyMap::~RendererOccupancyMap() {
			delete shader_map;
			delete shader_render;
			delete shader_final;

			delete texture_occupancy_map0;
			delete texture_occupancy_map1;
			delete texture_occupancy_map2;
			delete texture_occupancy_map3;

			delete fullscreen.buffer_index;
			delete fullscreen.buffer_vertex;
			delete fullscreen.mesh;

			delete fullscreen.target_count;
			delete fullscreen.target_color;
			delete fullscreen.target_alpha;
			delete fullscreen.framebuffer1;
			delete fullscreen.framebuffer2;

			delete chosen_pixel.buffer;
		}
		void RendererOccupancyMap::SetProjection(float fovy, float clip_near, float clip_far) {
			projection.fovy = fovy;
			projection.clip_near = clip_near;
			projection.clip_far = clip_far;
		}
		void RendererOccupancyMap::SetTextureFiltering(unsigned int mode) {
			texture_filter.Set(mode);
			for (unsigned int i = 0; i < 10; i++) texture_filter.Bind(i);
			texture_filter_mode = mode;
		}
		void RendererOccupancyMap::SetRasterizationMode(unsigned int mode) {
			if (mode == detail::RASTERIZATION_POINTS) glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
			else if (mode == detail::RASTERIZATION_WIREFRAME) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			else if (mode == detail::RASTERIZATION_FILL) glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			drawing_mode = mode;
		}
		void RendererOccupancyMap::SetCamera(Camera* incamera) {
			assert(incamera && "[RendererOccupancyMap] null camera.");
			this->camera = incamera;
		}
		void RendererOccupancyMap::SetScene(Scene* inscene) {
			assert(inscene && "[RendererOccupancyMap] null scene.");
			this->scene = inscene;
		}
		Camera* RendererOccupancyMap::GetCamera() {
			return camera;
		}
		void RendererOccupancyMap::ReloadGPUPrograms() {
			shader_map->Reload();
			shader_render->Reload();
			shader_final->Reload();
		}
		void RendererOccupancyMap::SetResolution(unsigned int width, unsigned int height) {
			screen.width = width;
			screen.height = height;
			projection.matrix = glm::perspective(glm::radians(projection.fovy), (float)width / (float)height, projection.clip_near, projection.clip_far);

			//resize occupancy map
			texture_occupancy_map0->Resize(width, height);
			texture_occupancy_map1->Resize(width, height);
			texture_occupancy_map2->Resize(width, height);
			texture_occupancy_map3->Resize(width, height);

			//resize framebuffer and all attached targets
			fullscreen.framebuffer1->Resize(width, height);
			fullscreen.framebuffer2->Resize(width, height);

			//chosen pixel
			chosen_pixel.posx = width / 2;
			chosen_pixel.posy = height / 2;
		}
		void RendererOccupancyMap::SetOverrideObjectTransparency(bool value) {
			override_object_transparency = value;
		}
		bool RendererOccupancyMap::GetOverrideObjectTransparency() {
			return override_object_transparency;
		}
		void RendererOccupancyMap::SetBackgroundColor(const glm::vec4& background_color) {
			this->background_color = background_color;
		}
		glm::uvec4 RendererOccupancyMap::GetOccupancyForPixel() {
			return glm::uvec4(chosen_pixel.occupancy[3], chosen_pixel.occupancy[2], chosen_pixel.occupancy[1], chosen_pixel.occupancy[0]);
		}
		std::string RendererOccupancyMap::GetName() {
			return "Occupancy Map";
		}

		//---------------------------------------------------------------------------------------
		void RendererOccupancyMap::Draw() {
			SetTextureFiltering(detail::TEXTURE_FILTER_TRILINEAR_ANISOTROPIC);
			SetRasterizationMode(detail::RASTERIZATION_FILL);
			glClearColor(background_color.x, background_color.y, background_color.z, background_color.w);

			//no depth
			glDisable(GL_DEPTH_TEST);

			//global bindings
			texture_occupancy_map0->BindAsImage(0, detail::ACCESS_READ_WRITE);
			texture_occupancy_map1->BindAsImage(1, detail::ACCESS_READ_WRITE);
			texture_occupancy_map2->BindAsImage(2, detail::ACCESS_READ_WRITE);
			texture_occupancy_map3->BindAsImage(3, detail::ACCESS_READ_WRITE);
			chosen_pixel.buffer->BindIndexed(detail::BUFFER_TARGET_STORAGE, 0);
			glBindImageTexture(4, 0, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32UI);
			glBindImageTexture(5, 0, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32UI);
			glBindImageTexture(6, 0, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32UI);

			//nodes
			std::vector<SceneNode*> nodelist;
			scene->GetSceneNodes(nodelist);


			//pass1, compute occupancy maps, counts and alpha sums
			fullscreen.framebuffer1->Bind();
			{
				glEnable(GL_BLEND);
				glm::vec4 zero = glm::vec4(0, 0, 0, 0);
				glClearBufferfv(GL_COLOR, 0, &zero.x);						// 0 is count
				glClearBufferfv(GL_COLOR, 1, &zero.x);						// 1 is alpha sum
				glBlendEquationi(0, GL_FUNC_ADD);							// SUM of counts
				glBlendFunci(0, GL_ONE, GL_ONE);
				glBlendEquationi(1, GL_FUNC_ADD);							// SUM of alphas
				glBlendFunci(1, GL_ONE, GL_ONE);
				glViewport(0, 0, screen.width, screen.height);
				shader_map->Bind();
				shader_map->SetUniform("view_matrix", camera->GetViewMatrix(), false);
				shader_map->SetUniform("projection_matrix", projection.matrix, false);
				shader_map->SetUniform("frustum_near_far", projection.clip_near, projection.clip_far);
				for (auto& node : nodelist) {
					shader_map->SetUniform("model_matrix", node->GetCurrentTransformation(), false);
					const std::vector<GPUEntity*> entities = node->GetEntities();
					for (auto& e : entities) {

						e->GetMaterial()->Bind(shader_map);
						if (override_object_transparency) shader_map->SetUniform("mat_transparency", 0.9f);
						e->GetMesh()->Draw();
					}
				}
				glDisable(GL_BLEND);
			}
			fullscreen.framebuffer1->Unbind();
			glMemoryBarrier(GL_ALL_BARRIER_BITS);


			//bind the accumulated alpha and counts
			fullscreen.target_count->BindAsImage(4, detail::ACCESS_READ_ONLY);
			fullscreen.target_alpha->BindAsImage(5, detail::ACCESS_READ_ONLY);


			///pass2, compose 
			fullscreen.framebuffer2->Bind();
			{
				//render targets
				glEnable(GL_BLEND);
				glm::vec4 zero = glm::vec4(0, 0, 0, 0);
				glClearBufferfv(GL_COLOR, 0, &zero.x);				// 0 is color
				glBlendEquationi(0, GL_FUNC_ADD);					// SUM of color
				glBlendFunci(0, GL_ONE, GL_ONE);
				glViewport(0, 0, screen.width, screen.height);
				shader_render->Bind();
				shader_render->SetUniform("view_matrix", camera->GetViewMatrix(), false);
				shader_render->SetUniform("projection_matrix", projection.matrix, false);
				shader_render->SetUniform("frustum_near_far", projection.clip_near, projection.clip_far);;
				for (auto& node : nodelist) {
					shader_render->SetUniform("model_matrix", node->GetCurrentTransformation(), false);
					const std::vector<GPUEntity*> entities = node->GetEntities();
					for (auto& e : entities) {
						e->GetMaterial()->Bind(shader_render);
						if (override_object_transparency) shader_render->SetUniform("mat_transparency", 0.75f);
						e->GetMesh()->Draw();
					}
				}
				glDisable(GL_BLEND);
			}
			fullscreen.framebuffer2->Unbind();

			//bind the previously computed accumulated color
			fullscreen.target_color->BindAsImage(6, detail::ACCESS_READ_ONLY);


			///pass3, final, add background and reinitialize
			glViewport(0, 0, screen.width, screen.height);
			glClearBufferfv(GL_COLOR, 0, &background_color.x);
			shader_final->Bind();
			shader_final->SetUniform("show_occupancy", (int)show_occupancy);
			shader_final->SetUniform("chosen_pixel", (float)chosen_pixel.posx, (float)chosen_pixel.posy);
			shader_final->SetUniform("background_color", background_color);
			fullscreen.mesh->Draw();
			glMemoryBarrier(GL_ALL_BARRIER_BITS);					//<- wait for clears on occupancy and writes on chosen pixel

			//save occupancy for output
			chosen_pixel.buffer->Copy((void*)&chosen_pixel.occupancy[0], sizeof(unsigned int) * 4);
			glm::uvec4 val = glm::uvec4(chosen_pixel.occupancy[3], chosen_pixel.occupancy[2], chosen_pixel.occupancy[1], chosen_pixel.occupancy[0]);
			//std::cout << "Occupancy = " << std::bitset<32>(val.x) << " " << std::bitset<32>(val.y) << " " << std::bitset<32>(val.z) << " " << std::bitset<32>(val.w) << std::endl;
			//std::cin.get();

			//reset occupancy to 0
			glm::uvec4 zero = glm::uvec4(0, 0, 0, 0);
			chosen_pixel.buffer->Update((void*)&zero[0], sizeof(unsigned int) * 4, detail::BUFFER_USAGE_CPU_GPU);
		}

		void RendererOccupancyMap::ShowOccupancy(bool value) {
			show_occupancy = value;
		}
}