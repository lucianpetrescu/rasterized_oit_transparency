//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//  a linked list OIT renderer

#include "renderer_linked_list.hpp"

namespace oit {
	RendererLinkedList::RendererLinkedList() {
		override_object_transparency = false;
		scene = nullptr;
		camera = nullptr;
		shader_collect = new oit::GPUProgram("../shaders/linkedlist_collect.vert", "../shaders/linkedlist_collect.frag");
		shader_compose = new oit::GPUProgram("../shaders/linkedlist_compose.vert", "../shaders/linkedlist_compose.frag");

		std::vector<Vertex> vertices;
		Vertex v1;	v1.position = glm::vec3(-1.f, -1.f, 0.f);	v1.texcoord = glm::vec3(0.f, 0.f, 0.f);	vertices.push_back(v1);
		Vertex v2;	v2.position = glm::vec3(1.f, -1.f, 0.f);	v2.texcoord = glm::vec3(1.f, 0.f, 0.f);	vertices.push_back(v2);
		Vertex v3;	v3.position = glm::vec3(1.f, 1.f, 0.f);		v3.texcoord = glm::vec3(1.f, 1.f, 0.f);	vertices.push_back(v3);
		Vertex v4;	v4.position = glm::vec3(-1.f, 1.f, 0.f);	v4.texcoord = glm::vec3(0.f, 1.f, 0.f);	vertices.push_back(v4);
		std::vector<unsigned int> indices;
		indices.push_back(0);	indices.push_back(1);	indices.push_back(2);
		indices.push_back(2);	indices.push_back(3);	indices.push_back(0);
		AABB aabb;
		fullscreen_buffer_vertex = new GPUBuffer((float*)&vertices[0], (unsigned int)vertices.size() * sizeof(oit::Vertex), oit::detail::BUFFER_USAGE_GPUSTATIC);
		fullscreen_buffer_index = new GPUBuffer((unsigned int*)&indices[0], (unsigned int)indices.size() * sizeof(unsigned int), oit::detail::BUFFER_USAGE_GPUSTATIC);
		fullscreen_mesh = new GPUMesh(fullscreen_buffer_vertex, fullscreen_buffer_index, 0, (unsigned int)indices.size(), aabb, oit::detail::TOPOLOGY_TRIANGLES);


		texture_head = new GPUTexture();
		texture_head->Create((unsigned int*)nullptr, 800, 600, 1, false);		//R32UI

		float *buffer_elements_data;
		max_list_elements = glm::max(1600 * 1200 * 6 * 32, 300000000);		//number of entries, which will lead to close to 1.5 GB (a 2 GB GPU should have no problem running this)
		buffer_elements_data = new float[max_list_elements];
		buffer_elements = new GPUBuffer((float*)buffer_elements_data, max_list_elements * sizeof(float), oit::detail::BUFFER_USAGE_GPUDYNAMIC);
		delete[] buffer_elements_data;

		unsigned int originaldata = 0;
		buffer_atomic_counter = new GPUBuffer((unsigned int*)&originaldata, (unsigned int)sizeof(unsigned int), oit::detail::BUFFER_USAGE_GPUDYNAMIC);

		SetTextureFiltering(detail::TEXTURE_FILTER_TRILINEAR_ANISOTROPIC);
		SetRasterizationMode(detail::RASTERIZATION_FILL);
		SetProjection(75.0f, 1.0f, 10000.0f);
		SetResolution(800, 600);
		background_color = glm::vec4(0.5, 0.5, 0.5, 0);
		glClearColor(background_color.x, background_color.y, background_color.z, background_color.w);
		glClearDepth(1);
	}
	RendererLinkedList::~RendererLinkedList() {
		delete shader_collect;
		delete shader_compose;

		delete fullscreen_mesh;
		delete fullscreen_buffer_index;
		delete fullscreen_buffer_vertex;

		delete texture_head;
		delete buffer_elements;
		delete buffer_atomic_counter;
	}
	void RendererLinkedList::SetProjection(float fovy, float clip_near, float clip_far)  {
		projection.fovy = fovy;
		projection.clip_near = clip_near;
		projection.clip_far = clip_far;
	}
	void RendererLinkedList::SetTextureFiltering(unsigned int mode)  {
		texture_filter.Set(mode);
		for (unsigned int i = 0; i < 10; i++) texture_filter.Bind(i);
		texture_filter_mode = mode;
	}
	void RendererLinkedList::SetRasterizationMode(unsigned int mode)  {
		if (mode == detail::RASTERIZATION_POINTS) glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
		else if (mode == detail::RASTERIZATION_WIREFRAME) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		else if (mode == detail::RASTERIZATION_FILL) glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		drawing_mode = mode;
	}
	void RendererLinkedList::SetCamera(Camera* incamera)  {
		assert(incamera && "[RendererLinkedList] null camera.");
		this->camera = incamera;
	}
	void RendererLinkedList::SetScene(Scene* inscene)  {
		assert(inscene && "[RendererLinkedList] null scene.");
		this->scene = inscene;
	}
	Camera* RendererLinkedList::GetCamera() {
		return camera;
	}
	void RendererLinkedList::ReloadGPUPrograms()  {
		shader_collect->Reload();
		shader_compose->Reload();
	}
	void RendererLinkedList::SetResolution(unsigned int width, unsigned int height) {
		screen.width = width;
		screen.height = height;
		projection.matrix = glm::perspective(glm::radians(projection.fovy), (float)width / (float)height, projection.clip_near, projection.clip_far);

		//same size -> return
		unsigned int w = texture_head->GetWidth();
		unsigned int h = texture_head->GetHeight();
		if (w == width && h == height) return;

		//resize used buffers
		texture_head->Resize(width, height);

		//resize elements buffer
		if (buffer_elements) {
			delete buffer_elements;
		}
		float *buffer_elements_data;
		max_list_elements = glm::min(width * height * 6 * 20, (unsigned int)300000000);	//6 floats per element, a mean of 20 elements per pixel (should run for 1.5GB gpus)
		buffer_elements_data = new float[max_list_elements];
		buffer_elements = new GPUBuffer((float*)buffer_elements_data, max_list_elements * sizeof(float), oit::detail::BUFFER_USAGE_GPUDYNAMIC);
		delete[] buffer_elements_data;
	}
	void RendererLinkedList::SetOverrideObjectTransparency(bool value)  {
		override_object_transparency = value;
	}
	bool RendererLinkedList::GetOverrideObjectTransparency() {
		return override_object_transparency;
	}
	void RendererLinkedList::SetBackgroundColor(const glm::vec4& background_color)  {
		this->background_color = background_color;
	}
	std::string RendererLinkedList::GetName()  {
		return "Linked List Per Pixel";
	}

	//---------------------------------------------------------------------------------------
	void RendererLinkedList::Draw()  {
		SetTextureFiltering(detail::TEXTURE_FILTER_TRILINEAR_ANISOTROPIC);
		SetRasterizationMode(detail::RASTERIZATION_FILL);
		glClearColor(background_color.x, background_color.y, background_color.z, background_color.w);

		//no depth
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_BLEND);

		//explicitly disable multisampling (otherwise some multisample sampling patterns sampled with a single sample per pixel will lead to seams during composition)
		glDisable(GL_MULTISAMPLE);

		//buffers
		unsigned int originaldata = 0;
		buffer_atomic_counter->Update((unsigned int*)&originaldata, sizeof(unsigned int), detail::BUFFER_USAGE_GPUDYNAMIC);
		buffer_atomic_counter->BindIndexed(detail::BUFFER_TARGET_ATOMIC_COUNTER, 0);
		glMemoryBarrier(GL_BUFFER_UPDATE_BARRIER_BIT);
		texture_head->BindAsImage(0, detail::ACCESS_READ_WRITE);
		buffer_elements->BindIndexed(detail::BUFFER_TARGET_STORAGE, 0);

		//pass1. collect
		glViewport(0, 0, screen.width, screen.height);
		glClearBufferfv(GL_COLOR, 0, &background_color.x);
		shader_collect->Bind();
		shader_collect->SetUniform("view_matrix", camera->GetViewMatrix(), false);
		shader_collect->SetUniform("projection_matrix", projection.matrix, false);
		shader_collect->SetUniform("MAX_LIST_ELEMENTS", max_list_elements);
		//collect scene nodes
		std::vector<SceneNode*> nodelist;
		scene->GetSceneNodes(nodelist);
		//for each node
		for (auto& node : nodelist) {
			shader_collect->SetUniform("model_matrix", node->GetCurrentTransformation(), false);
			//draw all entities
			const std::vector<GPUEntity*> entities = node->GetEntities();
			for (auto& e : entities) {
				e->GetMaterial()->Bind(shader_collect);
				if (override_object_transparency) shader_collect->SetUniform("mat_transparency", 0.75f);
				e->GetMesh()->Draw();
			}
		}

		glMemoryBarrier(GL_ALL_BARRIER_BITS);


		///pass2, compose and reinitialize
		glViewport(0, 0, screen.width, screen.height);
		glClearBufferfv(GL_COLOR, 0, &background_color.x);
		shader_compose->Bind();
		shader_compose->SetUniform("background_color", background_color.x, background_color.y, background_color.z);
		switch (sorting_algorithm) {
			case SortingAlgorithm::Bubble: 		shader_compose->SetUniform("sorting_algorithm", 0); break;
			case SortingAlgorithm::Selection: 	shader_compose->SetUniform("sorting_algorithm", 1); break;
			case SortingAlgorithm::Insertion: 	shader_compose->SetUniform("sorting_algorithm", 2); break;
			case SortingAlgorithm::Shell: 		shader_compose->SetUniform("sorting_algorithm", 3); break;
			case SortingAlgorithm::Quick: 		shader_compose->SetUniform("sorting_algorithm", 4); break;
			case SortingAlgorithm::Merge: 		shader_compose->SetUniform("sorting_algorithm", 5); break;
			default: break;
		}

		fullscreen_mesh->Draw();

		glMemoryBarrier(GL_ALL_BARRIER_BITS);
	}

	std::string& RendererLinkedList::GetSortingAlgorithmName() {
		return sorting_algorithm_name;
	}


	void RendererLinkedList::SetSortingAlgorithm(SortingAlgorithm algorithm) {
		sorting_algorithm = algorithm;
		switch (sorting_algorithm) {
		case SortingAlgorithm::Bubble: 		sorting_algorithm_name = "Bubble Sort"; break;
		case SortingAlgorithm::Selection: 	sorting_algorithm_name = "Selection Sort"; break;
		case SortingAlgorithm::Insertion:	sorting_algorithm_name = "Insertion Sort"; break;
		case SortingAlgorithm::Shell: 		sorting_algorithm_name = "Shell Sort"; break;
		case SortingAlgorithm::Quick: 		sorting_algorithm_name = "Quick Sort"; break;
		case SortingAlgorithm::Merge: 		sorting_algorithm_name = "Merge Sort"; break;
		default: break;
			//can't get here.
		}
	}


}