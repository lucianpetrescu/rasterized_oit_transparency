//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

#include "scene.hpp"

namespace oit {

	//--------------------------------------------------------------------------------------------
	//always starts and ends with point 0,0,0 with orientation 0,0,1
	TrajectoryPoint::TrajectoryPoint() { position = glm::vec3(0);  orientation = glm::quat(1, 0, 0, 0); duration = 0; };
	TrajectoryPoint::TrajectoryPoint(const glm::vec3& p, const glm::quat& o, double t) { position = p; orientation = o; duration = t; }
	bool TrajectoryPoint::operator==(const TrajectoryPoint& rhs) { if (position == rhs.position && orientation == rhs.orientation && duration == rhs.duration) return true; return false; }
	
	Trajectory::Trajectory() {
			ResetTrajectory();
		}
	Trajectory::~Trajectory() {}
	void Trajectory::SetCycling(bool cycling) { this->cycling = cycling; }
	bool Trajectory::GetCycling() { return cycling; }
	void Trajectory::AddPoint(const TrajectoryPoint& p) {
		points.push_back(p);
		total_duration += p.duration;
	}
	void Trajectory::RemovePoint(const TrajectoryPoint& p) {
		for (auto& pt : points) if (pt == p) total_duration -= p.duration;
		points.erase(std::remove(points.begin(), points.end(), p), points.end());
	}
	void Trajectory::ResetTrajectory() {
		cycling = false;
		total_duration = 0;
		walk_duration = 0;
		walk_index = 0;
		orient_per_sec = glm::quat(1, 0, 0, 0);
	}
	void Trajectory::ResetWalk() {
		walk_duration = 0;
		walk_index = 0;
		//but not rotation
	}
	/// compute trajectory
	/// the time is measured in milliseconds (ms)
	glm::mat4 Trajectory::GetTransformation(double dt) {
		//get orientation
		float exponent = (float)(fmod(dt, 1000.0) / 1000.0);
		glm::quat static_orient = glm::pow(orient_per_sec, exponent);

		//no trajectory!!
		if (points.size() == 0) return glm::mat4(1);

		//trajectory ended ?
		if (dt > total_duration && !cycling) {
			glm::vec3 p = points.end()->position;
			glm::quat q = points.end()->orientation;
			return glm::mat4_cast(static_orient) * glm::mat4_cast(q) * glm::translate(glm::mat4(1), p);
		}

		//find out where we are
		while (walk_duration <dt) {
			walk_duration += points[walk_index].duration;
			walk_index += 1;
			if (walk_index == points.size()) {
				if (cycling) {
					//cycle to 0 again
					walk_index = 0;
				}
				else {
					//the trajectory ends
					glm::vec3 p = points.end()->position;
					glm::quat q = points.end()->orientation;
					return glm::mat4_cast(static_orient) *glm::mat4_cast(q) * glm::translate(glm::mat4(1), p);
				}
			}
		}

		//the trajectory isn't ended
		unsigned int prev_walk_index;
		if (walk_index == 0) prev_walk_index = (int)points.size() - 1;	//cycling on 0?
		else prev_walk_index = walk_index - 1;
		float interp = (float)((dt - walk_duration) / points[walk_index].duration);
		glm::vec3 p = glm::normalize(glm::mix(points[prev_walk_index].position, points[walk_index].position, interp));
		glm::quat q = glm::slerp(points[prev_walk_index].orientation, points[walk_index].orientation, interp);

		return glm::mat4_cast(static_orient) *glm::mat4_cast(q) * glm::translate(glm::mat4(1), p);
	}
	const std::vector<TrajectoryPoint>& Trajectory::GetPoints() { return points; }
	double Trajectory::GetTotalDuration() { return total_duration; }
	void Trajectory::SetOrientationPerSecond(const glm::quat& orient_per_sec) { this->orient_per_sec = orient_per_sec; }
	void Trajectory::SetOrientationPerSecond(float dx, float dy, float dz) { orient_per_sec = glm::quat(glm::vec3(dx, dy, dz)); }
	glm::quat Trajectory::GetOrientationPerSecond() { return orient_per_sec; }


	//--------------------------------------------------------------------------------------------
	// takes ownership of children (tree structure), but not of the entities (because they are assets)
	SceneNode::SceneNode() {
		parent = nullptr;
		to_parent_transformation = glm::mat4(1);
	}
	SceneNode::~SceneNode() {
		for (auto c : children) if (c) delete c;
	}
	//nullptr as parent means it doesn't have any parents
	void SceneNode::SetParent(SceneNode* parent, const glm::mat4& transformation) {
		SetParent(parent);
		SetToParentTransformation(transformation);
	}
	void SceneNode::SetParent(SceneNode* parent) {
		if (this->parent == parent) return;
		if (parent) parent->RemoveChild(this);		//can't have 2 parents
		this->parent = parent;						//can be nullptr
	}
	SceneNode* SceneNode::GetParent() { return parent; }
	void SceneNode::SetToParentTransformation(const glm::mat4& transformation) {
		this->to_parent_transformation = transformation;
	}
	glm::mat4 SceneNode::GetToParentTransformation() { return to_parent_transformation; }

	void SceneNode::AddChild(SceneNode* node) {
		assert(node && "[SceneNode] adding uninitialized child");
		for (auto& c : children) if (c == node) return;
		children.push_back(node);
		node->parent = this;
	}
	const std::vector<SceneNode*>& SceneNode::GetChildren() { return children; }
	void SceneNode::RemoveChild(SceneNode* node) {
		assert(node && "[SceneNode] removing uninitialized child");
		children.erase(std::remove(children.begin(), children.end(), node), children.end());
		node->parent = nullptr;
	}
	void SceneNode::AddEntity(GPUEntity* entity) {
		assert(entity && "[SceneNode] adding uninitialized entity");
		for (auto& e : entities) if (e == entity) return;
		entities.push_back(entity);
	}
	const std::vector<GPUEntity*>& SceneNode::GetEntities() { return entities; }
	void SceneNode::RemoveEntity(GPUEntity* entity) {
		assert(entity && "[SceneNode] removing uninitialized entity");
		entities.erase(std::remove(entities.begin(), entities.end(), entity), entities.end());
	}
	void SceneNode::SetTrajectory(const Trajectory& t) { trajectory = t; }
	Trajectory SceneNode::GetTrajectory() { return trajectory; }

	///simulate
	/// the time is measured in ms (milliseconds)
	void SceneNode::Simulate(double dt) {
		//get parent if any
		glm::mat4 current_parent_transformation = glm::mat4(1);
		if (parent) current_parent_transformation = parent->GetCurrentTransformation();

		//compute for this and children
		current_transformation = trajectory.GetTransformation(dt) * to_parent_transformation * current_parent_transformation;
		for (auto& c : children) c->Simulate(dt);
	}

	//obtain all the scene nodes from this sub tree
	void SceneNode::GetSceneNodes(std::vector<SceneNode*>& nodes) {
		for (auto& c : children) nodes.push_back(c);
		for (auto& c : children) c->GetSceneNodes(nodes);
	}

	glm::mat4 SceneNode::GetCurrentTransformation() { return current_transformation; }


	//--------------------------------------------------------------------------------------------
	//takes ownership of children
	Scene::Scene() {
			camera = nullptr;
		}
	Scene::~Scene() {
			for (auto& c : children) if (c) delete c;
		}
	//simulates the entire scene
	//time is measured in ms (milliseconds)
	void Scene::Simulate(double dt) {
		for (auto& c : children) if (c) c->Simulate(dt);
	}

	//gets ALL the scene nodes
	void Scene::GetSceneNodes(std::vector<SceneNode*>& nodes) {
		for (auto& c : children) if (c)	nodes.push_back(c);
		for (auto& c : children) if (c) c->GetSceneNodes(nodes);
	}
	const std::vector<SceneNode*> Scene::GetChildren() { return children; }
	Camera* Scene::GetCamera() { return camera; }
	void Scene::SetCamera(Camera* camera) {
		if (camera == this->camera) return;
		this->camera = camera;
	}
	void Scene::AddSceneNode(SceneNode* node) {
		assert(node && "[Scene] adding uninitialized node.");
		for (auto& c : children) if (c == node) return;
		children.push_back(node);
		node->SetParent(nullptr);
	}
	void Scene::RemoveSceneNode(SceneNode* node) {
		assert(node && "[Scene] removing uninitialized node.");
		children.erase(std::remove(children.begin(), children.end(), node), children.end());
		node->SetParent(nullptr);
	}

}