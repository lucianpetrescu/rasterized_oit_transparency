//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

#pragma once
#include "renderer_interface.hpp"
#include <bitset>

namespace oit {
	class RendererOccupancyMap : public RendererInterface {
	public:
		RendererOccupancyMap();
		~RendererOccupancyMap();
		void SetProjection(float fovy, float clip_near, float clip_far) override;
		void SetTextureFiltering(unsigned int mode) override;
		void SetRasterizationMode(unsigned int mode) override;
		void SetCamera(Camera* incamera) override;
		void SetScene(Scene* inscene) override;
		Camera* GetCamera();
		void ReloadGPUPrograms() override;
		void SetResolution(unsigned int width, unsigned int height) override;
		void SetOverrideObjectTransparency(bool value) override;
		bool GetOverrideObjectTransparency();
		void SetBackgroundColor(const glm::vec4& background_color) override;
		glm::uvec4 GetOccupancyForPixel();
		std::string GetName() override;
		void ShowOccupancy(bool value);

		//---------------------------------------------------------------------------------------
		void Draw() override;

	private:
		struct {
			unsigned int width =0;
			unsigned int height =0;
		}screen;
		Camera* camera = nullptr;
		Scene* scene = nullptr;
		GPUProgram* shader_map = nullptr, *shader_render = nullptr, *shader_final = nullptr;					//map, render, gather
		GPUTextureFilter texture_filter;
		unsigned int drawing_mode, texture_filter_mode;
		bool override_object_transparency = false;
		struct {
			float fovy = 90.f;
			float clip_near = 0.1f;
			float clip_far = 1000.f;
			glm::mat4 matrix = glm::mat4(1);
		}projection;
		glm::vec4 background_color = glm::vec4(0,0,0,0);

		//occupancy textures
		bool show_occupancy = false;
		GPUTexture* texture_occupancy_map0 = nullptr;
		GPUTexture* texture_occupancy_map1 = nullptr;
		GPUTexture* texture_occupancy_map2 = nullptr;
		GPUTexture* texture_occupancy_map3 = nullptr;

		struct {
			GPUBuffer* buffer_vertex = nullptr;
			GPUBuffer* buffer_index = nullptr;
			GPUMesh* mesh = nullptr;
			GPUFramebuffer* framebuffer1 = nullptr;
			GPUFramebuffer* framebuffer2 = nullptr;
			GPUTexture* target_count = nullptr;
			GPUTexture* target_color = nullptr;
			GPUTexture* target_alpha = nullptr;
		}fullscreen;

		struct {
			GPUBuffer* buffer = nullptr;
			unsigned int occupancy[4] = { 0,0,0,0 };
			unsigned int posx = 0;
			unsigned int posy = 0;
		}chosen_pixel;
	};
}