//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

#pragma once
#include "scene.hpp"
#include "camera.hpp"
#include <ctime>
#include <string>

namespace oit{
	
	class RendererInterface{
	public:
		virtual ~RendererInterface() {};
		virtual void SetProjection(float fovy, float clip_near, float clip_far) = 0;
		virtual void SetRasterizationMode(unsigned int mode) = 0;
		virtual void SetScene(Scene* scene) = 0;
		virtual void SetCamera(Camera* camera) = 0;
		virtual void ReloadGPUPrograms() = 0;
		virtual void SetResolution(unsigned int width, unsigned int height) = 0;
		virtual void Draw() = 0;
		virtual void SetOverrideObjectTransparency(bool value) = 0;
		virtual void SetBackgroundColor(const glm::vec4& background_color) = 0;
		virtual void SetTextureFiltering(unsigned int mode) = 0;
		virtual std::string GetName() = 0;
	};
}