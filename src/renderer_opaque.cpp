//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//  opaque

#include "renderer_opaque.hpp"

namespace oit {
	RendererOpaque::RendererOpaque() {
		scene = nullptr;
		camera = nullptr;
		shader = new oit::GPUProgram("../shaders/opaque.vert", "../shaders/opaque.frag");

		SetTextureFiltering(detail::TEXTURE_FILTER_TRILINEAR_ANISOTROPIC);
		SetRasterizationMode(detail::RASTERIZATION_FILL);
		SetProjection(75.0f, 1.0f, 10000.0f);
		SetResolution(800, 600);
		SetBackgroundColor(glm::vec4(0.5, 0.5, 0.5, 0));
	}
	RendererOpaque::~RendererOpaque() {
		delete shader;
	}
	void RendererOpaque::SetProjection(float fovy, float clip_near, float clip_far) {
		projection.fovy = fovy;
		projection.clip_near = clip_near;
		projection.clip_far = clip_far;
	}
	void RendererOpaque::SetTextureFiltering(unsigned int mode) {
		texture_filter.Set(mode);
		for (unsigned int i = 0; i < 10; i++) texture_filter.Bind(i);
		texture_filter_mode = mode;
	}
	void RendererOpaque::SetRasterizationMode(unsigned int mode) {
		if (mode == detail::RASTERIZATION_POINTS) glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
		else if (mode == detail::RASTERIZATION_WIREFRAME) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		else if (mode == detail::RASTERIZATION_FILL) glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		drawing_mode = mode;
	}
	void RendererOpaque::SetCamera(Camera* incamera) {
		assert(incamera && "[RendererOpaque] null camera.");
		this->camera = incamera;
	}
	void RendererOpaque::SetScene(Scene* inscene) {
		assert(inscene && "[RendererOpaque] null scene.");
		this->scene = inscene;
	}
	Camera* RendererOpaque::GetCamera() {
		return camera;
	}
	void RendererOpaque::ReloadGPUPrograms() {
		shader->Reload();
	}
	void RendererOpaque::SetResolution(unsigned int width, unsigned int height) {
		screen.width = width;
		screen.height = height;
		projection.matrix = glm::perspective(glm::radians(projection.fovy), (float)width / (float)height, projection.clip_near, projection.clip_far);
	}
	void RendererOpaque::SetBackgroundColor(const glm::vec4& background_color) {
		this->background_color = background_color;
	}
	void RendererOpaque::SetOverrideObjectTransparency(bool value) {
	}
	std::string RendererOpaque::GetName() {
		return "Opaque";
	}

	//---------------------------------------------------------------------------------------
	void RendererOpaque::Draw() {
		SetTextureFiltering(detail::TEXTURE_FILTER_TRILINEAR_ANISOTROPIC);
		SetRasterizationMode(detail::RASTERIZATION_FILL);
		glClearColor(background_color.x, background_color.y, background_color.z, background_color.w);
		glClearDepth(1);

		///pass1
		glViewport(0, 0, screen.width, screen.height);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
		shader->Bind();
		shader->SetUniform("view_matrix", camera->GetViewMatrix(), false);
		shader->SetUniform("projection_matrix", projection.matrix, false);
			
		// collect scene nodes
		std::vector<SceneNode*> nodelist;
		scene->GetSceneNodes(nodelist);

		//for each node
		for (auto& node : nodelist) {
			shader->SetUniform("model_matrix", node->GetCurrentTransformation(), false);

			//draw all entities
			const std::vector<GPUEntity*> entities = node->GetEntities();
			for (auto& e : entities) {
				e->GetMaterial()->Bind(shader);
				e->GetMesh()->Draw();
			}
		}
	}
}