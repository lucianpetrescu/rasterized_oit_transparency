//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

#pragma once
#include "gpu_objects.hpp"
#include "camera.hpp"

namespace oit {

	//--------------------------------------------------------------------------------------------
	//always starts and ends with point 0,0,0 with orientation 0,0,1
	struct TrajectoryPoint {
		TrajectoryPoint();
		TrajectoryPoint(const glm::vec3& p, const glm::quat& o, double t);
		bool operator==(const TrajectoryPoint& rhs);
		glm::vec3 position;
		double duration;
		glm::quat orientation;
	};
	class Trajectory {
	public:
		Trajectory();
		~Trajectory();
		void SetCycling(bool cycling);
		bool GetCycling();
		void AddPoint(const TrajectoryPoint& p);
		void RemovePoint(const TrajectoryPoint& p);
		void ResetTrajectory();
		void ResetWalk();
		/// compute trajectory
		/// the time is measured in milliseconds (ms)
		glm::mat4 GetTransformation(double dt);
		const std::vector<TrajectoryPoint>& GetPoints();
		double GetTotalDuration();
		void SetOrientationPerSecond(const glm::quat& orient_per_sec);
		void SetOrientationPerSecond(float dx, float dy, float dz);
		glm::quat GetOrientationPerSecond();
	private:
		glm::quat orient_per_sec;
		bool cycling;
		double total_duration;
		double walk_duration;
		unsigned int walk_index;
		std::vector<TrajectoryPoint> points;
	};


	//--------------------------------------------------------------------------------------------
	// takes ownership of children (tree structure), but not of the entities (because they are assets)
	class SceneNode {
	public:
		SceneNode();
		~SceneNode();
		//nullptr as parent means it doesn't have any parents
		void SetParent(SceneNode* parent, const glm::mat4& transformation);
		void SetParent(SceneNode* parent);
		SceneNode* GetParent();
		void SetToParentTransformation(const glm::mat4& transformation);
		glm::mat4 GetToParentTransformation();
		void AddChild(SceneNode* node);
		const std::vector<SceneNode*>& GetChildren();
		void RemoveChild(SceneNode* node);
		void AddEntity(GPUEntity* entity);
		const std::vector<GPUEntity*>& GetEntities();
		void RemoveEntity(GPUEntity* entity);
		void SetTrajectory(const Trajectory& t);
		Trajectory GetTrajectory();

		///simulate
		/// the time is measured in ms (milliseconds)
		void Simulate(double dt);

		//obtain all the scene nodes from this sub tree
		void GetSceneNodes(std::vector<SceneNode*>& nodes);

		glm::mat4 GetCurrentTransformation();
	private:
		//relationship of the current node with parents and children
		SceneNode* parent;
		glm::mat4 to_parent_transformation;
		glm::mat4 current_transformation;
		std::vector<SceneNode*> children;
		Trajectory trajectory;

		//drawable contents of the current node, no other transformations
		std::vector<GPUEntity*> entities;
	};


	//--------------------------------------------------------------------------------------------
	//takes ownership of children
	class Scene {
	public:
		Scene();
		~Scene();

		//simulates the entire scene
		//time is measured in ms (milliseconds)
		void Simulate(double dt);

		//gets ALL the scene nodes
		void GetSceneNodes(std::vector<SceneNode*>& nodes);
		const std::vector<SceneNode*> GetChildren();
		Camera* GetCamera();
		void SetCamera(Camera* camera);
		void AddSceneNode(SceneNode* node);
		void RemoveSceneNode(SceneNode* node);
	private:
		//camera
		Camera *camera;

		//root node
		std::vector<SceneNode*> children;
	};

}