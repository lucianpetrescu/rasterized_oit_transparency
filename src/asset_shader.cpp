//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

#include "asset_shader.hpp"

namespace oit {

	namespace detail {

		unsigned int AssetLoadShader(const std::string &compute_shader_file) {
			Logger << LOGGER_NORMAL << "[AssetShader] loading: " << std::endl;
			Logger << LOGGER_NORMAL << "\tcompute shader = " << compute_shader_file << std::endl;
			std::vector<unsigned int> shaders;
			shaders.push_back(AssetCreateShader(compute_shader_file, GL_COMPUTE_SHADER));
			return AssetCreateProgram(shaders);
		}
		unsigned int AssetLoadShader(const std::string &vertex_shader_file, const std::string &fragment_shader_file) {
			Logger << LOGGER_NORMAL << "[AssetShader] loading: " << std::endl;
			Logger << LOGGER_NORMAL << "\tvertex shader = " << vertex_shader_file << std::endl;
			Logger << LOGGER_NORMAL << "\tfragment shader = " << fragment_shader_file << std::endl;
			std::vector<unsigned int> shaders;
			shaders.push_back(AssetCreateShader(vertex_shader_file, GL_VERTEX_SHADER));
			shaders.push_back(AssetCreateShader(fragment_shader_file, GL_FRAGMENT_SHADER));
			return AssetCreateProgram(shaders);
		}
		unsigned int AssetLoadShader(const std::string &vertex_shader_file, const std::string &geometry_shader_file, const std::string &fragment_shader_file) {
			Logger << LOGGER_NORMAL << "[AssetShader] loading: " << std::endl;
			Logger << LOGGER_NORMAL << "\tvertex shader = " << vertex_shader_file << std::endl;
			Logger << LOGGER_NORMAL << "\tgeometry shader = " << geometry_shader_file << std::endl;
			Logger << LOGGER_NORMAL << "\tfragment shader = " << fragment_shader_file << std::endl;
			std::vector<unsigned int> shaders;
			shaders.push_back(AssetCreateShader(vertex_shader_file, GL_VERTEX_SHADER));
			shaders.push_back(AssetCreateShader(geometry_shader_file, GL_GEOMETRY_SHADER));
			shaders.push_back(AssetCreateShader(fragment_shader_file, GL_FRAGMENT_SHADER));
			return AssetCreateProgram(shaders);
		}
		unsigned int AssetLoadShader(const std::string &vertex_shader_file, const std::string& tessctrl_shader_file, const std::string& tesseval_shader_file, const std::string &fragment_shader_file) {
			Logger << LOGGER_NORMAL << "[AssetShader] loading: " << std::endl;
			Logger << LOGGER_NORMAL << "\tvertex shader = " << vertex_shader_file << std::endl;
			Logger << LOGGER_NORMAL << "\tfragment shader = " << fragment_shader_file << std::endl;
			std::vector<unsigned int> shaders;
			shaders.push_back(AssetCreateShader(vertex_shader_file, GL_VERTEX_SHADER));
			shaders.push_back(AssetCreateShader(tessctrl_shader_file, GL_TESS_CONTROL_SHADER));
			shaders.push_back(AssetCreateShader(tesseval_shader_file, GL_TESS_EVALUATION_SHADER));
			shaders.push_back(AssetCreateShader(fragment_shader_file, GL_FRAGMENT_SHADER));
			return AssetCreateProgram(shaders);
		}
		unsigned int AssetLoadShader(const std::string &vertex_shader_file, const std::string& tessctrl_shader_file, const std::string& tesseval_shader_file, const std::string& geometry_shader_file, const std::string &fragment_shader_file) {
			Logger << LOGGER_NORMAL << "[AssetShader] loading: " << std::endl;
			Logger << LOGGER_NORMAL << "\tvertex shader = " << vertex_shader_file << std::endl;
			Logger << LOGGER_NORMAL << "\tfragment shader = " << fragment_shader_file << std::endl;
			std::vector<unsigned int> shaders;
			shaders.push_back(AssetCreateShader(vertex_shader_file, GL_VERTEX_SHADER));
			shaders.push_back(AssetCreateShader(tessctrl_shader_file, GL_TESS_CONTROL_SHADER));
			shaders.push_back(AssetCreateShader(tesseval_shader_file, GL_TESS_EVALUATION_SHADER));
			shaders.push_back(AssetCreateShader(geometry_shader_file, GL_GEOMETRY_SHADER));
			shaders.push_back(AssetCreateShader(fragment_shader_file, GL_FRAGMENT_SHADER));
			return AssetCreateProgram(shaders);
		}

		unsigned int AssetCreateShader(const std::string &shader_file, GLenum shader_type) {
			std::string shader_code;
			std::ifstream file(shader_file.c_str(), std::ios::in);
			if (!file.good()) {
				std::cout << "[AssetShader] Could not find file " << shader_file << " or lacking reading rights!" << std::endl;
				std::terminate();
			}
			
			 while( file.good() ) {
				std::string line;
				std::getline(file, line);
				shader_code.append(line + "\n");
			}
			file.close();
			

			int info_log_length = 0, compile_result = 0;
			unsigned int gl_shader_object;

			gl_shader_object = glCreateShader(shader_type);
			const char *shader_code_ptr = shader_code.c_str();
			const int shader_code_size = (int)shader_code.size();
			glShaderSource(gl_shader_object, 1, &shader_code_ptr, &shader_code_size);
			glCompileShader(gl_shader_object);
			glGetShaderiv(gl_shader_object, GL_COMPILE_STATUS, &compile_result);


			if (compile_result == GL_FALSE) {
				std::string str_shader_type = "";
				if (shader_type == GL_VERTEX_SHADER) str_shader_type = "vertex shader";
				if (shader_type == GL_TESS_CONTROL_SHADER) str_shader_type = "tess control shader";
				if (shader_type == GL_TESS_EVALUATION_SHADER) str_shader_type = "tess evaluation shader";
				if (shader_type == GL_GEOMETRY_SHADER) str_shader_type = "geometry shader";
				if (shader_type == GL_FRAGMENT_SHADER) str_shader_type = "fragment shader";
				if (shader_type == GL_COMPUTE_SHADER) str_shader_type = "compute shader";

				glGetShaderiv(gl_shader_object, GL_INFO_LOG_LENGTH, &info_log_length);
				std::vector<char> shader_log(info_log_length);
				glGetShaderInfoLog(gl_shader_object, info_log_length, NULL, &shader_log[0]);
				Logger << LOGGER_CRITICAL << "[AssetShader] COMPILE ERROR in " << str_shader_type << std::endl << &shader_log[0] << std::endl;
				return 0;
			}

			return gl_shader_object;
		}
		unsigned int AssetCreateProgram(const std::vector<unsigned int> &shader_objects) {
			int info_log_length = 0, link_result = 0;

			unsigned int gl_program_object = glCreateProgram();
			for (std::vector<unsigned int>::const_iterator it = shader_objects.begin(); it != shader_objects.end(); it++) glAttachShader(gl_program_object, (*it));
			glLinkProgram(gl_program_object);
			glGetProgramiv(gl_program_object, GL_LINK_STATUS, &link_result);

			if (link_result == GL_FALSE) {
				glGetProgramiv(gl_program_object, GL_INFO_LOG_LENGTH, &info_log_length);
				std::vector<char> program_log(info_log_length);
				glGetProgramInfoLog(gl_program_object, info_log_length, NULL, &program_log[0]);
				Logger << LOGGER_CRITICAL << "[AssetShader] LINK ERROR:" << std::endl << &program_log[0] << std::endl;
				return 0;
			}

			for (std::vector<unsigned int>::const_iterator it = shader_objects.begin(); it != shader_objects.end(); it++) glDeleteShader((*it));

			return gl_program_object;
		}
	}
}
