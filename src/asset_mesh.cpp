//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

#include "asset_mesh.hpp"
#include "gpu_objects.hpp"

namespace oit {

	//--------------------------------------------------------------------------------------------
	Vertex::Vertex() {
			position = normal = texcoord = tangent = extra = glm::vec3(0);
		}
	Vertex::Vertex(const Vertex& rhs) {
			position = rhs.position;
			normal = rhs.normal;
			tangent = rhs.tangent;
			texcoord = rhs.texcoord;
			extra = rhs.extra;
		}
	Vertex::Vertex(Vertex&& rhs) {
			position = rhs.position;
			normal = rhs.normal;
			tangent = rhs.tangent;
			texcoord = rhs.texcoord;
			extra = rhs.extra;
		}
	Vertex& Vertex::operator=(const Vertex &rhs) {
			position = rhs.position;
			normal = rhs.normal;
			tangent = rhs.tangent;
			texcoord = rhs.texcoord;
			extra = rhs.extra;
			return (*this);
		}
	bool operator==(const Vertex &v1, const Vertex &v2) {
		if (v1.position != v2.position) return false;
		if (v1.normal != v2.normal) return false;
		if (v1.tangent != v2.tangent) return false;
		if (v1.texcoord != v2.texcoord) return false;
		if (v1.extra != v2.extra) return false;
		return true;
	}

	//--------------------------------------------------------------------------------------------
	AABB::AABB() {
		Reset();
	}
	AABB::~AABB() {
	}
	void AABB::AddPoint(const glm::vec3& point) {
		if (point.x < inf.x) inf.x = point.x;
		if (point.y < inf.y) inf.y = point.y;
		if (point.z < inf.z) inf.z = point.z;
		if (point.x > sup.x) sup.x = point.x;
		if (point.y > sup.y) sup.y = point.y;
		if (point.z > sup.z) sup.z = point.z;
	}
	void AABB::AddAABB(const AABB& aabb) {
		AddPoint(aabb.inf);
		AddPoint(aabb.sup);
	}
	void AABB::Reset() {
		inf = glm::vec3(FLT_MAX, FLT_MAX, FLT_MAX);
		sup = glm::vec3(-FLT_MAX, -FLT_MAX, -FLT_MAX);
	}
	AABB& AABB::operator=(const AABB &rhs) {
		inf = rhs.inf;
		sup = rhs.sup;
		return (*this);
	}





	namespace detail {
		//--------------------------------------------------------------------------------------------
		ObjMaterial::ObjMaterial() {
			name = "";
			ambiental = transmission = glm::vec3(0);
			diffuse_transparency = specular_exponent = glm::vec4(0, 0, 0, 0);
			colormap_file = normalmap_file = displacementmap_file = alphamap_file = specularmap_file = "";
		}
		ObjMaterial ObjMaterial::operator=(const ObjMaterial& rhs) {
			name = rhs.name;
			ambiental = rhs.ambiental;
			transmission = rhs.transmission;
			diffuse_transparency = rhs.diffuse_transparency;
			specular_exponent = rhs.specular_exponent;
			colormap_file = rhs.colormap_file;
			normalmap_file = rhs.normalmap_file;
			displacementmap_file = rhs.displacementmap_file;
			alphamap_file = rhs.alphamap_file;
			specularmap_file = rhs.specularmap_file;
			return (*this);
		}
		ObjMaterial::~ObjMaterial() {
		}


		//--------------------------------------------------------------------------------------------
		ObjMesh::ObjMesh() {
			name = "";
			index_material = 0;
			index_first = 0;
			index_count = 0;
		}
		ObjMesh ObjMesh::operator=(const ObjMesh& rhs) {
			name = rhs.name;
			index_material = rhs.index_material;
			index_first = rhs.index_first;
			index_count = rhs.index_count;
			aabb = rhs.aabb;
			return (*this);
		}
		ObjMesh::~ObjMesh() {
		}



		//--------------------------------------------------------------------------------------------
		// http://paulbourke.net/dataformats/mtl/
		void AssetLoadMTLFile(const std::string& filename, std::vector<ObjMaterial>& materials) {
			Timer timer;
			//filename without extension
			std::string filename_without_extension = filename.substr(0, filename.find_last_of('.'));

			//open file
			std::ifstream file(filename.c_str(), std::ios::in | std::ios::binary);
			if (!file.good()) {
				Logger << LOGGER_CRITICAL << "[AssetMesh]: Could not find MTL " << filename << " or lacking reading rights!" << std::endl;
				return;
			}

			std::string line;
			std::vector<std::string> tokens;
			while (std::getline(file, line)) {
				//tokenize
				Tokenize(line, tokens);

				//if nothing discard
				if (tokens.size() == 0) continue;

				//if comment discard
				if (tokens.size() > 0 && tokens[0].at(0) == '#') continue;

				//new material
				if (tokens.size() > 1 && tokens[0] == "newmtl") {
					materials.push_back(ObjMaterial());
					materials[materials.size() - 1].name = filename_without_extension + tokens[1];
				}

				//ambiental
				if (tokens.size() > 3 && (tokens[0] == "Ka")) {
					materials[materials.size() - 1].ambiental = glm::vec3(ToFloat(tokens[1]), ToFloat(tokens[2]), ToFloat(tokens[3]));
				}
				//diffuse
				if (tokens.size() > 3 && (tokens[0] == "Kd")) {
					materials[materials.size() - 1].diffuse_transparency.x = ToFloat(tokens[1]);
					materials[materials.size() - 1].diffuse_transparency.y = ToFloat(tokens[2]);
					materials[materials.size() - 1].diffuse_transparency.z = ToFloat(tokens[3]);
				}
				//transparency
				if (tokens.size() > 1 && (tokens[0] == "Tr" || tokens[0] == "d")) {
					materials[materials.size() - 1].diffuse_transparency.w = ToFloat(tokens[1]);
				}
				//specular
				if (tokens.size() > 3 && (tokens[0] == "Ks")) {
					materials[materials.size() - 1].specular_exponent.x = ToFloat(tokens[1]);
					materials[materials.size() - 1].specular_exponent.y = ToFloat(tokens[2]);
					materials[materials.size() - 1].specular_exponent.z = ToFloat(tokens[3]);
				}
				//specular exponent 
				if (tokens.size() > 1 && (tokens[0] == "Ns")) {
					materials[materials.size() - 1].specular_exponent.w = ToFloat(tokens[1]);
				}
				//transmission filter
				if (tokens.size() > 3 && (tokens[0] == "Tf")) {
					materials[materials.size() - 1].transmission = glm::vec3(ToFloat(tokens[1]), ToFloat(tokens[2]), ToFloat(tokens[3]));
				}
				//color map
				if (tokens.size() > 1 && (tokens[0] == "map_Kd" || tokens[0] == "map_ad")) {
					materials[materials.size() - 1].colormap_file = tokens[1];
				}
				//alpha map
				if (tokens.size() > 1 && (tokens[0] == "map_d")) {
					materials[materials.size() - 1].alphamap_file = tokens[1];
				}
				//specular map
				if (tokens.size() > 1 && (tokens[0] == "map_Ks")) {
					materials[materials.size() - 1].specularmap_file = tokens[1];
				}
				//displacement map
				if (tokens.size() > 1 && (tokens[0] == "disp" || tokens[0] == "map_disp")) {
					materials[materials.size() - 1].displacementmap_file = tokens[1];
				}
				//normal map, bump map
				if (tokens.size() > 1 && (tokens[0] == "map_norm" || tokens[0] == "map_bump" || tokens[0] == "bump")) {
					materials[materials.size() - 1].normalmap_file = tokens[1];
				}
			}

			file.close();

			if (materials.size() == 0) {
				Logger << LOGGER_CRITICAL << "[AssetMesh]: MTL file " << filename << " does not contain any materials!" << std::endl;
			}
			else {
				Logger << LOGGER_NORMAL << "[AssetMesh]: MTL file " << filename << " loaded in " << timer.ElapsedSeconds() << " seconds." << std::endl;
			}
		}

		//-------------------------------------------------------------------------------------------------
		//parsing code...
		//variant for faces
		void TokenizeOBJFace(const std::string &source, std::vector<std::string> &tokens) {
			std::string aux = source;
			for (unsigned int i = 0; i < aux.size(); i++) if (aux[i] == '\\' || aux[i] == '/') aux[i] = ' ';
			Tokenize(aux, tokens);
		}


		struct Hash_Vertex {
			size_t operator()(const Vertex &v) const {
				return std::hash<float>()(v.position.x + v.position.y + v.position.z) ^ std::hash<float>()(v.normal.x) ^ std::hash<float>()(v.normal.y);
			}
		};

		// if v is found in vertices then it is not added,
		// returns the index of the vertex in vertices
		unsigned int AddVertex(std::unordered_multimap<Vertex, unsigned int, Hash_Vertex>& dictionary, std::vector<Vertex> &vertices, const Vertex& v) {
			auto its = dictionary.equal_range(v);
			for (auto it = its.first; it != its.second; ++it) {
				if (v == it->first) return it->second;
			}
			vertices.push_back(v);
			unsigned int vertex_index = (int)vertices.size() - 1;
			//dictionary.insert(std::unordered_multimap<Vertex, unsigned int, Hash_Vertex>::value_type(v, vertex_index));
			dictionary.insert(std::make_pair(v, vertex_index));
			return vertex_index;
		}

		//--------------------------------------------------------------------------------------------
		// Format: http://paulbourke.net/dataformats/obj/
		void AssetLoadOBJFile(const std::string &filename, std::vector<Vertex>& vertices, std::vector<unsigned int>& indices, std::vector<ObjMaterial>& materials, std::vector<ObjMesh>& meshes) {
			Timer timer;

			//filename without extension
			std::string filename_without_extension = filename.substr(0, filename.find_last_of('.'));

			//determine the MTL file
			std::string mtl_file = filename.substr(0, filename.find_last_of('.')) + ".mtl";

			//get materials from MTL file, but also add a default material for obj files without mtl
			materials.clear();
			AssetLoadMTLFile(mtl_file, materials);
			if (materials.size() == 0) {
				materials.push_back(ObjMaterial());
				materials[0].diffuse_transparency = glm::vec4(0.8, 0.8, 0.8, 0.0);
				materials[0].ambiental = glm::vec3(0.2, 0.2, 0.2);
				materials[0].specular_exponent = glm::vec4(1, 1, 1, 50);
			}



			//open OBJ file
			std::ifstream file(filename.c_str(), std::ios::in | std::ios::binary);
			if (!file.good()) {
				Logger << LOGGER_CRITICAL << "[AssetMesh]: Could not find OBJ " << filename << " or lacking reading rights!" << std::endl;
				std::terminate();
			}



			unsigned int index_current_material = 0;		//guaranteed to exist
			unsigned int index_current_first = 0;			//first index of the current sub-mesh
			unsigned int index_current_count = 0;			//number of indices of the current sub-mesh
			vertices.reserve(10000);
			indices.reserve(10000);
			std::vector<unsigned int> face_indices(10);
			std::unordered_multimap<Vertex, unsigned int, Hash_Vertex> dictionary;

			std::string line;
			std::vector<std::string> tokens, facetokens;
			std::vector<glm::vec3> positions;		positions.reserve(1000);
			std::vector<glm::vec3> normals;			normals.reserve(1000);
			std::vector<glm::vec2> texcoords;		texcoords.reserve(1000);
			while (std::getline(file, line)) {
				//tokenize
				Tokenize(line, tokens);

				//if nothing discard
				if (tokens.size() == 0) continue;

				//if comment discard
				if (tokens.size() > 0 && tokens[0].at(0) == '#') continue;

				//if vertex
				if (tokens.size() > 3 && tokens[0] == "v") {
					float px = ToFloat(tokens[1]);
					float py = ToFloat(tokens[2]);
					float pz = ToFloat(tokens[3]);
					positions.push_back(glm::vec3(px, py, pz));
				}

				//if normal
				if (tokens.size() > 3 && tokens[0] == "vn") normals.push_back(glm::vec3(ToFloat(tokens[1]), ToFloat(tokens[2]), ToFloat(tokens[3])));

				//if texcoord
				if (tokens.size() > 2 && tokens[0] == "vt") texcoords.push_back(glm::vec2(ToFloat(tokens[1]), ToFloat(tokens[2])));

				//if face (at least 3 indices)
				if (tokens.size() >= 4 && tokens[0] == "f") {

					const unsigned int FACE_TYPE_NONE = 0;
					const unsigned int FACE_TYPE_V = 1;
					const unsigned int FACE_TYPE_VT = 2;
					const unsigned int FACE_TYPE_VN = 3;
					const unsigned int FACE_TYPE_VTN = 4;

					//use the first face vertex to determine face format (v v/t v//n v/t/n) = (1 2 3 4)
					unsigned int face_format = FACE_TYPE_NONE;
					if (tokens[1].find("//") != std::string::npos) face_format = FACE_TYPE_VN;	//3, vertices and normals
					TokenizeOBJFace(tokens[1], facetokens);
					if (facetokens.size() == 3) face_format = FACE_TYPE_VTN;	//4, vertices, texcoords, normals
					else {
						if (facetokens.size() == 2) {
							if (face_format != 3) face_format = FACE_TYPE_VT;	//2, vertices texcoords
						}
						else {
							face_format = FACE_TYPE_V;							//1, only vertices
						}
					}

					//indices lookup
					if (tokens.size() > face_indices.size()) face_indices.resize(tokens.size());

					//for each token in the face
					for (unsigned int num_token = 1; num_token<tokens.size(); num_token++) {
						if (tokens[num_token].at(0) == '#') break;						//comment after face definition
						TokenizeOBJFace(tokens[num_token], facetokens);
						if (face_format == FACE_TYPE_V) {
							//only positions
							int p_index = ToInt(facetokens[0]);
							if (p_index>0) p_index -= 1;								//obj has 1...n indices
							else p_index = (int)positions.size() + p_index;					//negative index

							Vertex v;
							v.position = glm::vec3(positions[p_index].x, positions[p_index].y, positions[p_index].z);
							face_indices[num_token] = AddVertex(dictionary, vertices, v);
						}
						else if (face_format == FACE_TYPE_VT) {
							// position + texcoord
							int p_index = ToInt(facetokens[0]);
							if (p_index > 0) p_index -= 1;								//obj has 1...n indices
							else p_index = (int)positions.size() + p_index;					//negative index

							int t_index = ToInt(facetokens[1]);
							if (t_index > 0) t_index -= 1;								//obj has 1...n indices
							else t_index = (int)texcoords.size() + t_index;					//negative index

							Vertex v;
							v.position = glm::vec3(positions[p_index].x, positions[p_index].y, positions[p_index].z);
							v.texcoord = glm::vec3(texcoords[t_index].x, texcoords[t_index].y, 0);
							face_indices[num_token] = AddVertex(dictionary, vertices, v);
						}
						else if (face_format == FACE_TYPE_VN) {
							//position and normal
							int p_index = ToInt(facetokens[0]);
							if (p_index > 0) p_index -= 1;								//obj has 1...n indices
							else p_index = (int)positions.size() + p_index;					//negative index

							int n_index = ToInt(facetokens[1]);
							if (n_index > 0) n_index -= 1;								//obj has 1...n indices
							else n_index = (int)normals.size() + n_index;					//negative index

							Vertex v;
							v.position = glm::vec3(positions[p_index].x, positions[p_index].y, positions[p_index].z);
							v.normal = glm::vec3(normals[n_index].x, normals[n_index].y, normals[n_index].z);
							face_indices[num_token] = AddVertex(dictionary, vertices, v);
						}
						else {
							//FACE_TYPE_VTN

							//position, normal, texcoord
							int p_index = ToInt(facetokens[0]);
							if (p_index > 0) p_index -= 1;								//obj has 1...n indices
							else p_index = (int)positions.size() + p_index;					//negative index

							int t_index = ToInt(facetokens[1]);
							if (t_index > 0) t_index -= 1;								//obj has 1...n indices
							else t_index = (int)normals.size() + t_index;					//negative index

							int n_index = ToInt(facetokens[2]);
							if (n_index > 0) n_index -= 1;								//obj has 1...n indices
							else n_index = (int)normals.size() + n_index;					//negative index

							Vertex v;
							v.position = glm::vec3(positions[p_index].x, positions[p_index].y, positions[p_index].z);
							v.normal = glm::vec3(normals[n_index].x, normals[n_index].y, normals[n_index].z);
							v.texcoord = glm::vec3(texcoords[t_index].x, texcoords[t_index].y, 0);
							face_indices[num_token] = AddVertex(dictionary, vertices, v);
						}

						//indices
						if (num_token < 4) {
							// for the first 3 vertices of the face (which compose the first triangle)
							indices.push_back(face_indices[num_token]);
							index_current_count++;
						}
						else {
							// for the other vertices of the face, whch simply link with the previous 2 vertices to form triangle fans around the first vertex
							indices.push_back(face_indices[1]);
							indices.push_back(face_indices[num_token - 1]);
							indices.push_back(face_indices[num_token]);
							index_current_count += 3;
						}
					}//end for
				}//end face

				 //group, as in a subobject
				if (tokens.size() > 1 && tokens[0] == "g") {

					//finalize the previous submesh
					if (meshes.size() > 0) {
						meshes[meshes.size() - 1].index_count = index_current_count;
						meshes[meshes.size() - 1].index_first = index_current_first;
						meshes[meshes.size() - 1].index_material = index_current_material;

						//compute submesh aabb
						for (unsigned int i = index_current_first; i < (index_current_first + index_current_count); i++) {
							meshes[meshes.size() - 1].aabb.AddPoint(vertices[indices[i]].position);
						}
					}

					//update current indices..... there can't be any faces declared when the first sub-mesh is introduced, so its first=0
					index_current_first = index_current_first + index_current_count;
					index_current_count = 0;

					//create new submesh
					meshes.push_back(ObjMesh());
					meshes[meshes.size() - 1].name = filename_without_extension + tokens[1];

				}

				//use material
				if (tokens.size() > 1 && tokens[0] == "usemtl") {
					std::string matname = filename_without_extension + tokens[1];
					for (unsigned int i = 0; i < materials.size(); i++) {
						if (materials[i].name == matname) {
							index_current_material = i;
							break;
						}
						else if (i == materials.size() - 1) {
							//if it can't be found continue using the last material
							Logger << LOGGER_CRITICAL << "[AssetMesh] Could not find material " << tokens[1] << ", using previous material." << std::endl;
						}
					}
				}


			}//end while

			 //finalize the last submesh mesh
			if (meshes.size() > 0) {
				meshes[meshes.size() - 1].index_count = index_current_count;
				meshes[meshes.size() - 1].index_first = index_current_first;
				meshes[meshes.size() - 1].index_material = index_current_material;

				//compute submesh aabb
				for (unsigned int i = index_current_first; i < (index_current_first + index_current_count); i++) {
					meshes[meshes.size() - 1].aabb.AddPoint(vertices[indices[i]].position);
				}
			}
			else {
				//OR ONLY ONE MESH 

				meshes.push_back(ObjMesh());
				meshes[meshes.size() - 1].name = filename_without_extension;
				meshes[meshes.size() - 1].index_count = index_current_count;
				meshes[meshes.size() - 1].index_first = index_current_first;
				meshes[meshes.size() - 1].index_material = index_current_material;

				//compute submesh aabb
				for (unsigned int i = 0; i < index_current_count; i++) {
					meshes[meshes.size() - 1].aabb.AddPoint(vertices[indices[i]].position);
				}
			}

			Logger << LOGGER_NORMAL << "[AssetMesh]: Loaded file " << filename << " in " << timer.ElapsedSeconds() << " seconds." << std::endl;
		}
	}


	//--------------------------------------------------------------------------------------------
	//this considers the indices as a series of COUNTER CLOCKWISE TRIANGLES. 
	void MeshComputeNormals(std::vector<Vertex>& vertices, std::vector<unsigned int>& indices) {
		Timer timer;
		Logger << LOGGER_NORMAL << "[AssetMesh] Normal computation started" << std::endl;

		//for all faces
		//for (unsigned int i = 0; i < vertices.size(); i++) vertices[i].normal = glm::vec3(0);
		for (unsigned int i = 0; i < indices.size(); i = i + 3) {
			unsigned int index1 = indices[i];
			unsigned int index2 = indices[i + 1];
			unsigned int index3 = indices[i + 2];
			glm::vec3 v1 = vertices[index1].position;
			glm::vec3 v2 = vertices[index2].position;
			glm::vec3 v3 = vertices[index3].position;

			glm::vec3 d2 = v3 - v2;
			glm::vec3 d1 = v1 - v2;
			glm::vec3 facenormal = glm::cross(d2, d1);
			if (facenormal != glm::vec3(0)) facenormal = glm::normalize(facenormal);
			vertices[index1].normal += facenormal;
			vertices[index2].normal += facenormal;
			vertices[index3].normal += facenormal;
		}
		for (unsigned int i = 0; i < vertices.size(); i++) vertices[i].normal = glm::normalize(vertices[i].normal);
		Logger << LOGGER_NORMAL << "[AssetMesh] Normal computation took " << timer.ElapsedSinceLastCallSeconds() << " seconds." << std::endl;
	}

	//--------------------------------------------------------------------------------------------
	//this considers the indices as a series of COUNTER CLOCKWISE TRIANGLES. 
	void MeshComputeTangents(std::vector<Vertex>& vertices, std::vector<unsigned int>& indices) {

		Timer timer;
		Logger << LOGGER_NORMAL << "[AssetMesh] Tangent computation started" << std::endl;

		//for all faces
		for (unsigned int i = 0; i < indices.size(); i = i + 3) {
			unsigned int index1 = indices[i];
			unsigned int index2 = indices[i + 1];
			unsigned int index3 = indices[i + 2];

			glm::vec3 v1 = vertices[index1].position;
			glm::vec3 v2 = vertices[index2].position;
			glm::vec3 v3 = vertices[index3].position;
			glm::vec3 w1 = vertices[index1].texcoord;
			glm::vec3 w2 = vertices[index2].texcoord;
			glm::vec3 w3 = vertices[index3].texcoord;

			float x1 = v2.x - v1.x;
			float x2 = v3.x - v1.x;
			float y1 = v2.y - v1.y;
			float y2 = v3.y - v1.y;
			float z1 = v2.z - v1.z;
			float z2 = v3.z - v1.z;

			float s1 = w2.x - w1.x;
			float s2 = w3.x - w1.x;
			float t1 = w2.y - w1.y;
			float t2 = w3.y - w1.y;

			float r;
			if (s1 * t2 - s2 * t1) r = 1.0f / (s1 * t2 - s2 * t1);
			else r = 0;

			glm::vec3 sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
			glm::vec3 tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);

			vertices[index1].tangent += sdir;
			vertices[index2].tangent += sdir;
			vertices[index3].tangent += sdir;
		}
		//normalize, then orthogonalize with normal, per vertex
		for (unsigned int i = 0; i<vertices.size(); i++) {
			if (vertices[i].tangent == glm::vec3(0)) continue;
			glm::vec3 t = glm::normalize(vertices[i].tangent);
			glm::vec3 n = vertices[i].normal;

			glm::vec3 tnorm = glm::normalize(t - n * glm::dot(n, t));
			vertices[i].tangent = tnorm;
		}

		Logger << LOGGER_NORMAL << "[AssetMesh] Tangent computation took " << timer.ElapsedSinceLastCallSeconds() << " seconds." << std::endl;
	}


}