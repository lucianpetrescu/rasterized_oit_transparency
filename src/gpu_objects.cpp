//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

#include "gpu_objects.hpp"

namespace oit {

	//--------------------------------------------------------------------------------------------
	GPUProgram::GPUProgram() {
		shader = detail::UNINITIALIZED; type = 0;
	}
	GPUProgram::GPUProgram(const std::string& compute_file) {
		shader = detail::UNINITIALIZED; type = 0;
		Set(compute_file);
	}
	GPUProgram::GPUProgram(const std::string& vertex_file, const std::string& fragment_file) {
		shader = detail::UNINITIALIZED; type = 0;
		Set(vertex_file, fragment_file);
	}
	GPUProgram::GPUProgram(const std::string& vertex_file, const std::string& geometry_file, const std::string& fragment_file) {
		shader = detail::UNINITIALIZED; type = 0;
		Set(vertex_file, geometry_file, fragment_file);
	}
	GPUProgram::GPUProgram(const std::string& vertex_file, const std::string& tess_ctrl_file, const std::string& tess_eval_file, const std::string& fragment_file) {
		shader = detail::UNINITIALIZED; type = 0;
		Set(vertex_file, tess_ctrl_file, tess_eval_file, fragment_file);
	}
	GPUProgram::GPUProgram(const std::string& vertex_file, const std::string& tess_ctrl_file, const std::string& tess_eval_file, const std::string& geometry_file, const std::string& fragment_file) {
		shader = detail::UNINITIALIZED; type = 0;
		Set(vertex_file, tess_ctrl_file, tess_eval_file, geometry_file, fragment_file);
	}
	void GPUProgram::Set(const std::string& compute_file) {
		type = detail::SHADER_TYPE_C;
		this->compute_file = compute_file;
		Reload();
	}
	void GPUProgram::Set(const std::string& vertex_file, const std::string& fragment_file) {
		type = detail::SHADER_TYPE_VF;
		this->vertex_file = vertex_file;
		this->fragment_file = fragment_file;
		Reload();
	}
	void GPUProgram::Set(const std::string& vertex_file, const std::string& geometry_file, const std::string& fragment_file) {
		type = detail::SHADER_TYPE_VGF;
		this->vertex_file = vertex_file;
		this->geometry_file = geometry_file;
		this->fragment_file = fragment_file;
		Reload();
	}
	void GPUProgram::Set(const std::string& vertex_file, const std::string& tess_ctrl_file, const std::string& tess_eval_file, const std::string& fragment_file) {
		type = detail::SHADER_TYPE_VTTF;
		this->vertex_file = vertex_file;
		this->tess_ctrl_file = tess_ctrl_file;
		this->tess_eval_file = tess_eval_file;
		this->fragment_file = fragment_file;
		Reload();
	}
	void GPUProgram::Set(const std::string& vertex_file, const std::string& tess_ctrl_file, const std::string& tess_eval_file, const std::string& geometry_file, const std::string& fragment_file) {
		type = detail::SHADER_TYPE_VTTGF;
		this->vertex_file = vertex_file;
		this->tess_ctrl_file = tess_ctrl_file;
		this->tess_eval_file = tess_eval_file;
		this->geometry_file = geometry_file;
		this->fragment_file = fragment_file;
		Reload();
	}
	GPUProgram::~GPUProgram() {
		if (shader != detail::UNINITIALIZED) glDeleteProgram(shader);
	}
	void GPUProgram::Reload() {
		//rebuild
		if (shader != detail::UNINITIALIZED) glDeleteProgram(shader);
		if (type == detail::SHADER_TYPE_C)     shader = detail::AssetLoadShader(compute_file);
		if (type == detail::SHADER_TYPE_VF)    shader = detail::AssetLoadShader(vertex_file, fragment_file);
		if (type == detail::SHADER_TYPE_VGF)   shader = detail::AssetLoadShader(vertex_file, geometry_file, fragment_file);
		if (type == detail::SHADER_TYPE_VTTF)  shader = detail::AssetLoadShader(vertex_file, tess_ctrl_file, tess_eval_file, fragment_file);
		if (type == detail::SHADER_TYPE_VTTGF) shader = detail::AssetLoadShader(vertex_file, tess_ctrl_file, tess_eval_file, geometry_file, fragment_file);
		//clear locations as they can't be valid any more
		uniform_locations.clear();
	}
	unsigned int GPUProgram::GetType() { return type; }

	void GPUProgram::SetUniform(const std::string& name, int value) {
		int location = _GetLocation(name);
		if (location != -1) glUniform1i(location, value);
	}
	void GPUProgram::SetUniform(const std::string& name, unsigned int value) {
		int location = _GetLocation(name);
		if (location != -1) glUniform1ui(location, value);
	}
	void GPUProgram::SetUniform(const std::string& name, float value) {
		int location = _GetLocation(name);
		if (location != -1) glUniform1f(location, value);
	}
	void GPUProgram::SetUniform(const std::string& name, const glm::vec2& value) {
		SetUniform(name, value.x, value.y);
	}
	void GPUProgram::SetUniform(const std::string& name, float value0, float value1) {
		int location = _GetLocation(name);
		if (location != -1) glUniform2f(location, value0, value1);
	}
	void GPUProgram::SetUniform(const std::string& name, const glm::vec3& value) {
		SetUniform(name, value.x, value.y, value.z);
	}
	void GPUProgram::SetUniform(const std::string& name, float value0, float value1, float value2) {
		int location = _GetLocation(name);
		if (location != -1) glUniform3f(location, value0, value1, value2);
	}
	void GPUProgram::SetUniform(const std::string& name, const glm::vec4& value) {
		SetUniform(name, value.x, value.y, value.z, value.w);
	}
	void GPUProgram::SetUniform(const std::string& name, float value0, float value1, float value2, float value3) {
		int location = _GetLocation(name);
		if (location != -1) glUniform4f(location, value0, value1, value2, value3);
	}
	void GPUProgram::SetUniform(const std::string& name, const glm::mat4& m, bool transpose) {
		int location = _GetLocation(name);
		if (location != -1) glUniformMatrix4fv(location, 1, transpose, glm::value_ptr(m));
	}
	void GPUProgram::Bind() {
		glUseProgram(shader);
	}
	unsigned int GPUProgram::GetGLShader() { return shader; }
	int GPUProgram::_GetLocation(const std::string& name) {
		//exists?
		auto it = uniform_locations.find(name);
		if (it != uniform_locations.end()) return it->second;

		//if not, add
		int location = glGetUniformLocation(shader, name.c_str());
		if (location == -1) Logger << LOGGER_NORMAL << "[GPUProgram] WARNING, variable " << name << " does not exist or it was optimzed out by the compiler because it is not used." << std::endl;
		uniform_locations[name] = location;		//add even if not found (-1) in order to not produce the same message N times
		return location;
	}


	//--------------------------------------------------------------------------------------------
	GPUBuffer::GPUBuffer() {
			buffer = detail::UNINITIALIZED;
			memory = 0;
		}
	GPUBuffer::GPUBuffer(float* data, unsigned int size, unsigned int usage) {
			buffer = detail::UNINITIALIZED;
			memory = 0;
			Update(data, size, usage);
		}
	GPUBuffer::GPUBuffer(unsigned int* data, unsigned int size, unsigned int usage) {
			buffer = detail::UNINITIALIZED;
			memory = 0;
			Update(data, size, usage);
		}
	void GPUBuffer::Update(void* data, unsigned int size, unsigned int usage) {
		if (buffer == detail::UNINITIALIZED) glGenBuffers(1, &buffer);

		memory = size;
		glBindBuffer(GL_ARRAY_BUFFER, buffer);
		if (usage == detail::BUFFER_USAGE_GPUSTATIC)	glBufferData(GL_ARRAY_BUFFER, memory, data, GL_STATIC_DRAW);
		if (usage == detail::BUFFER_USAGE_GPUDYNAMIC)	glBufferData(GL_ARRAY_BUFFER, memory, data, GL_DYNAMIC_COPY);
		if (usage == detail::BUFFER_USAGE_CPU_GPU)		glBufferData(GL_ARRAY_BUFFER, memory, data, GL_STREAM_DRAW);	//driver decides anyway..

	}
	void GPUBuffer::Copy(void* data, unsigned int size) {
		assert(buffer != detail::UNINITIALIZED && "[GPUBuffer] copying from uninitialized buffer");
		glBindBuffer(GL_ARRAY_BUFFER, buffer);
		unsigned int* ptr = (unsigned int*)glMapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY);
		//std::cout << "PTR= "<<ptr[0] << " " << ptr[1] << " " << ptr[2] << " " << ptr[3] << std::endl;
		//ptr[0]; ptr[1]; ptr[2]; ptr[3];
		std::memcpy(data, ptr, size);
		glUnmapBuffer(GL_ARRAY_BUFFER);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

	}
	GPUBuffer::~GPUBuffer() {
		if (buffer != detail::UNINITIALIZED) glDeleteBuffers(1, &buffer);
	}
	void GPUBuffer::Bind(unsigned int target) {
		assert((buffer) && "[GPUBuffer] trying to bind an uninitialized gpu buffer");
		if (target == detail::BUFFER_TARGET_VERTEX) glBindBuffer(GL_ARRAY_BUFFER, buffer);
		else if (target == detail::BUFFER_TARGET_INDEX) glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer);
		else if (target == detail::BUFFER_TARGET_STORAGE) glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer);
		else if (target == detail::BUFFER_TARGET_DRAW_INDIRECT) glBindBuffer(GL_DRAW_INDIRECT_BUFFER, buffer);
		else if (target == detail::BUFFER_TARGET_DISPATCH_INDIRECT) glBindBuffer(GL_DISPATCH_INDIRECT_BUFFER, buffer);
		else if (target == detail::BUFFER_TARGET_ATOMIC_COUNTER) glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, buffer);
		else if (target == detail::BUFFER_TARGET_UNIFORM) glBindBuffer(GL_UNIFORM_BUFFER, buffer);
		else assert(false && "[GPUBuffer] unsupported binding target.");
	}
	void GPUBuffer::BindIndexed(unsigned int target, unsigned int index) {
		assert((buffer) && "[GPUBuffer] trying to bind an uninitialized gpu buffer");
		if (target == detail::BUFFER_TARGET_STORAGE) glBindBufferBase(GL_SHADER_STORAGE_BUFFER, index, buffer);
		else if (target == detail::BUFFER_TARGET_ATOMIC_COUNTER) glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, index, buffer);
		else if (target == detail::BUFFER_TARGET_UNIFORM) glBindBufferBase(GL_UNIFORM_BUFFER, index, buffer);
		else assert(false && "[GPUBuffer] unsupported indexed binding target.");
	}
	unsigned int GPUBuffer::GetMemory() { return memory; }
	unsigned int GPUBuffer::GetGLbuffer() { return buffer; }


	//--------------------------------------------------------------------------------------------
	// no creation through already initialized gpu buffers.
	GPUMesh::GPUMesh() {
			vao = detail::UNINITIALIZED;
			index_first = index_count = 0;
			topology = detail::TOPOLOGY_TRIANGLES;
			buffer_index = nullptr;
			buffer_index = nullptr;
		}
		//many meshes can share the same buffers so the offset is needed
	GPUMesh::GPUMesh(GPUBuffer *vertex_buffer, GPUBuffer* index_buffer, unsigned int index_first, unsigned int index_count, const AABB& aabb, unsigned int topology) {
			assert((vertex_buffer && index_buffer) && "[Mesh] no vertex or index buffers!");
			this->aabb = aabb;
			this->index_first = index_first;
			this->index_count = index_count;
			this->topology = topology;

			this->buffer_vertex = vertex_buffer;
			this->buffer_index = index_buffer;

			glGenVertexArrays(1, &vao);
			glBindVertexArray(vao);
			buffer_vertex->Bind(detail::BUFFER_TARGET_VERTEX);
			buffer_index->Bind(detail::BUFFER_TARGET_INDEX);
			glEnableVertexAttribArray(0);	//pos
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);							//vertices always start at 0
			glEnableVertexAttribArray(1);	//normal
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(glm::vec3)));
			glEnableVertexAttribArray(2);	//tangent
			glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(2 * sizeof(glm::vec3)));
			glEnableVertexAttribArray(3);	//texcoord
			glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(3 * sizeof(glm::vec3)));
			glEnableVertexAttribArray(4);	//extra
			glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(4 * sizeof(glm::vec3)));
		}
	GPUMesh::~GPUMesh() {
			if (vao != detail::UNINITIALIZED) glDeleteVertexArrays(1, &vao);
		}
	void GPUMesh::Draw(unsigned int instances ) {
		unsigned int offset = index_first * sizeof(unsigned int);												//indices start from an offset
		assert((vao) && "[Mesh] trying to draw an incomplete mesh");
		glBindVertexArray(vao);
		if (topology == detail::TOPOLOGY_POINTS)		 glDrawElementsInstanced(GL_POINTS, index_count, GL_UNSIGNED_INT, (void*)(size_t)offset, instances);
		else if (topology == detail::TOPOLOGY_LINES)     glDrawElementsInstanced(GL_LINES, index_count, GL_UNSIGNED_INT, (void*)(size_t)offset, instances);
		else if (topology == detail::TOPOLOGY_TRIANGLES) glDrawElementsInstanced(GL_TRIANGLES, index_count, GL_UNSIGNED_INT, (void*)(size_t)offset, instances);
		else if (topology == detail::TOPOLOGY_PATCHES_2) {
			glPatchParameteri(GL_PATCH_VERTICES, 2);
			glDrawElementsInstanced(GL_PATCHES, index_count, GL_UNSIGNED_INT, (void*)(size_t)offset, instances);
		}
		else if (topology == detail::TOPOLOGY_PATCHES_3) {
			glPatchParameteri(GL_PATCH_VERTICES, 3);
			glDrawElementsInstanced(GL_PATCHES, index_count, GL_UNSIGNED_INT, (void*)(size_t)offset, instances);
		}
		else if (topology == detail::TOPOLOGY_PATCHES_4) {
			glPatchParameteri(GL_PATCH_VERTICES, 4);
			glDrawElementsInstanced(GL_PATCHES, index_count, GL_UNSIGNED_INT, (void*)(size_t)offset, instances);
		}
		else assert(false && "[Mesh] unsupported topology");
	}
	const AABB& GPUMesh::GetAABB() { return aabb; }
	GPUBuffer* GPUMesh::GetBufferVertex() { return buffer_vertex; }
	GPUBuffer* GPUMesh::GetBufferIndex() { return buffer_index; }
	unsigned int GPUMesh::GetGLvao() { return vao; }
	unsigned int GPUMesh::GetTriangleCount() { return index_count / 3; }
	unsigned int GPUMesh::GetFirstIndexOffset() { return index_first * sizeof(unsigned int); }
	unsigned int GPUMesh::GetFirstIndex() { return index_first; }
	unsigned int GPUMesh::GetIndexCount() { return index_count; }



	//---------------------------------------------------------------------------------------------
	GPUTexture::GPUTexture() {
		tex = detail::UNINITIALIZED;
		data_type = detail::UNINITIALIZED;
		mipmaps = false;
		width = height = channels = memory = 0;
	}
	//data can be nullptr, which will create an empty texture
	void GPUTexture::Create(int *data, unsigned int width, unsigned int height, unsigned int channels, bool mipmaps) {
		Create((void*)data, detail::DATA_TYPE_INT, width, height, channels, mipmaps);
	}
	//data can be nullptr, which will create an empty texture
	void GPUTexture::Create(unsigned int *data, unsigned int width, unsigned int height, unsigned int channels, bool mipmaps) {
		Create((void*)data, detail::DATA_TYPE_UINT, width, height, channels, mipmaps);
	}
	//data can be nullptr, which will create an empty texture
	void GPUTexture::Create(float *data, unsigned int width, unsigned int height, unsigned int channels, bool mipmaps) {
		Create((void*)data, detail::DATA_TYPE_FLOAT, width, height, channels, mipmaps);
	}
	//data can be nullptr, which will create an empty texture
	void GPUTexture::Create(unsigned char *data, unsigned int width, unsigned int height, unsigned int channels, bool mipmaps) {
		Create((void*)data, detail::DATA_TYPE_UCHAR, width, height, channels, mipmaps);
	}
	//creates an empty depth texture
	void GPUTexture::CreateDepth(unsigned int width, unsigned int height) {
		Create(nullptr, detail::DATA_TYPE_DEPTH, width, height, 1, false);
	}
	//data is lost, should be used only for size varying textures like those used in a framebuffer.
	void GPUTexture::Resize(unsigned int width, unsigned int height) {
		assert(channels && "[Texture] can't resize uninitialized texture");
		Create(nullptr, data_type, width, height, channels, mipmaps);
	}
	GPUTexture::~GPUTexture() {
		if (tex != detail::UNINITIALIZED) glDeleteTextures(1, &tex);
	}
	void GPUTexture::Bind(unsigned int texture_unit) {
		assert(tex && "[Texture] can't use uninitialized texture");
		glActiveTexture(GL_TEXTURE0 + texture_unit);
		glBindTexture(GL_TEXTURE_2D, tex);
	}
	void GPUTexture::BindAsImage(unsigned int texture_unit, unsigned int access) {
		assert(tex && "[Texture] can't use uninitialized texture");
		glActiveTexture(GL_TEXTURE0 + texture_unit);
		glBindTexture(GL_TEXTURE_2D, tex);

		GLenum gl_internal_format;
		if (data_type == detail::DATA_TYPE_DEPTH) gl_internal_format = GL_R32F;

		if (channels == 1 && data_type == detail::DATA_TYPE_FLOAT) gl_internal_format = GL_R32F;
		if (channels == 2 && data_type == detail::DATA_TYPE_FLOAT) gl_internal_format = GL_RG32F;
		if (channels == 3 && data_type == detail::DATA_TYPE_FLOAT) gl_internal_format = GL_RGB32F;
		if (channels == 4 && data_type == detail::DATA_TYPE_FLOAT) gl_internal_format = GL_RGBA32F;

		if (channels == 1 && data_type == detail::DATA_TYPE_INT) gl_internal_format = GL_R32I;
		if (channels == 2 && data_type == detail::DATA_TYPE_INT) gl_internal_format = GL_RG32I;
		if (channels == 3 && data_type == detail::DATA_TYPE_INT) gl_internal_format = GL_RGB32I;
		if (channels == 4 && data_type == detail::DATA_TYPE_INT) gl_internal_format = GL_RGBA32I;

		if (channels == 1 && data_type == detail::DATA_TYPE_UINT) gl_internal_format = GL_R32UI;
		if (channels == 2 && data_type == detail::DATA_TYPE_UINT) gl_internal_format = GL_RG32UI;
		if (channels == 3 && data_type == detail::DATA_TYPE_UINT) gl_internal_format = GL_RGB32UI;
		if (channels == 4 && data_type == detail::DATA_TYPE_UINT) gl_internal_format = GL_RGBA32UI;

		if (channels == 1 && data_type == detail::DATA_TYPE_UCHAR) gl_internal_format = GL_R8;
		if (channels == 2 && data_type == detail::DATA_TYPE_UCHAR) gl_internal_format = GL_RG8;
		if (channels == 3 && data_type == detail::DATA_TYPE_UCHAR) gl_internal_format = GL_RGB8;
		if (channels == 4 && data_type == detail::DATA_TYPE_UCHAR) gl_internal_format = GL_RGBA8;

		if (channels == 1 && data_type == detail::DATA_TYPE_UINT_8) gl_internal_format = GL_R8UI;
		if (channels == 2 && data_type == detail::DATA_TYPE_UINT_8) gl_internal_format = GL_RG8UI;
		if (channels == 3 && data_type == detail::DATA_TYPE_UINT_8) gl_internal_format = GL_RGB8UI;
		if (channels == 4 && data_type == detail::DATA_TYPE_UINT_8) gl_internal_format = GL_RGBA8UI;

		if (channels == 1 && data_type == detail::DATA_TYPE_FLOAT_16) gl_internal_format = GL_R16F;
		if (channels == 2 && data_type == detail::DATA_TYPE_FLOAT_16) gl_internal_format = GL_RG16F;
		if (channels == 3 && data_type == detail::DATA_TYPE_FLOAT_16) gl_internal_format = GL_RGB16F;
		if (channels == 4 && data_type == detail::DATA_TYPE_FLOAT_16) gl_internal_format = GL_RGBA16F;

		GLenum gl_access = GL_READ_ONLY;
		if (access == detail::ACCESS_READ_ONLY)			gl_access = GL_READ_ONLY;
		else if (access == detail::ACCESS_WRITE_ONLY)	gl_access = GL_WRITE_ONLY;
		else if (access == detail::ACCESS_READ_WRITE)	gl_access = GL_READ_WRITE;

		glBindImageTexture(texture_unit, tex, 0, GL_FALSE, 0, gl_access, gl_internal_format);
	}
	bool GPUTexture::HasMipmaps() { return mipmaps; }
	unsigned int GPUTexture::GetDataType() { return data_type; }
	unsigned int GPUTexture::GetChannels() { return channels; }
	unsigned int GPUTexture::GetMemory() { return memory; }
	unsigned int GPUTexture::GetWidth() { return width; }
	unsigned int GPUTexture::GetHeight() { return height; }
	unsigned int GPUTexture::GetGLObject() { return tex; }

	void GPUTexture::Create(void *data, unsigned int type, unsigned int width, unsigned int height, unsigned int channels, bool mipmaps) {
		this->data_type = type;
		this->width = width;
		this->height = height;
		this->channels = channels;
		if (type == detail::DATA_TYPE_DEPTH || type == detail::DATA_TYPE_FLOAT) memory = channels * width * height * sizeof(float);
		else if (type == detail::DATA_TYPE_INT) memory = channels * width * height * sizeof(int);
		else if (type == detail::DATA_TYPE_UINT) memory = channels * width * height * sizeof(unsigned int);
		else if (type == detail::DATA_TYPE_UCHAR) memory = channels * width * height * sizeof(unsigned char);

		//gen and basic filtering
		if (tex != detail::UNINITIALIZED) glDeleteTextures(1, &tex);
		glActiveTexture(GL_TEXTURE0);
		glGenTextures(1, &tex);
		glBindTexture(GL_TEXTURE_2D, tex);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		//depth
		if (data_type == detail::DATA_TYPE_DEPTH) {
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
			glTexParameterf(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
			glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
			return;
		}

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		if (channels == 1 && data_type == detail::DATA_TYPE_FLOAT) glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, width, height, 0, GL_RED, GL_FLOAT, data);
		else if (channels == 2 && data_type == detail::DATA_TYPE_FLOAT) glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32F, width, height, 0, GL_RG, GL_FLOAT, data);
		else if (channels == 3 && data_type == detail::DATA_TYPE_FLOAT) glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, data);
		else if (channels == 4 && data_type == detail::DATA_TYPE_FLOAT) glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, data);

		if (channels == 1 && data_type == detail::DATA_TYPE_INT) glTexImage2D(GL_TEXTURE_2D, 0, GL_R32I, width, height, 0, GL_RED_INTEGER, GL_INT, data);
		else if (channels == 2 && data_type == detail::DATA_TYPE_INT) glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32I, width, height, 0, GL_RG_INTEGER, GL_INT, data);
		else if (channels == 3 && data_type == detail::DATA_TYPE_INT) glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32I, width, height, 0, GL_RGB_INTEGER, GL_INT, data);
		else if (channels == 4 && data_type == detail::DATA_TYPE_INT) glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32I, width, height, 0, GL_RGBA_INTEGER, GL_INT, data);

		if (channels == 1 && data_type == detail::DATA_TYPE_UINT) glTexImage2D(GL_TEXTURE_2D, 0, GL_R32UI, width, height, 0, GL_RED_INTEGER, GL_UNSIGNED_INT, data);
		else if (channels == 2 && data_type == detail::DATA_TYPE_UINT) glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32UI, width, height, 0, GL_RG_INTEGER, GL_UNSIGNED_INT, data);
		else if (channels == 3 && data_type == detail::DATA_TYPE_UINT) glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32UI, width, height, 0, GL_RGB_INTEGER, GL_UNSIGNED_INT, data);
		else if (channels == 4 && data_type == detail::DATA_TYPE_UINT) glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32UI, width, height, 0, GL_RGBA_INTEGER, GL_UNSIGNED_INT, data);

		if (channels == 1 && data_type == detail::DATA_TYPE_UCHAR) glTexImage2D(GL_TEXTURE_2D, 0, GL_R, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, data);
		else if (channels == 2 && data_type == detail::DATA_TYPE_UCHAR) glTexImage2D(GL_TEXTURE_2D, 0, GL_RG, width, height, 0, GL_RG, GL_UNSIGNED_BYTE, data);
		else if (channels == 3 && data_type == detail::DATA_TYPE_UCHAR) glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		else if (channels == 4 && data_type == detail::DATA_TYPE_UCHAR) glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

		if (channels == 1 && data_type == detail::DATA_TYPE_UINT_8) glTexImage2D(GL_TEXTURE_2D, 0, GL_R8UI, width, height, 0, GL_RED_INTEGER, GL_UNSIGNED_INT, data);
		else if (channels == 2 && data_type == detail::DATA_TYPE_UINT_8) glTexImage2D(GL_TEXTURE_2D, 0, GL_RG8UI, width, height, 0, GL_RG_INTEGER, GL_UNSIGNED_INT, data);
		else if (channels == 3 && data_type == detail::DATA_TYPE_UINT_8) glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8UI, width, height, 0, GL_RGB_INTEGER, GL_UNSIGNED_INT, data);
		else if (channels == 4 && data_type == detail::DATA_TYPE_UINT_8) glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8UI, width, height, 0, GL_RGBA_INTEGER, GL_UNSIGNED_INT, data);

		if (channels == 1 && data_type == detail::DATA_TYPE_FLOAT_16) glTexImage2D(GL_TEXTURE_2D, 0, GL_R16F, width, height, 0, GL_RED, GL_FLOAT, data);
		else if (channels == 2 && data_type == detail::DATA_TYPE_FLOAT_16) glTexImage2D(GL_TEXTURE_2D, 0, GL_RG16F, width, height, 0, GL_RG, GL_FLOAT, data);
		else if (channels == 3 && data_type == detail::DATA_TYPE_FLOAT_16) glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, data);
		else if (channels == 4 && data_type == detail::DATA_TYPE_FLOAT_16) glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, data);

		if (data && mipmaps) {
			glGenerateMipmap(GL_TEXTURE_2D);
			this->mipmaps = true;
		}
		else this->mipmaps = false;
	}



	//--------------------------------------------------------------------------------------------
	GPUFramebuffer::GPUFramebuffer(unsigned int width, unsigned int height) {
		this->width = width;
		this->height = height;
		glGenFramebuffers(1, &fbo);
		depth_binding = nullptr;
	}
	GPUFramebuffer::~GPUFramebuffer() {
		glDeleteFramebuffers(1, &fbo);
	}
	void GPUFramebuffer::AddDepthBinding(GPUTexture* texture) {
		assert(texture && "[Framebuffer] binding null texture");
		assert((texture->GetWidth() == width) && (texture->GetHeight() == height) && "[Framebuffer] binding texture with unexpected size");
		depth_binding = texture;
		Finalize();
	}
	void GPUFramebuffer::RemoveDepthBinding() {
		depth_binding = nullptr;
		Finalize();
	}
	void GPUFramebuffer::AddBinding(GPUTexture* texture, unsigned int binding_point) {
		assert(texture && "[Framebuffer] binding null texture");
		assert((texture->GetWidth() == width) && (texture->GetHeight() == height) && "[Framebuffer] binding texture with unexpected size");
		for (auto& b : bindings) {
			if (b.first == texture && b.second == binding_point) return;
			if (b.second == binding_point) assert(false && "[Framebuffer] can't bind multiple textures to the same binding point");
		}
		bindings.push_back(std::make_pair(texture, binding_point));
		Finalize();
	}
	void GPUFramebuffer::RemoveBinding(GPUTexture* texture, unsigned int binding_point) {
		assert(texture && "[Framebuffer] removing null texture");
		bindings.erase(std::remove(bindings.begin(), bindings.end(), std::make_pair(texture, binding_point)), bindings.end());
		Finalize();
	}
	void GPUFramebuffer::Resize(unsigned int width, unsigned int height) {
		this->width = width;
		this->height = height;
		//depth
		if (depth_binding) depth_binding->Resize(width, height);
		//others
		for (auto& b : bindings) b.first->Resize(width, height);
		Finalize();
	}
	//prepare for rendering
	void GPUFramebuffer::Finalize() {
		//depth binding
		glBindFramebuffer(GL_FRAMEBUFFER, fbo);
		if (depth_binding) glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depth_binding->GetGLObject(), 0);

		///color bindings
		std::vector<GLenum> drawbuffers;
		for (auto& b : bindings) {
			glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + b.second, b.first->GetGLObject(), 0);
			drawbuffers.push_back(GL_COLOR_ATTACHMENT0 + b.second);
		}
		glDrawBuffers((int)drawbuffers.size(), &drawbuffers[0]);

		//check
		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (status == GL_FRAMEBUFFER_UNDEFINED) std::cout << "VALUE = undefined " << status << std::endl;
		if (status == GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT) std::cout << "VALUE = incomplete attachment " << status << std::endl;
		if (status == GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT) std::cout << "VALUE = incomplete missing attachment " << status << std::endl;
		if (status == GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER) std::cout << "VALUE = incomplete draw buffer " << status << std::endl;
		if (status == GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER) std::cout << "VALUE = incomplete read buffer " << status << std::endl;
		if (status == GL_FRAMEBUFFER_UNSUPPORTED) std::cout << "VALUE = unsupported " << status << std::endl;
		if (status == GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE) std::cout << "VALUE = incomplete multisample " << status << std::endl;
		if (status == GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS) std::cout << "VALUE = incomplete layer targets " << status << std::endl;
		assert((status == GL_FRAMEBUFFER_COMPLETE) && "[Framebuffer] framebuffer with incomplete status");
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	void GPUFramebuffer::Bind() {
		int screen_size[4]; glGetIntegerv(GL_VIEWPORT, &screen_size[0]);
		prev_screen_width = screen_size[2];
		prev_screen_height = screen_size[3];
		glBindFramebuffer(GL_FRAMEBUFFER, fbo);
		glViewport(0, 0, width, height);
	}
	void GPUFramebuffer::Unbind() {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, prev_screen_width, prev_screen_height);
	}

	unsigned int GPUFramebuffer::GetWidth() { return width; }
	unsigned int GPUFramebuffer::GetHeight() { return height; }
	unsigned int GPUFramebuffer::GetGLObject() { return fbo; }
	unsigned int GPUFramebuffer::GetPreviousScreenWidth() { return prev_screen_width; }
	unsigned int GPUFramebuffer::GetPreviousScreenHeight() { return prev_screen_height; }
	GPUTexture* GPUFramebuffer::GetDepthBinding() { return depth_binding; }
	const std::vector<std::pair<GPUTexture*, unsigned int>> GPUFramebuffer::GetBindings() { return bindings; }


	//-------------------------------------------------------------------------------------------------
	GPUTextureFilter::GPUTextureFilter() {
			glGenSamplers(1, &sampler);
		}
	GPUTextureFilter::~GPUTextureFilter() {
		glDeleteSamplers(1, &sampler);
	}
	void GPUTextureFilter::Set(unsigned int mode) {
		if (mode == detail::TEXTURE_FILTER_CLAMP) SetClamp();
		else if (mode == detail::TEXTURE_FILTER_REPEAT) SetRepeat();
		else if (mode == detail::TEXTURE_FILTER_NEAREST) SetNearest();
		else if (mode == detail::TEXTURE_FILTER_BILINEAR) SetBilinear();
		else if (mode == detail::TEXTURE_FILTER_TRILINEAR) SetTrilinear();
		else if (mode == detail::TEXTURE_FILTER_TRILINEAR_ANISOTROPIC) {
			SetTrilinear();
			SetAnisotropic(4);
		}
		else assert(false && "[GPUTextureFilter] unsupported mode!!");
	}
	void GPUTextureFilter::SetClamp() {
		glSamplerParameteri(sampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glSamplerParameteri(sampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glSamplerParameteri(sampler, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	}
	void GPUTextureFilter::SetRepeat() {
		glSamplerParameteri(sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glSamplerParameteri(sampler, GL_TEXTURE_WRAP_R, GL_REPEAT);
	}
	//sampling only the nearest neighbour
	void GPUTextureFilter::SetNearest() {
		glSamplerParameteri(sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glSamplerParameteri(sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}
	//sampling in a vicinity with bilinear interpolation
	void GPUTextureFilter::SetBilinear() {
		glSamplerParameteri(sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glSamplerParameteri(sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	//sampling in a vicinity with trilinear interpolation (needs mipmaps to work)
	// notes: combine this with anisotropic for best results
	void GPUTextureFilter::SetTrilinear() {
		glSamplerParameteri(sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glSamplerParameteri(sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	//using an anisotropic sampling kernel on the texture, with more samples taken on the dominant intersection axis
	void GPUTextureFilter::SetAnisotropic(int max_anisotropy ) {
		int hardware_max_anisotropy;
		glGetIntegerv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &hardware_max_anisotropy);
		glSamplerParameteri(sampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, std::min(max_anisotropy, hardware_max_anisotropy));
	}
	void GPUTextureFilter::Bind(unsigned int texture_unit) {
		glBindSampler(texture_unit, sampler);
	}
	int GPUTextureFilter::GetGLObject() { return sampler; }



	//---------------------------------------------------------------------------------------------
	// they have to match shader information, JUST FOR TESTING NOTHING COMPLICATED like gpu buffers as targets, etc
	GPUMaterial::GPUMaterial() {
			transparency = false;
			is_alpha_culled = false;
			codepath = 0;
		}
	GPUMaterial::~GPUMaterial() {}
	void GPUMaterial::Add(GPUTexture* texture, unsigned int texture_unit) {
		assert(texture && "[Material] adding null texture");
		for (auto& e : textures)if (e.first == texture && e.second == texture_unit) return;
		textures.push_back(std::make_pair(texture, texture_unit));
	}
	void GPUMaterial::Add(const glm::vec4& value, const std::string& name) {
		for (auto& e : uniform4)if (e.first == value && e.second == name) return;
		uniform4.push_back(std::make_pair(value, name));
	}
	void GPUMaterial::Add(const glm::vec3& value, const std::string& name) {
		for (auto& e : uniform3)if (e.first == value && e.second == name) return;
		uniform3.push_back(std::make_pair(value, name));
	}
	void GPUMaterial::Add(const glm::vec2& value, const std::string& name) {
		for (auto& e : uniform2)if (e.first == value && e.second == name) return;
		uniform2.push_back(std::make_pair(value, name));
	}
	void GPUMaterial::Add(const float& value, const std::string& name) {
		for (auto& e : uniform1)if (e.first == value && e.second == name) return;
		uniform1.push_back(std::make_pair(value, name));
	}
	const std::vector < std::pair<GPUTexture*, unsigned int>>& GPUMaterial::GetTextures() { return textures; }
	const std::vector < std::pair<glm::vec4, std::string>>& GPUMaterial::GetUniform4() { return uniform4; }
	const std::vector < std::pair<glm::vec3, std::string>>& GPUMaterial::GetUniform3() { return uniform3; }
	const std::vector < std::pair<glm::vec2, std::string>>& GPUMaterial::GetUniform2() { return uniform2; }
	const std::vector < std::pair<float, std::string>>& GPUMaterial::GetUniform1() { return uniform1; }
	bool GPUMaterial::HasTextures() { return (textures.size() > 0); }
	void GPUMaterial::Bind(GPUProgram* shader) {
		assert(shader && "[Material] using uninitialized gpu program.");
		for (auto& e : textures) e.first->Bind(e.second);
		for (auto& e : uniform4) shader->SetUniform(e.second.c_str(), e.first.x, e.first.y, e.first.z, e.first.w);
		for (auto& e : uniform3) shader->SetUniform(e.second.c_str(), e.first.x, e.first.y, e.first.z);
		for (auto& e : uniform2) shader->SetUniform(e.second.c_str(), e.first.x, e.first.y);
		for (auto& e : uniform1) shader->SetUniform(e.second.c_str(), e.first);
	}
	void GPUMaterial::SetTransparency(float transparency) {
		this->transparency = transparency;
	}
	float GPUMaterial::GetTransparency() { return transparency; }
	bool GPUMaterial::IsTransparent() {
		if (transparency > 0.01) return true;
		return false;
	}
	void GPUMaterial::SetAlphaCulled(bool culled) {
		is_alpha_culled = culled;
	}
	bool GPUMaterial::IsAlphaCulled() { return is_alpha_culled; }
	unsigned int GPUMaterial::GetCodePath() { return codepath; }
	void GPUMaterial::SetCodePath(unsigned int codepath) { this->codepath = codepath; }



	//--------------------------------------------------------------------------------------------
	// an object and its hardware instances act as as single entity on the cpu, even if they oftenly
	// represent more than one gpu object (as seen on the screen) and even if they usually SHARE gpu resources between instances
	// a scene entity DOES NOT OWN any assets, as these can be shared (they can be owned by the asset manager or manually owned)
	GPUEntity::GPUEntity() {
			mesh = nullptr;
			instance_buffer = nullptr;
			instance_count = 0;
			material = nullptr;
			is_light = false;
			is_transparent = false;
		}
	GPUEntity::~GPUEntity() {}
	GPUEntity::GPUEntity(GPUMesh* mesh, GPUMaterial* material, bool is_light, GPUBuffer* instance_buffer, unsigned int instances ) {
		assert(mesh && "[Entity] adding uninitialized mesh");
		this->mesh = mesh;
		assert(material && "[Entity] adding uninitialized material");
		this->material = material;
		this->instance_buffer = instance_buffer;
		this->instance_count = instances;
		this->is_transparent = (material->GetTransparency() > 0.01);
		this->is_light = is_light;
	}
	void GPUEntity::SetMesh(GPUMesh* mesh) {
		assert(mesh && "[Entity] adding uninitialized mesh");
		this->mesh = mesh;
	}
	void GPUEntity::SetMaterial(GPUMaterial* material) {
		assert(material && "[Entity] adding uninitialized material");
		this->material = material;
	}
	void GPUEntity::SetInstances(GPUBuffer* instance_buffer, unsigned int instances) {
		this->instance_buffer = instance_buffer;
		this->instance_count = instances;
	}
	void GPUEntity::SetIsLight(bool value) { is_light = value; }
	//override material
	void GPUEntity::SetIsTransparent(bool value) { is_transparent = value; }
	void GPUEntity::Set(GPUMesh* mesh, GPUMaterial* material, GPUBuffer* instance_buffer, unsigned int instances ) {
		assert(mesh && "[Entity] adding uninitialized mesh");
		this->mesh = mesh;
		assert(material && "[Entity] adding uninitialized material");
		this->material = material;
		this->instance_buffer = instance_buffer;
		this->instance_count = instances;
	}
	void GPUEntity::Draw(GPUProgram* shader) {
		if (instance_buffer) instance_buffer->Bind(detail::INSTANCING_BINDING_POINT);
		material->Bind(shader);
		mesh->Draw(instance_count);
	}
	GPUMesh* GPUEntity::GetMesh() { return mesh; }
	GPUBuffer* GPUEntity::GetInstanceBuffer() { return instance_buffer; }
	unsigned int GPUEntity::GetInstanceCount() { return instance_count; }
	GPUMaterial* GPUEntity::GetMaterial() { return material; }
	bool GPUEntity::IsLight() { return is_light; }
	bool GPUEntity::IsTransparent() { return is_transparent; }
}