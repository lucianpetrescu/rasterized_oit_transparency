//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


#include "camera.hpp"


namespace oit {
	//simple FPS camera
	Camera::Camera() {
		position = glm::vec3(0, 0, 50);
		forward = glm::vec3(0, 0, -1);
		up = glm::vec3(0, 1, 0);
		right = glm::vec3(1, 0, 0);
		ComputeView();
	}
	Camera::Camera(const glm::vec3 &position, const glm::vec3 &focus, const glm::vec3 &up) {
		SetView(position, focus, up);
	}
	Camera::~Camera() {
	}

	void Camera::SetView(const glm::vec3 &position, const glm::vec3 &focus, const glm::vec3 &up) {
		this->position = position;
		forward = glm::normalize(focus - position);
		right = glm::cross(forward, up);
		this->up = glm::cross(right, forward);
		ComputeView();
	}

	void Camera::TranslateForward(float distance) {
		position = position + glm::normalize(glm::vec3(forward.x, 0, forward.z))*distance;
		ComputeView();
	}
	void Camera::TranslateUpword(float distance) {
		position = position + glm::normalize(glm::vec3(0, up.y, 0))*distance;
		ComputeView();
	}
	void Camera::TranslateRight(float distance) {
		position = position + glm::normalize(glm::vec3(right.x, 0, right.z))*distance;
		ComputeView();
	}

	void Camera::RotateX(float angle) {
		if ((forward.y > 0.9 && angle > 0) || (forward.y < -0.9 && angle < 0)) return;
		forward = glm::normalize(glm::vec3((glm::rotate(glm::mat4(1.0f), angle, right)*glm::vec4(forward, 1))));
		ComputeView();
	}
	void Camera::RotateY(float angle) {
		forward = glm::normalize(glm::vec3((glm::rotate(glm::mat4(1.0f), angle, up)*glm::vec4(forward, 1))));
		right = glm::normalize(glm::vec3((glm::rotate(glm::mat4(1.0f), angle, up)*glm::vec4(right, 1))));
		ComputeView();
	}

	const glm::mat4& Camera::GetViewMatrix() {
		return view_matrix;
	}

	const glm::vec3& Camera::GetPosition() {
		return position;
	}
	const glm::vec3& Camera::GetUp() {
		return up;
	}
	const glm::vec3& Camera::GetForward() {
		return forward;
	}
	const glm::vec3 Camera::GetFocus() {
		return position + forward;
	}
	const glm::vec3& Camera::GetRight() {
		return right;
	}

	void Camera::ComputeView() {
		view_matrix = glm::lookAt(position, position + glm::normalize(forward), up);
	}
}