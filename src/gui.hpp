//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


#pragma once
#include "utils.hpp"
#include "gpu_objects.hpp"

namespace oit {
	namespace detail {
		///----------------------------------------------------------------------------------------------
		struct GuiVertexFormat {
			GuiVertexFormat();
			GuiVertexFormat(const glm::vec2& position, const glm::vec2& texcoord, const glm::vec3& color = glm::vec3(1, 1, 1));
			glm::vec2 position;
			glm::vec2 texcoord;
			glm::vec3 color;
		};

		///----------------------------------------------------------------------------------------------
		struct GuiString {
			GuiString();
			GuiString(float start_pos_x, float start_pos_y, float width, float height, const std::string &text, const glm::vec3& color = glm::vec3(1, 1, 1));
			float start_pos_x, start_pos_y;
			float width, height;
			glm::vec3 color;
			std::string text;
		};
	}

	///----------------------------------------------------------------------------------------------
	class Gui {
	public:
		Gui(const std::string& fontfile, const std::string& vertex_shader_file, const std::string& fragment_shader_file);
		~Gui();
		void ReloadGPUProgram();
		void ReloadAssets();
		void SetResolution(unsigned int width, unsigned int height);
		void ClearText();
		void AddText(float startposx, float startposy, float wperchar, float hperchar, std::string text, const glm::vec3& color = glm::vec3(1, 1, 1));
		void Draw();
	private:
		GPUProgram program;
		GPUTexture texture;
		GPUTextureFilter texture_filter;
		unsigned int vao;
		unsigned int vbo;
		std::string fontfile;

		float screen_width, screen_height;
		std::vector<detail::GuiString> strings;
		std::vector<detail::GuiVertexFormat> vertices;
	};
}