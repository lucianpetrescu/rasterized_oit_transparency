//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//  a linked list OIT renderer

#pragma once
#include "renderer_interface.hpp"

namespace oit {
	class RendererLinkedList : public RendererInterface {
	public:
		enum class SortingAlgorithm{ Bubble, Selection, Insertion, Shell, Quick, Merge };
		RendererLinkedList();
		~RendererLinkedList();
		void SetProjection(float fovy, float clip_near, float clip_far) override;
		void SetTextureFiltering(unsigned int mode) override;
		void SetRasterizationMode(unsigned int mode) override;
		void SetCamera(Camera* incamera) override;
		void SetScene(Scene* inscene) override;
		Camera* GetCamera();
		void ReloadGPUPrograms() override;
		void SetResolution(unsigned int width, unsigned int height)override;
		void SetOverrideObjectTransparency(bool value) override;
		bool GetOverrideObjectTransparency();
		void SetBackgroundColor(const glm::vec4& background_color) override;
		std::string GetName() override;
		std::string& GetSortingAlgorithmName();
		void SetSortingAlgorithm(SortingAlgorithm algorithm);

		//---------------------------------------------------------------------------------------
		void Draw() override;

	private:
		struct {
			unsigned int width = 0;
			unsigned int height = 0;
		}screen;
		Camera* camera = nullptr;
		Scene* scene = nullptr;
		GPUProgram* shader_collect = nullptr, *shader_compose= nullptr;				//flush, add, compose
		GPUTextureFilter texture_filter;
		unsigned int drawing_mode, texture_filter_mode;
		bool override_object_transparency = false;
		struct {
			float fovy = 90.f;
			float clip_near = 0.1f;
			float clip_far = 1000.f;
			glm::mat4 matrix = glm::mat4(1);
		}projection;
		glm::vec4 background_color = glm::vec4(0,0,0,0);

		//special
		GPUBuffer* fullscreen_buffer_vertex = nullptr;
		GPUBuffer* fullscreen_buffer_index = nullptr;
		GPUMesh* fullscreen_mesh = nullptr;

		unsigned int max_list_elements = 0;
		GPUTexture* texture_head = nullptr;
		GPUBuffer* buffer_elements = nullptr;
		GPUBuffer* buffer_atomic_counter = nullptr;

		//sorting algorithm
		std::string sorting_algorithm_name = "Bubble Sort";
		SortingAlgorithm sorting_algorithm = SortingAlgorithm::Bubble;
	};
}