//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//  the simple part of the OIT term
//  Cf = B + C1*A1 + C2*A2 + ... + Cn*An - A1*B - A2*B ... - An*B 

#include "renderer_meshkin.hpp"

namespace oit {
	RendererMeshkin::RendererMeshkin() {
		full = false;
		override_object_transparency = false;
		scene = nullptr;
		camera = nullptr;
		shader_pass1 = new oit::GPUProgram("../shaders/meshkin_pass1.vert", "../shaders/meshkin_pass1.frag");
		shader_pass2 = new oit::GPUProgram("../shaders/meshkin_pass2.vert", "../shaders/meshkin_pass2.frag");

		std::vector<Vertex> vertices;
		Vertex v1;	v1.position = glm::vec3(-1, -1, 0);	v1.texcoord = glm::vec3(0, 0, 0);	vertices.push_back(v1);
		Vertex v2;	v2.position = glm::vec3(1, -1, 0);	v2.texcoord = glm::vec3(1, 0, 0);	vertices.push_back(v2);
		Vertex v3;	v3.position = glm::vec3(1, 1, 0);	v3.texcoord = glm::vec3(1, 1, 0);	vertices.push_back(v3);
		Vertex v4;	v4.position = glm::vec3(-1, 1, 0);	v4.texcoord = glm::vec3(0, 1, 0);	vertices.push_back(v4);
		std::vector<unsigned int> indices;
		indices.push_back(0);	indices.push_back(1);	indices.push_back(2);
		indices.push_back(2);	indices.push_back(3);	indices.push_back(0);
		AABB aabb;
		fullscreen_buffer_vertex = new GPUBuffer((float*)&vertices[0], (unsigned int)vertices.size() * sizeof(Vertex), detail::BUFFER_USAGE_GPUSTATIC);
		fullscreen_buffer_index = new GPUBuffer((unsigned int*)&indices[0], (unsigned int)indices.size() * sizeof(unsigned int), detail::BUFFER_USAGE_GPUSTATIC);
		fullscreen_mesh = new GPUMesh(fullscreen_buffer_vertex, fullscreen_buffer_index, 0, (unsigned int)indices.size(), aabb, detail::TOPOLOGY_TRIANGLES);

		fullscreen_texture_filter.SetBilinear();
		fullscreen_render_target1 = new GPUTexture();
		fullscreen_render_target1->Create((float*)nullptr, 800, 600, 4, false);
		fullscreen_render_target2 = new GPUTexture();
		fullscreen_render_target2->Create((float*)nullptr, 800, 600, 1, false);
		fullscreen_framebuffer = new GPUFramebuffer(800, 600);
		fullscreen_framebuffer->AddBinding(fullscreen_render_target1, 0);
		fullscreen_framebuffer->AddBinding(fullscreen_render_target2, 1);

		SetTextureFiltering(detail::TEXTURE_FILTER_TRILINEAR_ANISOTROPIC);
		SetRasterizationMode(detail::RASTERIZATION_FILL);
		SetProjection(75.0f, 1.0f, 10000.0f);
		SetResolution(800, 600);
		background_color = glm::vec4(0.5, 0.5, 0.5, 0);
		//background_color = glm::vec4(0, 0, 0, 0);
		glClearColor(background_color.x, background_color.y, background_color.z, background_color.w);
	}
	RendererMeshkin::~RendererMeshkin() {
		delete shader_pass1;
		delete shader_pass2;
		delete fullscreen_mesh;
		delete fullscreen_buffer_index;
		delete fullscreen_buffer_vertex;
		delete fullscreen_render_target1;
		delete fullscreen_render_target2;
		delete fullscreen_framebuffer;
	}
	void RendererMeshkin::SetProjection(float fovy, float clip_near, float clip_far) {
		projection.fovy = fovy;
		projection.clip_near = clip_near;
		projection.clip_far = clip_far;
	}
	void RendererMeshkin::SetTextureFiltering(unsigned int mode)  {
		texture_filter.Set(mode);
		for (unsigned int i = 0; i < 10; i++) texture_filter.Bind(i);
		texture_filter_mode = mode;
	}
	void RendererMeshkin::SetRasterizationMode(unsigned int mode)  {
		if (mode == detail::RASTERIZATION_POINTS) glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
		else if (mode == detail::RASTERIZATION_WIREFRAME) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		else if (mode == detail::RASTERIZATION_FILL) glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		drawing_mode = mode;
	}
	void RendererMeshkin::SetCamera(Camera* incamera)  {
		assert(incamera && "[RendererMeshkin] null camera.");
		this->camera = incamera;
	}
	void RendererMeshkin::SetScene(Scene* inscene)  {
		assert(inscene && "[RendererMeshkin] null scene.");
		this->scene = inscene;
	}
	Camera* RendererMeshkin::GetCamera() {
		return camera;
	}
	void RendererMeshkin::ReloadGPUPrograms()  {
		shader_pass1->Reload();
		shader_pass2->Reload();
	}
	void RendererMeshkin::SetResolution(unsigned int width, unsigned int height)  {
		screen.width = width;
		screen.height = height;
		projection.matrix = glm::perspective(glm::radians(projection.fovy), (float)width / (float)height, projection.clip_near, projection.clip_far);

		//resize used buffers
		fullscreen_framebuffer->Resize(width, height);
	}
	void RendererMeshkin::SetOverrideObjectTransparency(bool value)  {
		override_object_transparency = value;
	}
	bool RendererMeshkin::GetOverrideObjectTransparency() {
		return override_object_transparency;
	}
	void RendererMeshkin::SetBackgroundColor(const glm::vec4& background_color)  {
		this->background_color = background_color;
	}

	void RendererMeshkin::SetFull(bool value) {
		full = value;
	}
	bool RendererMeshkin::GetFull() {
		return full;
	}
	std::string RendererMeshkin::GetName()  {
		if(full) return "Meshkin Full";
		return "Meshkin Partial";
	}

	//---------------------------------------------------------------------------------------
	void RendererMeshkin::Draw()  {
		SetTextureFiltering(detail::TEXTURE_FILTER_TRILINEAR_ANISOTROPIC);
		SetRasterizationMode(detail::RASTERIZATION_FILL);
		glClearColor(background_color.x, background_color.y, background_color.z, background_color.w);


		///pass1
		fullscreen_framebuffer->Bind();
		{
			glClearBufferfv(GL_COLOR, 0, &background_color.x);
			glm::vec4 white_color = glm::vec4(1, 1, 1, 1);
			glClearBufferfv(GL_COLOR, 1, &white_color.x);
			glDisable(GL_DEPTH_TEST);
			glEnable(GL_BLEND);
			glBlendEquationSeparatei(0, GL_FUNC_ADD, GL_FUNC_ADD);			/// SUM (Ci-Background)*Ai  and SUM 1/Ai
			glBlendFuncSeparatei(0, GL_ONE, GL_ONE, GL_ONE, GL_ONE);
			glBlendEquationi(1, GL_FUNC_ADD);								/// PROD 1/Ai  
			glBlendFunci(1, GL_ZERO, GL_SRC_COLOR);							/// Result = New*0 + Old*New

			shader_pass1->Bind();
			shader_pass1->SetUniform("view_matrix", camera->GetViewMatrix(), false);
			shader_pass1->SetUniform("projection_matrix", projection.matrix, false);
			//collect scene nodes
			std::vector<SceneNode*> nodelist;
			scene->GetSceneNodes(nodelist);
			//for each node
			for (auto& node : nodelist) {
				shader_pass1->SetUniform("model_matrix", node->GetCurrentTransformation(), false);

				//draw all entities
				const std::vector<GPUEntity*> entities = node->GetEntities();
				for (auto& e : entities) {
					e->GetMaterial()->Bind(shader_pass1);
					if (override_object_transparency) shader_pass1->SetUniform("mat_transparency", 0.75f);
					e->GetMesh()->Draw();
				}
			}
			glDisable(GL_BLEND);
		}
		fullscreen_framebuffer->Unbind();

		///pass2
		glViewport(0, 0, screen.width, screen.height);
		glClearBufferfv(GL_COLOR, 0, &background_color.x);
		glDisable(GL_DEPTH_TEST);
		fullscreen_render_target1->Bind(0);		fullscreen_texture_filter.Bind(0);
		fullscreen_render_target2->Bind(1);		fullscreen_texture_filter.Bind(1);
		shader_pass2->Bind();
		shader_pass2->SetUniform("background_color", background_color.x, background_color.y, background_color.z);
		if (full) shader_pass2->SetUniform("full", 1.0f);
		else shader_pass2->SetUniform("full", 0.0f);
		fullscreen_mesh->Draw();
	}
}