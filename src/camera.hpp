//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


#pragma once
#include "utils.hpp"


namespace oit {
	//simple FPS camera
	class Camera {
	public:
		Camera();
		Camera(const glm::vec3 &position, const glm::vec3 &focus, const glm::vec3 &up);
		~Camera();

		void SetView(const glm::vec3 &position, const glm::vec3 &focus, const glm::vec3 &up);

		void TranslateForward(float distance);
		void TranslateUpword(float distance);
		void TranslateRight(float distance);

		void RotateX(float angle);
		void RotateY(float angle);

		const glm::mat4& GetViewMatrix();

		const glm::vec3& GetPosition();
		const glm::vec3& GetUp();
		const glm::vec3& GetForward();
		const glm::vec3 GetFocus();
		const glm::vec3& GetRight();

	private:
		void ComputeView();
	private:
		glm::vec3 position;
		glm::vec3 up;
		glm::vec3 forward;
		glm::vec3 right;
		glm::mat4 view_matrix;
	};
}