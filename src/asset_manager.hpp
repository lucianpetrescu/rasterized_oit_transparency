//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

#pragma once
#include "scene.hpp"
#include "asset_mesh.hpp"
#include "asset_image.hpp"
#include <list>
#include <map>

namespace oit {

	//--------------------------------------------------------------------------------------------
	//creates an unmanaged GPUMesh, with unmanaged GPUBuffers, considering the entire obj file as a single mesh, discards materials
	GPUMesh* CreateMeshFromFile(const std::string& filename);

	//--------------------------------------------------------------------------------------------
	//creates an unmanaged GPUTexture
	GPUTexture* CreateTextureFromFile(const std::string& filename);
	//--------------------------------------------------------------------------------------------
	//asset managers manage the assets in a scene, it can load OBJ+MTL scenes, in-code generated scenes
	//it can also save/load in a prorpietary binary format
	//it can also create managed objects which will be c
	class AssetManager {
	public:
		AssetManager();
		~AssetManager();

		//--------------------------------------------------------------------------------------------
		//deletes all managed assets
		void Clear();

		//--------------------------------------------------------------------------------------------
		//clears scene and loads from file
		Scene* LoadScenePropietary(const std::string& filename);

		//--------------------------------------------------------------------------------------------
		//saves current scene and all assets (even the ones that are unrefereced by the scene) to file
		void SaveSceneProprietary(const std::string& filename);

		//--------------------------------------------------------------------------------------------
		//clears all assets, destroys the previous scene if any existed and loads an obj(mtl optional) as scene
		Scene* LoadSceneObj(const std::string& obj_filename);

		//--------------------------------------------------------------------------------------------
		//clears all assets, destroys the previous scene if any existed abd loads a manually created scene
		void LoadScene(Scene* scene);

	private:
		//--------------------------------------------------------------------------------------------
		//private registration functions
		GPUBuffer* GetBuffer(const std::string& name);
		GPUTexture* GetTexture(const std::string& name);
		GPUMaterial* GetMaterial(const std::string& name);
		GPUMesh* GetMesh(const std::string& name);
		GPUEntity* GetEntity(const std::string& name);
		void AddBuffer(GPUBuffer* buffer, const std::string& name = "");
		void AddMesh(GPUMesh* mesh, const std::string& name = "");
		void AddMaterial(GPUMaterial* material, const std::string& name = "");
		void AddTexture(GPUTexture* texture, const std::string& name = "");
		void AddEntity(GPUEntity* entity, const std::string& name = "");
		void AddScene(Scene* scene);

	private:
		Scene *scene = nullptr;
		std::map<GPUEntity*, std::string> entities;
		std::map<GPUBuffer*, std::string> buffers;
		std::map<GPUMesh*, std::string> meshes;						//file + objname
		std::map<GPUTexture*, std::string> textures;				//filename
		std::map<GPUMaterial*, std::string> materials;
	};

}