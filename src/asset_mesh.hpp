//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

#pragma once
#include "utils.hpp"
#include <unordered_map>
#include <unordered_set>

namespace oit {

	//--------------------------------------------------------------------------------------------
	struct Vertex {
		glm::vec3 position;
		glm::vec3 normal;
		glm::vec3 texcoord;
		glm::vec3 tangent;
		glm::vec3 extra;
		Vertex();
		Vertex(const Vertex& rhs);
		Vertex(Vertex&& rhs);
		Vertex& operator=(const Vertex &rhs);
		friend bool operator==(const Vertex &v1, const Vertex &v2);
	};
	bool operator==(const Vertex &v1, const Vertex &v2);

	//--------------------------------------------------------------------------------------------
	struct AABB {
		glm::vec3 inf = glm::vec3(FLT_MAX, FLT_MAX, FLT_MAX);
		glm::vec3 sup = glm::vec3(-FLT_MAX, -FLT_MAX, -FLT_MAX);
		AABB();
		~AABB();
		void AddPoint(const glm::vec3& point);
		void AddAABB(const AABB& aabb);
		void Reset();
		AABB& operator=(const AABB &rhs);
	};





	namespace detail {
		//--------------------------------------------------------------------------------------------
		struct ObjMaterial {
			std::string name;					///material name
			glm::vec3 ambiental;
			glm::vec3 transmission;				/// how much light is lost through transmission
			glm::vec4 diffuse_transparency;
			glm::vec4 specular_exponent;
			std::string colormap_file;			///usually there is a ambiental map too, but it almost always identical to the diffuse/color map.
			std::string normalmap_file;
			std::string displacementmap_file;
			std::string alphamap_file;
			std::string specularmap_file;
			ObjMaterial();
			ObjMaterial operator=(const ObjMaterial& rhs);
			~ObjMaterial();
		};


		//--------------------------------------------------------------------------------------------
		struct ObjMesh {
			std::string name;
			unsigned int index_material;			//index in the ObjMaterial array 
			unsigned int index_first;				//what is the first index of this sub-mesh
			unsigned int index_count;				//how many indices are in this sub-mesh
			AABB aabb;
			ObjMesh();
			ObjMesh operator=(const ObjMesh& rhs);
			~ObjMesh();
		};



		//--------------------------------------------------------------------------------------------
		// http://paulbourke.net/dataformats/mtl/
		void AssetLoadMTLFile(const std::string& filename, std::vector<ObjMaterial>& materials);

		//--------------------------------------------------------------------------------------------
		// Format: http://paulbourke.net/dataformats/obj/
		void AssetLoadOBJFile(const std::string &filename, std::vector<Vertex>& vertices, std::vector<unsigned int>& indices, std::vector<ObjMaterial>& materials, std::vector<ObjMesh>& meshes);
	}


	//--------------------------------------------------------------------------------------------
	//this considers the indices as a series of COUNTER CLOCKWISE TRIANGLES. 
	void MeshComputeNormals(std::vector<Vertex>& vertices, std::vector<unsigned int>& indices);

	//--------------------------------------------------------------------------------------------
	//this considers the indices as a series of COUNTER CLOCKWISE TRIANGLES. 
	void MeshComputeTangents(std::vector<Vertex>& vertices, std::vector<unsigned int>& indices);


}