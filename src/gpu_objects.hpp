//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

#pragma once
#include "utils.hpp"
#include "asset_shader.hpp"
#include "asset_image.hpp"
#include "asset_mesh.hpp"

namespace oit {

	//--------------------------------------------------------------------------------------------
	namespace detail {
		const unsigned int INSTANCING_BINDING_POINT = 0;
		const unsigned int TOPOLOGY_POINTS = 10;
		const unsigned int TOPOLOGY_LINES = 11;
		const unsigned int TOPOLOGY_TRIANGLES = 12;
		const unsigned int TOPOLOGY_PATCHES_2 = 13;
		const unsigned int TOPOLOGY_PATCHES_3 = 14;
		const unsigned int TOPOLOGY_PATCHES_4 = 15;
		const unsigned int RASTERIZATION_POINTS = 20;
		const unsigned int RASTERIZATION_WIREFRAME = 21;
		const unsigned int RASTERIZATION_FILL = 22;
		const unsigned int TEXTURE_FILTER_CLAMP = 30;
		const unsigned int TEXTURE_FILTER_REPEAT = 31;
		const unsigned int TEXTURE_FILTER_NEAREST = 32;
		const unsigned int TEXTURE_FILTER_BILINEAR = 33;
		const unsigned int TEXTURE_FILTER_TRILINEAR = 34;
		const unsigned int TEXTURE_FILTER_TRILINEAR_ANISOTROPIC = 35;
		const unsigned int BUFFER_USAGE_GPUSTATIC = 40;
		const unsigned int BUFFER_USAGE_GPUDYNAMIC = 41;
		const unsigned int BUFFER_USAGE_CPU_GPU = 42;
		const unsigned int BUFFER_TARGET_VERTEX = 50;
		const unsigned int BUFFER_TARGET_INDEX = 51;
		const unsigned int BUFFER_TARGET_STORAGE = 52;
		const unsigned int BUFFER_TARGET_DRAW_INDIRECT = 53;
		const unsigned int BUFFER_TARGET_DISPATCH_INDIRECT = 54;
		const unsigned int BUFFER_TARGET_ATOMIC_COUNTER = 55;
		const unsigned int BUFFER_TARGET_UNIFORM = 56;
		const unsigned int ACCESS_READ_ONLY = 60;
		const unsigned int ACCESS_WRITE_ONLY = 61;
		const unsigned int ACCESS_READ_WRITE = 62;
		const unsigned int LIGHT_TYPE_POINT = 70;
		const unsigned int LIGHT_TYPE_SPOT = 71;
		const unsigned int LIGHT_TYPE_DIRECTIONAL = 72;
		const unsigned int SHADER_TYPE_C = 80;
		const unsigned int SHADER_TYPE_VF = 81;
		const unsigned int SHADER_TYPE_VGF = 82;
		const unsigned int SHADER_TYPE_VTTF = 83;
		const unsigned int SHADER_TYPE_VTTGF = 84;
		const unsigned int UNINITIALIZED = UINT_MAX - 1;
		const unsigned int DATA_TYPE_UCHAR = 90;
		const unsigned int DATA_TYPE_FLOAT = 91;
		const unsigned int DATA_TYPE_UINT = 92;
		const unsigned int DATA_TYPE_INT = 93;
		const unsigned int DATA_TYPE_DEPTH = 94;
		const unsigned int DATA_TYPE_FLOAT_16 = 95;
		const unsigned int DATA_TYPE_UINT_8 = 96;
	}

	//--------------------------------------------------------------------------------------------
	class GPUProgram {
	public:
		GPUProgram();
		GPUProgram(const std::string& compute_file);
		GPUProgram(const std::string& vertex_file, const std::string& fragment_file);
		GPUProgram(const std::string& vertex_file, const std::string& geometry_file, const std::string& fragment_file);
		GPUProgram(const std::string& vertex_file, const std::string& tess_ctrl_file, const std::string& tess_eval_file, const std::string& fragment_file);
		GPUProgram(const std::string& vertex_file, const std::string& tess_ctrl_file, const std::string& tess_eval_file, const std::string& geometry_file, const std::string& fragment_file);
		void Set(const std::string& compute_file);
		void Set(const std::string& vertex_file, const std::string& fragment_file);
		void Set(const std::string& vertex_file, const std::string& geometry_file, const std::string& fragment_file);
		void Set(const std::string& vertex_file, const std::string& tess_ctrl_file, const std::string& tess_eval_file, const std::string& fragment_file);
		void Set(const std::string& vertex_file, const std::string& tess_ctrl_file, const std::string& tess_eval_file, const std::string& geometry_file, const std::string& fragment_file);
		~GPUProgram();
		void Reload();
		unsigned int GetType();

		void SetUniform(const std::string& name, int value);
		void SetUniform(const std::string& name, unsigned int value);
		void SetUniform(const std::string& name, float value);
		void SetUniform(const std::string& name, const glm::vec2& value);
		void SetUniform(const std::string& name, float value0, float value1);
		void SetUniform(const std::string& name, const glm::vec3& value);
		void SetUniform(const std::string& name, float value0, float value1, float value2);
		void SetUniform(const std::string& name, const glm::vec4& value);
		void SetUniform(const std::string& name, float value0, float value1, float value2, float value3);
		void SetUniform(const std::string& name, const glm::mat4& m, bool transpose);
		void Bind();
		unsigned int GetGLShader();
	private:
		int _GetLocation(const std::string& name);

	private:
		std::map<std::string, int> uniform_locations;
		unsigned int shader;
		unsigned int type;
		std::string compute_file;
		std::string vertex_file;
		std::string fragment_file;
		std::string geometry_file;
		std::string tess_ctrl_file;
		std::string tess_eval_file;
	};


	//--------------------------------------------------------------------------------------------
	class GPUBuffer {
	public:
		GPUBuffer();
		GPUBuffer(float* data, unsigned int size, unsigned int usage);
		GPUBuffer(unsigned int* data, unsigned int size, unsigned int usage);
		void Update(void* data, unsigned int size, unsigned int usage);
		void Copy(void* data, unsigned int size);
		~GPUBuffer();
		void Bind(unsigned int target);
		void BindIndexed(unsigned int target, unsigned int index);
		unsigned int GetMemory();
		unsigned int GetGLbuffer();
	private:
		unsigned int buffer;
		unsigned int memory;
	};



	//--------------------------------------------------------------------------------------------
	// no creation through already initialized gpu buffers.
	class GPUMesh {
	public:
		GPUMesh();
		//many meshes can share the same buffers so the offset is needed
		GPUMesh(GPUBuffer *vertex_buffer, GPUBuffer* index_buffer, unsigned int index_first, unsigned int index_count, const AABB& aabb, unsigned int topology);
		~GPUMesh();
		void Draw(unsigned int instances = 1);
		const AABB& GetAABB();
		GPUBuffer* GetBufferVertex();
		GPUBuffer* GetBufferIndex();
		unsigned int GetGLvao();
		unsigned int GetTriangleCount();
		unsigned int GetFirstIndexOffset();
		unsigned int GetFirstIndex();
		unsigned int GetIndexCount();
	private:
		unsigned int topology;
		AABB aabb;
		unsigned int vao;
		unsigned int index_first;
		unsigned int index_count;
		GPUBuffer* buffer_vertex;
		GPUBuffer* buffer_index;
	};



	//---------------------------------------------------------------------------------------------
	class GPUTexture {
	public:
		GPUTexture();
		//data can be nullptr, which will create an empty texture
		void Create(int *data, unsigned int width, unsigned int height, unsigned int channels, bool mipmaps);
		//data can be nullptr, which will create an empty texture
		void Create(unsigned int *data, unsigned int width, unsigned int height, unsigned int channels, bool mipmaps);
		//data can be nullptr, which will create an empty texture
		void Create(float *data, unsigned int width, unsigned int height, unsigned int channels, bool mipmaps);
		//data can be nullptr, which will create an empty texture
		void Create(unsigned char *data, unsigned int width, unsigned int height, unsigned int channels, bool mipmaps);
		//creates an empty depth texture
		void CreateDepth(unsigned int width, unsigned int height);
		//data is lost, should be used only for size varying textures like those used in a framebuffer.
		void Resize(unsigned int width, unsigned int height);
		~GPUTexture();
		void Bind(unsigned int texture_unit);
		void BindAsImage(unsigned int texture_unit, unsigned int access);
		bool HasMipmaps();
		unsigned int GetDataType();
		unsigned int GetChannels();
		unsigned int GetMemory();
		unsigned int GetWidth();
		unsigned int GetHeight();
		unsigned int GetGLObject();

		void Create(void *data, unsigned int type, unsigned int width, unsigned int height, unsigned int channels, bool mipmaps);
	private:
		bool mipmaps;
		unsigned int data_type;
		unsigned int channels;
		unsigned int memory;
		unsigned int width, height;
		unsigned int tex;
	};



	//--------------------------------------------------------------------------------------------
	class GPUFramebuffer {
	public:
		GPUFramebuffer(unsigned int width, unsigned int height);
		~GPUFramebuffer();
		void AddDepthBinding(GPUTexture* texture);
		void RemoveDepthBinding();
		void AddBinding(GPUTexture* texture, unsigned int binding_point);
		void RemoveBinding(GPUTexture* texture, unsigned int binding_point);
		void Resize(unsigned int width, unsigned int height);
	private:
		//prepare for rendering
		void Finalize();
	public:
		void Bind();
		void Unbind();

		unsigned int GetWidth();
		unsigned int GetHeight();
		unsigned int GetGLObject();
		unsigned int GetPreviousScreenWidth();
		unsigned int GetPreviousScreenHeight();
		GPUTexture* GetDepthBinding();
		const std::vector<std::pair<GPUTexture*, unsigned int>> GetBindings();
	private:
		unsigned int fbo;
		unsigned int width, height;
		unsigned int prev_screen_width, prev_screen_height;
		GPUTexture* depth_binding;
		std::vector<std::pair<GPUTexture*, unsigned int>> bindings;		//textures and binding points	
	};


	//-------------------------------------------------------------------------------------------------
	class GPUTextureFilter {
	public:
		GPUTextureFilter();
		~GPUTextureFilter();
		void Set(unsigned int mode);
		void SetClamp();
		void SetRepeat();
		//sampling only the nearest neighbour
		void SetNearest();
		//sampling in a vicinity with bilinear interpolation
		void SetBilinear();
		//sampling in a vicinity with trilinear interpolation (needs mipmaps to work)
		// notes: combine this with anisotropic for best results
		void SetTrilinear();
		//using an anisotropic sampling kernel on the texture, with more samples taken on the dominant intersection axis
		void SetAnisotropic(int max_anisotropy = 4);
		void Bind(unsigned int texture_unit);
		int GetGLObject();
	private:
		unsigned int sampler;
	};



	//---------------------------------------------------------------------------------------------
	// they have to match shader information, JUST FOR TESTING NOTHING COMPLICATED like gpu buffers as targets, etc
	class GPUMaterial {
	public:
		GPUMaterial();
		~GPUMaterial();
		void Add(GPUTexture* texture, unsigned int texture_unit);
		void Add(const glm::vec4& value, const std::string& name);
		void Add(const glm::vec3& value, const std::string& name);
		void Add(const glm::vec2& value, const std::string& name);
		void Add(const float& value, const std::string& name);
		const std::vector < std::pair<GPUTexture*, unsigned int>>& GetTextures();
		const std::vector < std::pair<glm::vec4, std::string>>& GetUniform4();
		const std::vector < std::pair<glm::vec3, std::string>>& GetUniform3();
		const std::vector < std::pair<glm::vec2, std::string>>& GetUniform2();
		const std::vector < std::pair<float, std::string>>& GetUniform1();
		bool HasTextures();
		void Bind(GPUProgram* shader);
		void SetTransparency(float transparency);
		float GetTransparency();
		bool IsTransparent();
		void SetAlphaCulled(bool culled);
		bool IsAlphaCulled();
		unsigned int GetCodePath();
		void SetCodePath(unsigned int codepath);
	private:
		unsigned int codepath;														//specific codepath
		bool is_alpha_culled;														//is it alpha culled?
		float transparency;															//transparency value, 0 means opaque
		std::vector<std::pair<GPUTexture*, unsigned int>> textures;					//texture , texture unit pair
		std::vector<std::pair<glm::vec4, std::string>> uniform4;					//vector 4 uniform
		std::vector<std::pair<glm::vec3, std::string>> uniform3;					//vector 4 uniform
		std::vector<std::pair<glm::vec2, std::string>> uniform2;					//vector 4 uniform
		std::vector<std::pair<float, std::string>> uniform1;						//vector 4 uniform
	};



	//--------------------------------------------------------------------------------------------
	// an object and its hardware instances act as as single entity on the cpu, even if they oftenly
	// represent more than one gpu object (as seen on the screen) and even if they usually SHARE gpu resources between instances
	// a scene entity DOES NOT OWN any assets, as these can be shared (they can be owned by the asset manager or manually owned)
	class GPUEntity {
	public:
		GPUEntity();
		~GPUEntity();
		GPUEntity(GPUMesh* mesh, GPUMaterial* material, bool is_light = false, GPUBuffer* instance_buffer = nullptr, unsigned int instances = 1);
		void SetMesh(GPUMesh* mesh);
		void SetMaterial(GPUMaterial* material);
		void SetInstances(GPUBuffer* instance_buffer, unsigned int instances);
		void SetIsLight(bool value);
		//override material
		void SetIsTransparent(bool value);
		void Set(GPUMesh* mesh, GPUMaterial* material, GPUBuffer* instance_buffer = nullptr, unsigned int instances = 1);
		void Draw(GPUProgram* shader);
		GPUMesh* GetMesh();
		GPUBuffer* GetInstanceBuffer();
		unsigned int GetInstanceCount();
		GPUMaterial* GetMaterial();
		bool IsLight();
		bool IsTransparent();
	private:
		bool is_light, is_transparent;
		GPUMesh* mesh;
		unsigned int instance_count;
		GPUBuffer* instance_buffer;
		GPUMaterial* material;
	};
}