//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

#pragma once
#include "utils.hpp"

namespace oit {

	namespace detail {
		unsigned int AssetCreateShader(const std::string &shader_file, GLenum shader_type);
		unsigned int AssetCreateProgram(const std::vector<unsigned int> &shader_objects);

		unsigned int AssetLoadShader(const std::string &compute_shader_file);
		unsigned int AssetLoadShader(const std::string &vertex_shader_file, const std::string &fragment_shader_file);
		unsigned int AssetLoadShader(const std::string &vertex_shader_file, const std::string &geometry_shader_file, const std::string &fragment_shader_file);
		unsigned int AssetLoadShader(const std::string &vertex_shader_file, const std::string& tessctrl_shader_file, const std::string& tesseval_shader_file, const std::string &fragment_shader_file);
		unsigned int AssetLoadShader(const std::string &vertex_shader_file, const std::string& tessctrl_shader_file, const std::string& tesseval_shader_file, const std::string& geometry_shader_file, const std::string &fragment_shader_file);

		unsigned int AssetCreateShader(const std::string &shader_file, GLenum shader_type);
		unsigned int AssetCreateProgram(const std::vector<unsigned int> &shader_objects);
	}
}
