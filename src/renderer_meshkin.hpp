//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//  the simple part of the OIT term
//  Cf = B + C1*A1 + C2*A2 + ... + Cn*An - A1*B - A2*B ... - An*B 

#pragma once
#include "renderer_interface.hpp"

namespace oit {
	class RendererMeshkin : public RendererInterface {
	public:
		RendererMeshkin();
		~RendererMeshkin();
		void SetProjection(float fovy, float clip_near, float clip_far)override;
		void SetTextureFiltering(unsigned int mode) override;
		void SetRasterizationMode(unsigned int mode) override;
		void SetCamera(Camera* incamera) override;
		void SetScene(Scene* inscene) override;
		Camera* GetCamera();
		void ReloadGPUPrograms() override;
		void SetResolution(unsigned int width, unsigned int height) override;
		void SetOverrideObjectTransparency(bool value) override;
		bool GetOverrideObjectTransparency();
		void SetBackgroundColor(const glm::vec4& background_color) override;

		void SetFull(bool value);
		bool GetFull();
		std::string GetName() override;

		//---------------------------------------------------------------------------------------
		void Draw() override;

	private:
		//full meshkin?
		bool full = false;

		struct {
			unsigned int width = 0;
			unsigned int height = 0;
		}screen;

		Camera* camera = nullptr;
		Scene* scene = nullptr;
		GPUProgram* shader_pass1 = nullptr, *shader_pass2 = nullptr;
		GPUTextureFilter texture_filter;
		unsigned int drawing_mode, texture_filter_mode;
		bool override_object_transparency = false;
		struct {
			float fovy = 90.f;
			float clip_near = 0.1f;
			float clip_far = 1000.f;
			glm::mat4 matrix = glm::mat4(1);
		}projection;
		glm::vec4 background_color = glm::vec4(0,0,0,0);

		//special
		GPUTextureFilter fullscreen_texture_filter;
		GPUBuffer* fullscreen_buffer_vertex = nullptr;
		GPUBuffer* fullscreen_buffer_index = nullptr;
		GPUMesh* fullscreen_mesh = nullptr;
		GPUTexture* fullscreen_render_target1 = nullptr, *fullscreen_render_target2 = nullptr;
		GPUFramebuffer* fullscreen_framebuffer = nullptr;
	};
}