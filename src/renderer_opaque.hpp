//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//  opaque

#pragma once
#include "renderer_interface.hpp"

namespace oit {
	class RendererOpaque : public RendererInterface{
	public:
		RendererOpaque();
		~RendererOpaque();
		void SetProjection(float fovy, float clip_near, float clip_far) override;
		void SetTextureFiltering(unsigned int mode) override;
		void SetRasterizationMode(unsigned int mode) override;
		void SetCamera(Camera* incamera) override;
		void SetScene(Scene* inscene) override;
		Camera* GetCamera();
		void ReloadGPUPrograms() override;
		void SetResolution(unsigned int width, unsigned int height) override;
		void SetBackgroundColor(const glm::vec4& background_color) override;
		void SetOverrideObjectTransparency(bool value) override;
		std::string GetName() override;

		//---------------------------------------------------------------------------------------
		void Draw() override;

	private:
		struct {
			unsigned int width =0;
			unsigned int height =0;
		}screen;
		Camera* camera = nullptr;
		Scene* scene = nullptr;
		GPUProgram* shader = nullptr;
		GPUTextureFilter texture_filter;
		unsigned int drawing_mode, texture_filter_mode;
		bool override_object_transparency = false;
		struct {
			float fovy = 90.f;
			float clip_near =0.1f;
			float clip_far = 1000.0f;
			glm::mat4 matrix = glm::mat4(1);
		}projection;
		glm::vec4 background_color = glm::vec4(0, 0, 0, 0);
	};
}