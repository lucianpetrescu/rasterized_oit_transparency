#version 430
layout(early_fragment_tests) in;
layout(location = 0) out vec4 output_framebuffer;				

uniform vec3 mat_diffuse;
uniform float mat_transparency;
uniform uint MAX_LIST_ELEMENTS;

layout(binding = 0) uniform atomic_uint atomicbuffer;					//0th atomic counter buffer
layout(binding = 0, r32ui) coherent uniform uimage2D headTexture;		//0th texture unit
struct ListFragment
{
	vec3 color;
	float alpha;
	float depth;
	uint nextFragment;
};
layout(std140, binding = 0) buffer ElementsBuffer						//0th ssbo unit
{
	ListFragment elements[];
};

void main(){

	vec3 color = mat_diffuse;
	float opacity = mat_transparency;

	uint magic_number = 300000000;
	uint max_elements = MAX_LIST_ELEMENTS;

	uint fragno = atomicCounterIncrement(atomicbuffer);											//atomically allocated elements index (fragment number)

	if(fragno<max_elements){
		uint oldhead = imageAtomicExchange(headTexture, ivec2(gl_FragCoord.xy), fragno);		//atomically increase pixel list index and get previous head
		elements[fragno].color = color;
		elements[fragno].alpha = opacity;
		elements[fragno].depth = gl_FragCoord.z;												
		elements[fragno].nextFragment = oldhead;												//old head is next		
	}

}