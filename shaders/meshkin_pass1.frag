#version 430
layout(location = 0) out vec4 render_target1;				// B+ SUM(Ci-B)*Ai   .... SUM 1/Ai
layout(location = 1) out float render_target2;				// PROD Ai

uniform vec3 mat_diffuse;
uniform float mat_transparency;
uniform vec3 background_color;

void main(){

	vec3 color = mat_diffuse;
	float opacity = mat_transparency;

	render_target1 = vec4( (color - background_color) * opacity , 1.0/opacity);	
	render_target2 = opacity;														// for A0*A1*A2...
}