#version 430
layout(location = 0) out vec3 output_color;							//0 color
layout(binding = 0, r32ui) coherent uniform uimage2D occupancyMap0;
layout(binding = 1, r32ui) coherent uniform uimage2D occupancyMap1;
layout(binding = 2, r32ui) coherent uniform uimage2D occupancyMap2;
layout(binding = 3, r32ui) coherent uniform uimage2D occupancyMap3;

layout(binding = 4, rgba32f) coherent uniform image2D imageCount;
layout(binding = 5, r32f) coherent uniform image2D imageAlpha;

uniform vec3 mat_diffuse;
uniform float mat_transparency;

in highp float depth_viewspace_01;

void main(){
	//compute position in occupancy map
	uint occupancy_pos = uint(depth_viewspace_01*128);
	uint occupied_position = uint(clamp (depth_viewspace_01 * 128,0,127));
	uint slab = clamp(uint(occupancy_pos / 32),0,3);
	uint slab_nth = clamp(uint(occupancy_pos - slab*32),0,31);				//left to right
	uint occupancy_slab = uint(pow(2, slab_nth));

	//load data
	ivec2 coord = ivec2(gl_FragCoord.xy);
	uint occupancy[4];

	occupancy[0] = imageLoad(occupancyMap0, coord).x;
	occupancy[1] = imageLoad(occupancyMap1, coord).x;
	occupancy[2] = imageLoad(occupancyMap2, coord).x;
	occupancy[3] = imageLoad(occupancyMap3, coord).x;

	vec4 count = imageLoad(imageCount, coord);			// x for 0, y for 1, z for 2, w for 3
	float alphasum = imageLoad(imageAlpha, coord).x;

	//compute the number of fragments that are in front a
	float fragments_in_front = 0;								/// from 00001000000000000000 to 0000X1111111111111111																			
	//fragments_in_front = bitCount((occupancy_slab - 1) & occupancy[slab]) + bitCount((occupancy_slab - 1) & occupancy[slab])
	if(slab>=1) for(int i=int(slab-1);i>=0;i--) fragments_in_front+=count[i];						

	//aproximate alpha based on approximated density before this fragment
	float medium_alpha = alphasum / float(count.x+count.y+count.z+count.w);
	float exponent = clamp(fragments_in_front,0,100);
	float term = pow( 1-medium_alpha,exponent);
	output_color =  term* mat_transparency * mat_diffuse;
}