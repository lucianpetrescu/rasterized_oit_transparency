#version 430

layout(location = 0) out vec4 output_count;									//count
layout(location = 1) out float output_alpha;								// alpha sum

layout(binding = 0, r32ui) coherent uniform uimage2D occupancyMap0;			//closest
layout(binding = 1, r32ui) coherent uniform uimage2D occupancyMap1;
layout(binding = 2, r32ui) coherent uniform uimage2D occupancyMap2;
layout(binding = 3, r32ui) coherent uniform uimage2D occupancyMap3;			//farthest

uniform float mat_transparency;

in float depth_viewspace_01;


void main(){


	//compute position in occupancy map
	uint occupancy_pos = uint(depth_viewspace_01*128);
	uint occupied_position = uint(clamp (depth_viewspace_01 * 128,0,127));
	uint slab = clamp(uint(occupancy_pos / 32),0,3);
	uint slab_nth = clamp(uint(occupancy_pos - slab*32),0,31);				//left to right
	uint occupancy_slab = uint(pow(2, slab_nth));
	
	// write to occupancy maps
	if(slab==0) imageAtomicOr(occupancyMap0, ivec2(gl_FragCoord.xy), occupancy_slab);
	else if(slab==1) imageAtomicOr(occupancyMap1, ivec2(gl_FragCoord.xy), occupancy_slab);
	else if(slab==2) imageAtomicOr(occupancyMap2, ivec2(gl_FragCoord.xy), occupancy_slab);
	else if(slab==3) imageAtomicOr(occupancyMap3, ivec2(gl_FragCoord.xy), occupancy_slab);
	
	vec4 total_count = vec4(0);	total_count[slab] = 1;
	output_count = total_count;
	output_alpha = mat_transparency;
}