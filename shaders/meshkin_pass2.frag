#version 430
layout(location = 0) out vec4 out_color;

layout(binding = 0) uniform sampler2D renderTarget1;		/// B+ SUM (Ci-B)*Ai	.... SUM 1/Ai
layout(binding = 1) uniform sampler2D renderTarget2;		/// PROD Ai
uniform vec3 background_color;
uniform float full;

in vec2 texcoord;
void main(){

	vec4 term1 = texture(renderTarget1, texcoord).xyzw;
	out_color = vec4(term1.xyz,1);

	if(full>0.5){
		float term2 = texture(renderTarget2, texcoord).x;
	
		float aprod = term2.x;
		float oneoverasum = term1.w;

		vec3 color = term1.xyz;
		if(aprod<1) color+=  background_color * aprod *( 1- oneoverasum);

		out_color = vec4(color,1);
	}
	
}