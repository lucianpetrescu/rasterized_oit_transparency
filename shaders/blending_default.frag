#version 430
layout(location = 0) out vec4 output_framebuffer;

uniform vec3 mat_diffuse;
uniform float mat_transparency;

void main(){

	output_framebuffer = vec4(mat_diffuse,mat_transparency);
}