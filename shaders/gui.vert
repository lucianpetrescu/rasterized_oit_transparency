#version 430
layout (location = 0) in vec2 in_position;
layout (location = 1) in vec2 in_texcoord;  
layout (location = 2) in vec3 in_color;
uniform vec2 screen_resolution;
out vec2 texcoord;
out vec3 color;

void main()
{        
	//screen space -> ndc space
	vec2 ndcpos = (in_position - screen_resolution/2.0f)/(screen_resolution/2);

	color = in_color;
	texcoord = in_texcoord;
    gl_Position = vec4(ndcpos,0,1);
}