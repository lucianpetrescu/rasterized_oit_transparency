#version 430
layout(location = 0) out vec3 output_framebuffer;			
layout(binding = 0, r32ui) coherent uniform uimage2D occupancyMap0;			//closest
layout(binding = 1, r32ui) coherent uniform uimage2D occupancyMap1;
layout(binding = 2, r32ui) coherent uniform uimage2D occupancyMap2;
layout(binding = 3, r32ui) coherent uniform uimage2D occupancyMap3;			//farthest

layout(binding = 4, rgba32f) coherent uniform image2D imageCount;
layout(binding = 5, r32f) coherent uniform image2D imageAlpha;
layout(binding = 6, rgba32f) coherent uniform image2D imageColor;

layout(std140, binding = 0) buffer ChosenPixelBuffer						//0th ssbo unit
{
	uvec4 chosen_pixel_buffer[];
};
uniform vec2 chosen_pixel;
uniform vec4 background_color;
uniform int show_occupancy;

void main(){
	
	ivec2 coord = ivec2(gl_FragCoord.xy);
	uint o0 = imageAtomicExchange(occupancyMap0, coord,0);
	uint o1 = imageAtomicExchange(occupancyMap1, coord,0);
	uint o2 = imageAtomicExchange(occupancyMap2, coord,0);
	uint o3 = imageAtomicExchange(occupancyMap3, coord,0);

	//save occupancy for debug.
		
	vec4 count = imageLoad(imageCount, coord);
	float alpha_sum = imageLoad(imageAlpha, coord).x;
	vec3 color =imageLoad(imageColor, coord).xyz;

	float total_count = count.x + count.y + count.z + count.w;
	if(total_count >0 ){
		//NORMAL RENDER PATH
		float medium_alpha = alpha_sum/ total_count;
		output_framebuffer = (color + background_color.xyz * pow(1-medium_alpha, total_count));
		
		//DEBUG OUTPUT FOR OCCUPANCY
		//make it green (red is used in UI and the sampling circle is blue)
		if(show_occupancy == 1){
			output_framebuffer = vec3(0, (  count.x + count.y +count.z +count.w )/7.f,0);
		}
	}else{
		//BACKGROUND
		output_framebuffer = background_color.xyz;
	}

	//output the occupancy for the chosen pixel area
	ivec2 chosen_coord = ivec2(chosen_pixel);
	if(distance(chosen_coord, coord)<5){
		uvec4 cpb = chosen_pixel_buffer[0];
		chosen_pixel_buffer[0]= uvec4(o0 | cpb.x,o1 | cpb.y,o2 | cpb.z,o3 | cpb.a);
		output_framebuffer = vec3(0,0,1);				//blue circle 
	}
}