#version 430

layout(location = 0) in vec3 in_position;	
uniform mat4 model_matrix, view_matrix, projection_matrix; 
uniform vec2 frustum_near_far;

out float depth_viewspace_01;

void main(){

	vec4 position = view_matrix * model_matrix*vec4(in_position,1);
	
	float n = frustum_near_far.x;
	float f = frustum_near_far.y;
	depth_viewspace_01 = clamp( position.z/(n-f),0,1);
	
	gl_Position = projection_matrix*position;
}
