﻿#version 430
layout(early_fragment_tests) in;
layout(location = 0) out vec4 output_framebuffer;						//default framebuffer

layout(binding = 0, r32ui) coherent uniform uimage2D headTexture;		//0th texture unit
struct ListFragment
{
	vec3 color;
	float alpha;
	float depth;
	uint nextFragment;
};
layout(std140, binding = 0) buffer ElementsBuffer						//0th ssbo unit
{
	ListFragment elements[];
};


struct Fragment{
	vec3 color;	
	float alpha;
	float depth;
};


const int SortingAlgorithmBubble = 0;
const int SortingAlgorithmSelection = 1;
const int SortingAlgorithmInsertion = 2;
const int SortingAlgorithmShell = 3;
const int SortingAlgorithmQuick = 4;
const int SortingAlgorithmMerge = 5;
uniform int sorting_algorithm;
uniform vec3 background_color;

//global memory for fragments for this pixel
Fragment frags[48];
Fragment aux;
void swapFragments(int i, int j){
	aux.color = frags[i].color;
	aux.alpha = frags[i].alpha;
	aux.depth = frags[i].depth;
					
	frags[i].color = frags[j].color;
	frags[i].alpha = frags[j].alpha;
	frags[i].depth = frags[j].depth;
					
	frags[j].color = aux.color;
	frags[j].alpha = aux.alpha;
	frags[j].depth = aux.depth;
}

// the most basic sort
void BubbleSort(int count){
	for(int i=0;i<(count-1);i++){
		for(int j=i+1;j<count;j++){
			if(frags[i].depth>frags[j].depth){
				swapFragments(i,j);
			}
		}
	}
}

// many library sort (e.g. timsort) variants default to insertion sort if the input < K (32 or 64) 
// because of the adaptive property and of the small number of compares
void InsertionSort(int count){
	for(int i = 1; i<count; i++){
		int j = i;
		while( j > 0 && frags[j-1].depth > frags[j].depth){
			swapFragments(j-1,j);
			j--;
		}
	}
}

//for small counts (as is the case here) selection sort can outperform instertion sort because it does less
//swaps (writes) and more compares (reads). Given that the GPU cache/local memory is heavily optimized for 
//reads the selection sort
void SelectionSort(int count){
	for(int j=0;j<(count-1);j++){
		int imin = j;
		for(int i=j+1; i<count;i++)	if(frags[i].depth < frags[imin].depth) imin = i;
		if(j!=imin)swapFragments(j,imin);
	}
}
// works as an insertion sort which first tries to quickly swap elements over progressively 
// smaller distances until reaching the 1 distance when it becomes a simple insertion sort.
// This strategy is useful to benefit from the adaptive property of the insertion sort, 
// because it quickly transforms the array into a close to sorted form.
// Two gap sequence variants are implemented, the classic O(N^2) shell gap sequence which 
// has O(N^2) complexity, and the Pratt sequence which has O(Nlog^2N) complexity. 
void ShellSort(int count){
	//classic Shell gaps
	const bool USE_CLASSIC_SHELL_GAPS = false;		//true = SHELL gaps, false = PRATT gaps
	if(USE_CLASSIC_SHELL_GAPS){
		int gap = count/2;
		while(gap>0){
			for(int i=gap;i<count; i++){
				aux.color = frags[i].color;
				aux.alpha = frags[i].alpha;
				aux.depth = frags[i].depth;
				int j;
				for(j = i; j>= gap && frags[j-gap].depth > aux.depth; j-=gap){
					frags[j].color = frags[j-gap].color;	
					frags[j].alpha = frags[j-gap].alpha;
					frags[j].depth = frags[j-gap].depth;
				}
				frags[j].color = aux.color;
				frags[j].alpha = aux.alpha;
				frags[j].depth = aux.depth;
			}
			gap = gap/2;
		}
	}else{
		//Pratt O(Nlog^2N) gaps, stopping at maximum element strictly under 48/2, where 48 is the constant max number of elements per pixel)
		const int gaps[9] = { 1,2,3,4,6,8,9,12,18 };
		for(int kgap=8;kgap>=0;kgap--){
			int gap = gaps[kgap];
			for(int i=gap;i<count; i++){
				aux.color = frags[i].color;
				aux.alpha = frags[i].alpha;
				aux.depth = frags[i].depth;
				int j;
				for(j = i; j>= gap && frags[j-gap].depth > aux.depth; j-=gap){
					frags[j].color = frags[j-gap].color;	
					frags[j].alpha = frags[j-gap].alpha;
					frags[j].depth = frags[j-gap].depth;
				}
				frags[j].color = aux.color;
				frags[j].alpha = aux.alpha;
				frags[j].depth = aux.depth;
			}
		}
	}
}

int QuickSortPartition(int low, int high){
	float depth_pivot = frags[high].depth;
	int idx = low - 1;
	for (int i = low; i < high; i++) {
		if (frags[i].depth <= depth_pivot) {
			idx++;
			swapFragments(i, idx);
		}
	}
	swapFragments(idx + 1, high);
	return idx + 1;
}
//in-place iterative quicksort (recursivity is not permitted in GLSL!)
//O(nlogn) with O(n^2) worst case
void QuickSort(int count){
	//we'll pivot a maximum of max_count fragment (48) times
	ivec2 stack[48];
	int stack_idx = 0;

	//push in the pivot stack (don't forget to push reachable limits -> [0,count-1]
	stack[stack_idx++] = ivec2(0, count-1);
	
	//while stack is not empty
	while (stack_idx > 0) {
		ivec2 bounds = stack[--stack_idx];
		int low = bounds.x;
		int high = bounds.y;

		int p = QuickSortPartition(low, high);
		if ((p - 1) > low)  stack[stack_idx++] = ivec2(low, p - 1);
		if ((p + 1) < high) stack[stack_idx++] = ivec2(p + 1, high);
	}
}

//in-place bottom-up iterative merge sort, in O(NlogN) time complexity and O(N) space complexity
void MergeSort(int count){
	Fragment additional[48];
	int size = 1;
	while (size < count) {
		int runs = (count / size / 2 + 1);
		for (int s = runs-1; s >=0; s--) {
			int l = s*2 * size;
			int m = min( (s*2+1)*size, count);
			int r = min( (s*2+2)*size, count);
			if (m == r) continue;

			//merge
			int i = l, j = m;
			for (int k = l; k < r; k++) {
				if (i < m && (j >= r || frags[i].depth <= frags[j].depth)) additional[k] = frags[i++];
				else additional[k] = frags[j++];
			}

			//copy to frags
			for (int k = l; k < r; k++) frags[k] = additional[k];
		}
		size = size * 2;
	}
}

void main(){

	uint head = imageLoad(headTexture, ivec2(gl_FragCoord.xy)).x;
	uint magic_number = 300000000;

	if(head!=magic_number){
		
		//read list
		int count=0;
		while(count<48 && head!=magic_number){
			frags[count].color=elements[head].color;
			frags[count].alpha=elements[head].alpha;
			frags[count].depth=elements[head].depth;
			
			count+=1;
			head = elements[head].nextFragment;	
		}

		//sort list
		switch(sorting_algorithm){
			case SortingAlgorithmBubble: BubbleSort(count); break;
			case SortingAlgorithmInsertion: InsertionSort(count); break;
			case SortingAlgorithmSelection: SelectionSort(count); break;
			case SortingAlgorithmShell: ShellSort(count); break;
			case SortingAlgorithmQuick: QuickSort(count); break;
			case SortingAlgorithmMerge: MergeSort(count); break;
			default: break;
		}
		
		//front to back
		vec3 fragment_color=vec3(0);
		float fragment_alpha =1;
		for(int k=0;k<count;k++){
			vec3 C = frags[k].color;
			float A = frags[k].alpha;
			fragment_color = fragment_alpha*C * A + fragment_color;						/// Cf = Cfrag*Af*(1-Arc) + Cf
			fragment_alpha = fragment_alpha*(1-A);										/// Af = (1-Afrag)*Af				<- A is opacity not transparency
		}
		fragment_color = fragment_alpha*background_color + fragment_color;

		//output
		output_framebuffer = vec4(fragment_color,1);
		
	}else{
		output_framebuffer = vec4(background_color,1);				//empty list
	}

	//independent of result reinitialize head texture for the next frame
	imageStore(headTexture, ivec2(gl_FragCoord.xy),uvec4(magic_number,0,0,0));	
}