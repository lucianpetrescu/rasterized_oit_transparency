#version 440
layout (location =0) out vec4 outputFramebuffer;
layout (binding =0) uniform sampler2D texture;
in vec2 texcoord;
in vec3 color;

void main()
{
	vec3 data = texture2D(texture,texcoord).xyz;
	if(data.x<0.2) discard;
	outputFramebuffer=vec4(color,1);
}