#version 430

layout(location = 0) in vec3 in_position;	
layout(location = 2) in vec3 in_texcoord;

out vec2 texcoord;
void main(){

	texcoord = in_texcoord.xy;

	gl_Position = vec4(in_position,1);
}
